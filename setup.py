#*****************************************************************************
#       Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from setuptools import setup, Extension

libvoxelize = Extension('pyvoxelize._libvoxelize', ['pyvoxelize/_libvoxelize.cpp'],
                         extra_compile_args = ['-std=c++11', '-march=native', '-mtune=native', '-frounding-math',
                                               '-Wall', '-Wpedantic', '-Wno-unknown-pragmas',
                                               '-Wno-unused-variable', '-Wno-unused-function'])

setup(name = 'pyvoxelize',
      version = '0.1',
      description = 'Interface to voxelize solver',
      install_requires = ['numpy', 'scipy', 'vispy', 'imageio', 'imageio-ffmpeg'],
      # extra_requires = ['scipy', 'vispy'],
      ext_modules = [libvoxelize],
      packages    = ['pyvoxelize'])

