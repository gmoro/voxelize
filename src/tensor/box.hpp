/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#ifndef TENSOR_BOX_HPP
#define TENSOR_BOX_HPP
#include "tensor.hpp"
#include <sstream> 
#include <limits>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iostream>
#include <cmath>
#include <array>

template<typename Numeric>
class Tensor_box : public Tensor
{
    public:
        std::vector< std::vector<Numeric> > data;
        Tensor_box() {}
        Tensor_box (const Tensor & t) : Tensor(t) {} 
};

template<typename Numeric>
Tensor_box<Numeric> make_tensor_box(const std::vector<Numeric> & box)
{
    index_t N = box.size();
    Tensor_box<Numeric> result;
    result.pos = std::vector<std::vector<index_t>>(N, {0,1});
    result.idx = std::vector<std::vector<small_index_t>>(N, {0});
    result.data.resize(N);
    for(index_t k = 0; k < N; ++k) {
        result.data[k] = std::vector<Numeric>(1, box[k]);
    }
    return result;
}

template<typename Numeric>
Tensor_box<Numeric> make_tensor_box(const std::vector<Numeric> & box, const std::vector<small_index_t> & indices)
{
    index_t N = box.size();
    Tensor_box<Numeric> result;
    result.pos = std::vector<std::vector<index_t>>(N, {0,1});
    result.idx = std::vector<std::vector<small_index_t>>(N, {0});
    result.data.resize(N);
    for(index_t k = 0; k < N; ++k) {
        result.idx[k][0] = indices[k];
        result.data[k].resize(indices[k]+1, 0);
        result.data[k][0] = box[k];
        result.data[k][indices[k]] = box[k];
    }
    return result;
}

template<typename Numeric>
void get_box(const Tensor_box<Numeric> & box_set, const std::vector<index_t> & index,
                             std::vector<Numeric> & result)
{
    index_t N = index.size();
    for(index_t i = 0; i < N; ++i) {
        result[i] = box_set.data[i][box_set.idx[i][index[i]]];
    }
}


// Split boxes in n in a given direction.
template<typename Numeric>
void split(Tensor_box<Numeric> & box, index_t d, index_t n)
{
    index_t N = box.get_number_axes();
    index_t Npos = box.pos[d].size();
    index_t Nbox = box.pos[d].back();
    // duplicate data
    index_t Ndata = box.data[d].size();
    box.data[d].resize(n*Ndata, 0);
    std::vector<Numeric> boxes;
    boxes.reserve(n);
    for(index_t j = 0; j < Ndata; ++j) {
        split(box.data[d][Ndata-1-j], n, boxes);
        for(index_t k = 0; k < n; ++k) {
            box.data[d][(Ndata-1-j)*n + k] = boxes[k];
        }
    }
    // update pos and idx sizes
    index_t Nidx;
    for(index_t j = d; j < N; ++j) {
        Nidx = box.idx[j].size();
        box.idx[j].resize(n*Nidx, 0);
        if(j < N-1) {
            box.pos[j+1].resize(n*Nidx+1, 0);
        }
    }
    // update initial pos
    for(index_t i = 1; i < Npos; ++i) {
        box.pos[d][i] *= n;
    }
    // update idx and remaining pos
    for(index_t i = 0; i < Nbox; ++i) {
        index_t start = Nbox - 1 - i;
        index_t delta = 1;
        // multiplicate initial idx
        for(index_t k = 0; k < n; k++) {
            box.idx[d][n*start+n-1-k] = n*box.idx[d][start]+n-1-k;
        }
        for(index_t j = d+1; j < N; ++j) {
            // mutliplicate pos
            index_t delta_pos = box.pos[j][start+delta]-box.pos[j][start];
            //from end to start to avoid overwrite
            for(index_t m = 0; m < n; ++m) {
                for(index_t k = 0; k < delta; ++k) {
                    box.pos[j][n*start + (n-m)*delta - k] = (n-1)*box.pos[j][start] + (n-1-m)*delta_pos + box.pos[j][start+delta-k];
                }
            }
            start = box.pos[j][start];
            delta = delta_pos;
            // duplicate idx from end to start to avoid overwrite
            for(index_t m = 0; m < n; ++m) {
                for(index_t k = 0; k < delta; ++k) {
                    box.idx[j][n*start + (n-m)*delta - 1 - k] = box.idx[j][start+delta-1-k];
                }
            }
        }
    }
}

// Split boxes in every direction, in number given in input.
// New coordinates are kept in monotoneous order.
template<typename Numeric>
void split(Tensor_box<Numeric> & box, const std::vector<index_t> & S)
{
    index_t Ndata, Npos, Nidx;
    index_t N = box.get_number_axes();
    index_t n = 1;
    for(index_t k = 0; k < N; ++k) {
        if(S[k] >= 1) {
            Ndata = box.data[k].size();
            Npos = box.pos[k].size() - 1;
            Nidx = box.idx[k].size();
            box.data[k].reserve(S[k]*Ndata);
            box.pos[k].reserve(n*Npos + 1);
            box.idx[k].reserve(S[k]*n*Nidx);
            n *= S[k];
        }
    }
    for(index_t k = 0; k < N; ++k) {
        if(S[k] >= 1) {
            split(box, k, S[k]);
        }
    }
}

// Split boxes in 2 in every direction.
// New coordinates are kept in monotoneous order.
template<typename Numeric>
void split(Tensor_box<Numeric> & box)
{
    index_t Ndata, Npos, Nidx;
    index_t N = box.get_number_axes();
    index_t n = 1;
    for(index_t k = 0; k < N; ++k) {
        Ndata = box.data[k].size();
        Npos = box.pos[k].size() - 1;
        Nidx = box.idx[k].size();
        box.data[k].reserve(2*Ndata);
        box.pos[k].reserve(n*Npos + 1);
        box.idx[k].reserve(2*n*Nidx);
        n *= 2;
    }
    for(index_t k = 0; k < N; ++k) {
        split(box, k, 2);
    }
}

// Shift boxes coordinates origin
template<typename Numeric>
void shift(Tensor_box<Numeric> & box_set, const std::vector<Numeric> & vector)
{
    for(index_t i = 0; i < box_set.data.size(); ++i) {
        for(index_t j = 0; j < box_set.data[i].size(); ++j) {
            add(box_set.data[i][j], vector[i], box_set.data[i][j]);
        }
    }
}

template<typename Numeric>
void inflate(Tensor_box<Numeric> & box_set, const Numeric & scale)
{
    Numeric db(0);
    Numeric m(0);
    for(index_t i = 0; i < box_set.data.size(); ++i) {
        for(index_t j = 0; j < box_set.data[i].size(); ++j) {
            mid(box_set.data[i][j], m);
            delta(box_set.data[i][j], db);
            fma(db, scale, m, box_set.data[i][j]);
        }
    }
}


// Remove elements that are true in the table
template<typename Numeric>
void remove(Tensor_box<Numeric> & box, const std::vector<char> & table)
{
    index_t c;
    index_t d;
    std::vector<char> table_tmp;
    index_t N = box.get_number_axes()-1;
    auto it_table = table.cbegin();
    for(index_t k = 0; k < N+1; ++k) {
        c = 0;
        d = 0;
        index_t Npos = box.pos[N-k].size();
        index_t Nidx = box.idx[N-k].size();
        table_tmp.resize(Npos - 1);
        for(index_t j = 0; j < Npos - 1; ++j) {
            // the first range bound is already updated in the loop
            // and is now at position j-d, its value shifted by c
            for(index_t i = box.pos[N-k][j-d] + c; i < box.pos[N-k][j+1]; ++i) {
                if(it_table[i]) {
                    c++;
                } else {
                    box.idx[N-k][i-c] = box.idx[N-k][i];
                }
            }
            if(box.pos[N-k][j-d] + c == box.pos[N-k][j+1]) {
                table_tmp[j] = true;
                d++;
            } else {
                table_tmp[j] = false;
            }
            box.pos[N-k][j+1-d] = box.pos[N-k][j+1] - c;
        }
        box.idx[N-k].resize(Nidx - c);
        box.pos[N-k].resize(Npos - d);
        it_table = table_tmp.cbegin();
    }
}

inline std::ostream& print_normal_obj(std::ostream& os)
{
    // Preamble of the obj file
    os << "vn " << -1 << " " <<  0 << " " <<  0 << '\n';
    os << "vn " <<  1 << " " <<  0 << " " <<  0 << '\n';
    os << "vn " <<  0 << " " << -1 << " " <<  0 << '\n';
    os << "vn " <<  0 << " " <<  1 << " " <<  0 << '\n';
    os << "vn " <<  0 << " " <<  0 << " " << -1 << '\n';
    os << "vn " <<  0 << " " <<  0 << " " <<  1 << '\n';
    os << '\n';
    return os;
}


// Print the box to an obj file
template<typename Numeric>
index_t append_obj(std::ostream& os, const Tensor_box<Numeric> & box_set, const std::vector<index_t> axes, index_t start)
{
    double first_corner[3];
    double second_corner[3];

    if(box_set.get_number_elements() == 0) {
        return start;
    }
    index_t max = *std::max_element(axes.begin(), axes.end());
    index_t N = box_set.idx[max].size();
    std::vector<index_t> it_box(max+1, 0);
    std::vector<std::vector<index_t>> boxes;
    boxes.resize(N);
    for(index_t k = 0; k < N; ++k) {
        boxes[k] = it_box;
        box_set.advance(it_box);
    }
    std::sort(boxes.begin(), boxes.end(), [&](const std::vector<index_t> & u, const std::vector<index_t> & v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][u[axes[i]]];
                index_t vi = box_set.idx[axes[i]][v[axes[i]]];
                if(ui != vi) {
                    return ui < vi;
                }
            }
            return false;
    });
    auto last = std::unique(boxes.begin(), boxes.end(), [&](const std::vector<index_t> & u, const std::vector<index_t> & v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][u[axes[i]]];
                index_t vi = box_set.idx[axes[i]][v[axes[i]]];
                if(ui != vi) {
                    return false;
                }
            }
            return true;
    });
    boxes.erase(last, boxes.end());


    //typename Numeric::base_type first_corner[3];
    //typename Numeric::base_type second_corner[3];
    //index_t dim = box_set.get_number_axes() < 3 ? box_set.get_number_axes() : 3;
    //index_t N = box_set.idx[dim-1].size();
    //std::vector<Numeric> b(dim, 0);
    //std::vector<index_t> it_box(dim, 0);

    //std::stringstream buf;
    //buf.str().reserve(N*dim*200);
    //buf << std::setprecision(std::numeric_limits<double>::digits10);
    auto& buf = os;
    buf << std::setprecision(std::numeric_limits<double>::digits10);

    index_t dim = axes.size();
    std::vector<Numeric> b(max+1, 0);
    index_t n = start;
    for(index_t k = 0; k < boxes.size(); ++k) {
        get_box(box_set, boxes[k], b);
        //box_set.advance(it_box);

        for(size_t i = 0; i < dim; ++i) {
            first_corner[i] = get_lower(b[axes[i]]);
            second_corner[i] = get_upper(b[axes[i]]);
        }
        for(size_t i=dim; i<3; ++i) {
            first_corner[i] = 0;
            second_corner[i] = get_upper(b[axes[0]]) - get_lower(b[axes[0]]);
        }

        /* write the label of each box as comment on os
        for(const auto& i:b) {
            buf << "# " << i.get_log_label({0.5, 1.5}) << '\n';
        }
        */

        if(dim == 2) {
            buf << "v " << first_corner[0] << " " << first_corner[1] << " " << 0 << '\n';
            buf << "v " << second_corner[0] << " " << first_corner[1] << " " << 0 << '\n';
            buf << "v " << second_corner[0] << " " << second_corner[1] << " " << 0 << '\n';
            buf << "v " << first_corner[0] << " " << second_corner[1] << " " << 0 << '\n';
            buf << '\n';

            // Face and normal
            buf << "f " << n + 1 << "//" << 6 << " "
                       << n + 2 << "//" << 6 << " "
                       << n + 3 << "//" << 6 << " "
                       << n + 4 << "//" << 6 << '\n';
            n = start + 4*(k+1);
        } else  {
            buf << "v " << first_corner[0] << " " << first_corner[1] << " " << first_corner[2] << '\n';
            buf << "v " << second_corner[0] << " " << first_corner[1] << " " << first_corner[2] << '\n';
            buf << "v " << first_corner[0] << " " << second_corner[1] << " " << first_corner[2] << '\n';
            buf << "v " << second_corner[0] << " " << second_corner[1] << " " << first_corner[2] << '\n';
            buf << "v " << first_corner[0] << " " << first_corner[1] << " " << second_corner[2] << '\n';
            buf << "v " << second_corner[0] << " " << first_corner[1] << " " << second_corner[2] << '\n';
            buf << "v " << first_corner[0] << " " << second_corner[1] << " " << second_corner[2] << '\n';
            buf << "v " << second_corner[0] << " " << second_corner[1] << " " << second_corner[2] << '\n';
            buf << '\n';

            // Faces and normals
            buf << "f " << n + 1 << "//" << 1 << " "
                       << n + 5 << "//" << 1 << " "
                       << n + 7 << "//" << 1 << " "
                       << n + 3 << "//" << 1 << '\n';
            buf << "f " << n + 4 << "//" << 2 << " "
                       << n + 8 << "//" << 2 << " "
                       << n + 6 << "//" << 2 << " "
                       << n + 2 << "//" << 2 << '\n';
            buf << "f " << n + 1 << "//" << 3 << " "
                       << n + 2 << "//" << 3 << " "
                       << n + 6 << "//" << 3 << " "
                       << n + 5 << "//" << 3 << '\n';
            buf << "f " << n + 7 << "//" << 4 << " "
                       << n + 8 << "//" << 4 << " "
                       << n + 4 << "//" << 4 << " "
                       << n + 3 << "//" << 4 << '\n';
            buf << "f " << n + 3 << "//" << 5 << " "
                       << n + 4 << "//" << 5 << " "
                       << n + 2 << "//" << 5 << " "
                       << n + 1 << "//" << 5 << '\n';
            buf << "f " << n + 5 << "//" << 6 << " "
                       << n + 6 << "//" << 6 << " "
                       << n + 8 << "//" << 6 << " "
                       << n + 7 << "//" << 6 << '\n';
            buf << '\n';
            n = start + 8*(k+1);
        }
    }
    //os.write(buf.str().c_str(), buf.str().size());
    return n;
}

// Experimental writers here

static void extract_indices_boxes(const Tensor & box_set,
                                  const std::vector<index_t> & axes,
                                  std::vector<std::vector<index_t>> & boxes,
                                  std::vector<index_t> & indices_boxes)
{
    index_t max = *std::max_element(axes.begin(), axes.end());
    index_t N = box_set.idx[max].size();
    std::vector<index_t> it_box(max+1, 0);
    boxes.resize(N);
    indices_boxes.resize(N);
    for(index_t k = 0; k < N; ++k) {
        boxes[k] = it_box;
        box_set.advance(it_box);
    }
    std::iota(indices_boxes.begin(), indices_boxes.end(), 0);
    std::sort(indices_boxes.begin(), indices_boxes.end(), [&](index_t  u, index_t v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][boxes[u][axes[i]]];
                index_t vi = box_set.idx[axes[i]][boxes[v][axes[i]]];
                if(ui != vi) {
                    return ui < vi;
                }
            }
            return false;
    });
    auto last = std::unique(indices_boxes.begin(), indices_boxes.end(), [&](index_t u, index_t v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][boxes[u][axes[i]]];
                index_t vi = box_set.idx[axes[i]][boxes[v][axes[i]]];
                if(ui != vi) {
                    return false;
                }
            }
            return true;
    });
    indices_boxes.erase(last, indices_boxes.end());
}


static void extract_indices_normals(const Tensor & box_set,
                                    const std::vector<index_t> & indices_boxes,
                                    index_t level,
                                    std::vector<index_t> & indices_normals)
{
    indices_normals.resize(indices_boxes.size());
    for(index_t k = 0; k < indices_boxes.size(); ++k) {
        index_t ind = indices_boxes[k];
        for(index_t i = level+1; i < box_set.pos.size(); ++i) {
            ind = box_set.pos[i][ind];
        }
        indices_normals[k] = ind;
    }
}

template<typename Numeric>
void append_boxes(const Tensor_box<Numeric> & box_set, bool reversed,
                  std::vector<Numeric> & boxes)
{
    index_t dim = box_set.get_number_axes();
    index_t N   = box_set.get_number_elements();
    index_t prev_size = boxes.size();
    boxes.resize(prev_size + dim*N, 0);
    std::vector<index_t> it_box = box_set.begin();
    std::vector<Numeric> b(dim, 0);
    for(index_t i = 0; i < N; ++i) {
        get_box(box_set, it_box, b);
        if(reversed) {
            for(index_t j = 0; j < dim; ++j) {
                boxes[prev_size + dim*i + j] = b[dim-1-j];
            }
        } else {
            for(index_t j = 0; j < dim; ++j) {
                boxes[prev_size + dim*i + j] = b[j];
            }
        }
        box_set.advance(it_box);
    }
}


using ply_float = float;

// Print points with normals and faces to vectors 
template<typename Numeric>
void append_cubes(const Tensor_box<Numeric> & box_set,
                  const std::vector<index_t> & axes,
                  std::vector<std::array<ply_float, 3>> & points)
{
    if(box_set.get_number_elements() == 0) {
        return;
    }
    std::vector<std::vector<index_t>> boxes;
    std::vector<index_t> indices;
    extract_indices_boxes(box_set, axes, boxes, indices);

    index_t dim = axes.size();
    index_t bsize = boxes[0].size();
    std::vector<Numeric> b(bsize, 0);
    ply_float first_corner[3];
    ply_float second_corner[3];
    index_t vstart = static_cast<int32_t>(points.size());
    points.resize(points.size() + 8*indices.size());
    double delta = get_upper(box_set.data[axes[0]][0]) - get_lower(box_set.data[axes[0]][0]);
    for(index_t i = 0; i < dim; ++i) {
        for(index_t k = 0; k < box_set.data[axes[i]].size(); ++k) {
            double new_delta = get_upper(box_set.data[axes[i]][k]) - get_lower(box_set.data[axes[i]][k]);
            if(new_delta < delta) {
                delta = new_delta;
            }
        }
    }
    for(index_t k = 0; k < indices.size(); ++k) {
        get_box(box_set, boxes[indices[k]], b);
        for(size_t i = 0; i < dim; ++i) {
            first_corner[i] = get_lower(b[axes[i]]);
            second_corner[i] = get_upper(b[axes[i]]);
        }
        for(size_t i=dim; i<3; ++i) {
            first_corner[i] = 0;
            second_corner[i] = delta;
        }

        // Points and normals
        if(dim <= 2) {
            points[vstart + 4*k    ] = {first_corner[0], first_corner[1], 0};
            points[vstart + 4*k + 1] = {second_corner[0], first_corner[1], 0};
            points[vstart + 4*k + 2] = {second_corner[0], second_corner[1], 0};
            points[vstart + 4*k + 3] = {first_corner[0], second_corner[1], 0};
        } else {
            points[vstart + 8*k    ] = {first_corner[0], first_corner[1], first_corner[2]};
            points[vstart + 8*k + 1] = {second_corner[0], first_corner[1], first_corner[2]};
            points[vstart + 8*k + 2] = {first_corner[0], second_corner[1], first_corner[2]};
            points[vstart + 8*k + 3] = {second_corner[0], second_corner[1], first_corner[2]};
            points[vstart + 8*k + 4] = {first_corner[0], first_corner[1], second_corner[2]};
            points[vstart + 8*k + 5] = {second_corner[0], first_corner[1], second_corner[2]};
            points[vstart + 8*k + 6] = {first_corner[0], second_corner[1], second_corner[2]};
            points[vstart + 8*k + 7] = {second_corner[0], second_corner[1], second_corner[2]};
        }
    }
}

#pragma pack(push, 1)
struct ply_quad {
    unsigned char nf;
    int32_t v0;
    int32_t v1;
    int32_t v2;
    int32_t v3;
    ply_quad() {}
    ply_quad(int32_t b,
             int32_t c,
             int32_t d,
             int32_t e): nf(4), v0(b), v1(c), v2(d), v3(e) {}
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ply_triangle {
    unsigned char nf;
    int32_t v0;
    int32_t v1;
    int32_t v2;
    ply_triangle() {}
    ply_triangle(int32_t b,
                 int32_t c,
                 int32_t d): nf(3), v0(b), v1(c), v2(d) {}
};
#pragma pack(pop)


static void print_ply_cubes(std::vector<std::array<ply_float, 3>> points,
                            index_t dim,
                            std::ofstream & os)
{
    int32_t f_size = (dim <= 2)?
          static_cast<int32_t>(points.size()/4) :
          static_cast<int32_t>(points.size()/8*6);
    os << "ply\n";
    os << "format binary_little_endian 1.0\n";
    os << "element vertex " << std::to_string(points.size()) <<  "\n";
    os << "property float x\n";
    os << "property float y\n";
    os << "property float z\n";
    os << "element face " << std::to_string(f_size) <<  "\n";
    os << "property list uchar int vertex_index\n";
    os << "end_header\n";
    os.write(reinterpret_cast<char*>(points.data()), points.size()*3*sizeof(ply_float));
    std::vector<struct ply_quad> faces(f_size);
    if(dim <= 2) {
        for(int32_t k = 0; k < f_size; ++k) {
            faces[k] = ply_quad(4*k+0, 4*k+1, 4*k+2, 4*k+3);
        }
    } else {
        for(int32_t k = 0; k < f_size/6; ++k) {
            faces[6*k + 0] = ply_quad(8*k+0, 8*k+4, 8*k+6, 8*k+2);
            faces[6*k + 1] = ply_quad(8*k+3, 8*k+7, 8*k+5, 8*k+1);
            faces[6*k + 2] = ply_quad(8*k+0, 8*k+1, 8*k+5, 8*k+4);
            faces[6*k + 3] = ply_quad(8*k+6, 8*k+7, 8*k+3, 8*k+2);
            faces[6*k + 4] = ply_quad(8*k+2, 8*k+3, 8*k+1, 8*k+0);
            faces[6*k + 5] = ply_quad(8*k+4, 8*k+5, 8*k+7, 8*k+6);
        }
    }
    os.write(reinterpret_cast<char*>(faces.data()), faces.size()*(sizeof(unsigned char) + 4*sizeof(int32_t)));
}

// Print points with normals and faces to vectors 
template<typename Numeric>
index_t append_points_normals(Tensor_box<Numeric> & box_set,
                           const std::vector<std::array<ply_float,6>> & centers_normals,
                           const std::vector<index_t> & axes,
                           std::vector<std::array<ply_float, 6>> & points,
                           bool trim)
{
    if(box_set.get_number_elements() == 0) {
        return 0;
    }
    std::vector<index_t> it_box = box_set.begin();
    index_t Ne = box_set.get_number_elements();
    std::vector<std::vector<index_t>> boxes(Ne);
    // std::vector<index_t> indices_boxes;
    // std::vector<index_t> indices_normals;
    // index_t level = *std::max_element(axes.begin(), axes.end());
    // extract_indices_boxes(box_set, axes, boxes, indices_boxes);
    // extract_indices_normals(box_set, indices_boxes, level, indices_normals);

    for(index_t k = 0; k < Ne; ++k) {
        boxes[k] = it_box;
        box_set.advance(it_box);
    }

    const index_t dim = axes.size();
    const index_t bsize = boxes[0].size();
    const index_t vstart = static_cast<int32_t>(points.size());
    // points.resize(points.size() + indices_boxes.size());
    points.resize(points.size() + boxes.size());

    std::vector<Numeric> b(bsize, 0);
    ply_float normal[3];
    ply_float center[3];
    double direction[3];
    double offset;
    //#pragma omp parralel for shared(indices_boxes, boxes, indices_normals)
    index_t skipped = 0;
    std::vector<char> trimmed_points(boxes.size(), false);
    for(index_t k = 0; k < boxes.size(); ++k) {
        //get_box(box_set, boxes[indices_boxes[k]], b);
        get_box(box_set, boxes[k], b);
        ply_float N = 1;
        //offset = get_center(normals[indices_normals[k]][0]);
        //offset = get_center(normals[k][0]);
        for(size_t i = 0; i < dim; ++i) {
            center[i] = get_center(b[axes[i]]);
            //normal[i] = get_center(normals[indices_normals[k]][1+i]);
            normal[i] = centers_normals[k][3+i];
            //double bound = (b[axes[i]].upper + b[axes[i]].neg_lower)/2;
            //direction[i] = normal[i]>0? bound : -bound;
            //ply_float abs_normal = normal[i]>0? normal[i] : -normal[i];
            //if(abs_normal > N) { N = abs_normal;}
        }
        //ply_float iN = 1/N;
        
        for(size_t i=dim; i<3; ++i) {
            center[i] = 0;
        }
        for(size_t i = 0; i < dim; ++i) {
            center[i] += centers_normals[k][i];
        }
        // Points and normals
        points[vstart + k - skipped] = {center[0], center[1], center[2], normal[0], normal[1], normal[2]};
        // if(dim <= 2) {
        //     // Points and canonical normals
        //     points[vstart + k] = {center[0], center[1], center[2], 0, 0, 1};
        // } else  {
        //     //double N2 = 0;
        //     double N1 = 0;
        //     offset *= iN;
        //     for(size_t i = 0; i < dim; ++i) {
        //         normal[i] *= iN;
        //         N1 += normal[i]*direction[i];
        //         //N2 += normal[i]*normal[i];
        //     }
        //     if(N1 > 0) {
        //         offset /= N1;
        //         //offset /= N2;
        //         bool outside = (offset > 1 or offset < -1);
        //         if(offset > 1) {
        //             offset = 1;
        //         } else if(offset < -1) {
        //             offset = -1;
        //         }
        //         double shift[3];
        //         for(size_t i = 0; i < dim; ++i) {
        //             shift[i] = offset*direction[i];
        //             //shift[i] = offset*normal[i];
        //             //double bound = (b[axes[i]].upper + b[axes[i]].neg_lower)/2;
        //             //if(shift[i] > bound or shift[i] < -bound) {
        //             //    outside = true;
        //             //    //offset = 0;
        //             //    //break;
        //             //}
        //         }
        //         if(outside and trim) {
        //             //if(trim) {
        //             //    skipped++;
        //             //} else {
        //             //    points[vstart + k - skipped] = {center[0], center[1], center[2], 0, 0, 0};
        //             //    //points[vstart + k - skipped] = {center[0], center[1], center[2], normal[0], normal[1], normal[2]};
        //             //}
        //             trimmed_points[k] = true;
        //             skipped++;
        //             continue;
        //         }
        //         for(size_t i = 0; i < dim; ++i) {
        //             center[i] -= shift[i];
        //         }
        //     }
        //     // Points and normals
        //     points[vstart + k - skipped] = {center[0], center[1], center[2], normal[0], normal[1], normal[2]};
        //     //std::cout << center[0] << " " << center[1] << " " << center[2] << " "
        //     //          << normal[0]/N << " " << normal[1]/N << " " << normal[2]/N << "\n";
        // }
    }
    if(skipped > 0) {
        remove(box_set, trimmed_points);
    }
    return skipped;
}

template<typename Numeric>
void append_neighbours(const Tensor_box<Numeric> & box_set_0,
                       const index_t & start_0,
                       const index_t & level_0,
                       const Tensor_box<Numeric> & box_set_1,
                       const index_t & start_1,
                       const index_t & level_1,
                       std::vector<int32_t> & neighbours)
{
    index_t step = level_0 - level_1;
    index_t Ne = box_set_0.get_number_elements();
    index_t Na = box_set_0.get_number_axes();
    index_t block_size = 2*Na + 2*Na*(Na-1);

    if(box_set_0.get_number_elements() == 0 or box_set_1.get_number_elements() == 0) {
        return;
    }
    // check for disjointness
    for(index_t i = 0; i < Na; ++i) {
        small_index_t min0 = *std::min_element(box_set_0.idx[i].begin(), box_set_0.idx[i].end());
        small_index_t max0 = *std::max_element(box_set_0.idx[i].begin(), box_set_0.idx[i].end());
        small_index_t min1 = *std::min_element(box_set_1.idx[i].begin(), box_set_1.idx[i].end());
        small_index_t max1 = *std::max_element(box_set_1.idx[i].begin(), box_set_1.idx[i].end());
        if((min0 > 0 and ((min0-1)>>step) > max1) or ((max0+1)>>step) < min1) {
            return;
        }
    }

    std::vector<index_t> it_box = box_set_0.begin();
    for(index_t k = 0; k < Ne; ++k) {
        std::vector<small_index_t> indices(Na);
        std::vector<small_index_t> near_indices(Na);
        std::vector<index_t> it_near_box(Na);
        index_t pos = start_0 + it_box.back();
        int32_t pos_near_box;
        box_set_0.get_idx(it_box, indices);
        for(index_t i = 0; i < Na; ++i) {
            near_indices[i] = indices[i] >> step;
        }
        for(index_t i = 0; i < Na; ++i) {
            near_indices[i] = (indices[i] + 1) >> step;
            //std::cout << "Test indice : \n";
            //for(index_t j = 0; j < Na; ++j) { std::cout << near_indices[j] << "\n"; }
            bool found = box_set_1.contains(0, 0, near_indices, it_near_box);
            //std::cout << found << "\n";
            if(found) {
                pos_near_box = static_cast<int32_t>(start_1 + it_near_box.back());
                neighbours[pos*block_size + Na + i] = pos_near_box;
            }
            for(index_t j = 0; j < Na-i-1; ++j) {
                if(indices[i+1+j] > 0) {
                    near_indices[i+1+j] = (indices[i+1+j] - 1) >> step;
                    found = box_set_1.contains(0, 0, near_indices, it_near_box);
                    if(found) {
                        pos_near_box = static_cast<int32_t>(start_1 + it_near_box.back());
                        neighbours[pos*block_size + 2*Na + 2*Na*(Na-1)/2 + (i+1+j)*(i+j)/2 + i] = pos_near_box;
                    }
                }
                near_indices[i+1+j] = (indices[i+1+j] + 1) >> step;
                found = box_set_1.contains(0, 0, near_indices, it_near_box);
                if(found) {
                    pos_near_box = static_cast<int32_t>(start_1 + it_near_box.back());
                    neighbours[pos*block_size + 2*Na + 3*Na*(Na-1)/2 + (i+1+j)*(i+j)/2 + i] = pos_near_box;
                }
                near_indices[i+1+j] = indices[i+1+j] >> step;
            }
            // We add boxes in lower lexical order only if they are bigger
            if(step > 0 and indices[i] > 0) {
                near_indices[i] = (indices[i] - 1) >> step;
                found = box_set_1.contains(0, 0, near_indices, it_near_box);
                if(found) {
                    pos_near_box = static_cast<int32_t>(start_1 + it_near_box.back());
                    neighbours[pos*block_size + i] = pos_near_box;
                }
                for(index_t j = 0; j < Na-i-1; ++j) {
                    if(indices[i+1+j] > 0) {
                        near_indices[i+1+j] = (indices[i+1+j] - 1) >> step;
                        found = box_set_1.contains(0, 0, near_indices, it_near_box);
                        if(found) {
                            pos_near_box = static_cast<int32_t>(start_1 + it_near_box.back());
                            neighbours[pos*block_size + 2*Na + (i+1+j)*(i+j)/2 + i] = pos_near_box;
                        }
                    }
                    near_indices[i+1+j] = (indices[i+1+j] + 1) >> step;
                    found = box_set_1.contains(0, 0, near_indices, it_near_box);
                    if(found) {
                        pos_near_box = static_cast<int32_t>(start_1 + it_near_box.back());
                        neighbours[pos*block_size + 2*Na + 1*Na*(Na-1)/2 + (i+1+j)*(i+j)/2 + i] = pos_near_box;
                    }
                    near_indices[i+1+j] = indices[i+1+j] >> step;
                }
            }
            near_indices[i] = indices[i] >> step;
        }
        box_set_0.advance(it_box);
    }
}

static void append_triangles(std::vector<std::array<ply_float, 6>> & points,
                                   const std::vector<int32_t> & neighbours,
                                   index_t dim,
                                   std::vector<struct ply_triangle> & triangles)
{
    index_t block_size = 2*dim + 2*dim*(dim-1);
    for(index_t i = 0; i < neighbours.size()/block_size; ++i) {
        for(index_t j = 0; j < dim; ++j) {
            for(index_t k = 0; k < dim-j-1; ++k) {
                for(index_t m = 0; m < 4; ++m) {
                    int32_t n0, n1, n2, n3;
                    n0 = static_cast<int32_t>(i);
                    n1 = neighbours[i*block_size + dim*(m >> 1) + j];
                    n2 = neighbours[i*block_size + 2*dim + m*dim*(dim-1)/2 + (j+1+k)*(j+k)/2 + j];
                    n3 = neighbours[i*block_size + dim*(m % 2) + j+1+k];
                    if(n0 != -1 and n1 != -1 and n2 != -1 and n3 != -1) {
                        //TODO : project points on an interpolating plane
                        //and perform Delaunay for example, or choose max
                        //angle, or project each triple on plane orthogonal
                        //to normal
                        // choose smallest diagonal
                        ply_float V02[3] = {points[n2][0] - points[n0][0],
                                         points[n2][1] - points[n0][1],
                                         points[n2][2] - points[n0][2]};
                        ply_float V13[3] = {points[n3][0] - points[n1][0],
                                         points[n3][1] - points[n1][1],
                                         points[n3][2] - points[n1][2]};
                        ply_float D02 = V02[0]*V02[0] + V02[1]*V02[1] + V02[2]*V02[2];
                        ply_float D13 = V13[0]*V13[0] + V13[1]*V13[1] + V13[2]*V13[2];
                        if(D13 > D02 and n2 != n1 and n2 != n3) {
                            std::swap(n0, n1);
                            std::swap(n1, n2);
                            std::swap(n2, n3);
                        }
                        
                        // check orientation
                        ply_float V01[3] = {points[n1][0] - points[n0][0],
                                        points[n1][1] - points[n0][1],
                                        points[n1][2] - points[n0][2]};
                        ply_float V03[3] = {points[n3][0] - points[n0][0],
                                        points[n3][1] - points[n0][1],
                                        points[n3][2] - points[n0][2]};
                        ply_float N0[3] = {points[n0][3], points[n0][4], points[n0][5]};
                        ply_float N013[3] = {V01[1]*V03[2] - V01[2]*V03[1],
                                          V01[2]*V03[0] - V01[0]*V03[2],
                                          V01[0]*V03[1] - V01[1]*V03[0]};
                        ply_float det013 = N0[0]*N013[0] + N0[1]*N013[1] + N0[2]*N013[2];

                        // Collapse if angle greater than pi/4
                        //if(det013*det013 < 0.5*(N0[0]*N0[0]+N0[1]*N0[1]+N0[2]*N0[2])*
                        //                       (N013[0]*N013[0]+N013[1]*N013[1]+N013[2]*N013[2])) {
                        //    for(index_t p = 0; p < 6; ++p) {
                        //        points[n0][p] = (points[n0][p] + points[n2][p])/2;
                        //        points[n2][p] = points[n0][p];
                        //    }
                        //}

                        if(n1 == n2 or n2 == n3) {
                            if(det013 < 0) {
                                triangles.push_back(ply_triangle(n0, n3, n1));
                            } else {
                                triangles.push_back(ply_triangle(n0, n1, n3));
                            }
                            continue;
                        }
                        
                        //Collapse if short edge
                        //if(D02 < 0.01*D13 or D13 < 0.01*D02) {
                        //    for(index_t p = 0; p < 6; ++p) {
                        //        points[n0][p] = (points[n0][p] + points[n2][p])/2;
                        //        points[n2][p] = points[n0][p];
                        //    }
                        //    continue;
                        //}

                        ply_float V21[3] = {points[n1][0] - points[n2][0],
                                        points[n1][1] - points[n2][1],
                                        points[n1][2] - points[n2][2]};
                        ply_float V23[3] = {points[n3][0] - points[n2][0],
                                        points[n3][1] - points[n2][1],
                                        points[n3][2] - points[n2][2]};
                        ply_float N2[3] = {points[n2][3], points[n2][4], points[n2][5]};
                        ply_float N213[3] = {V21[1]*V23[2] - V21[2]*V23[1],
                                          V21[2]*V23[0] - V21[0]*V23[2],
                                          V21[0]*V23[1] - V21[1]*V23[0]};
                        ply_float det213 = N2[0]*N213[0] + N2[1]*N213[1] + N2[2]*N213[2];

                        //if(det213*det213 < 0.5*(N2[0]*N2[0]+N2[1]*N2[1]+N2[2]*N2[2])*
                        //                       (N213[0]*N213[0]+N213[1]*N213[1]+N213[2]*N213[2])) {
                        //    for(index_t p = 0; p < 6; ++p) {
                        //        points[n0][p] = (points[n0][p] + points[n2][p])/2;
                        //        points[n2][p] = points[n0][p];
                        //    }
                        //}

                        //if(det013 > 0 and det213 < 0) {
                        //    triangles.push_back(ply_triangle(n0, n1, n3));
                        //    triangles.push_back(ply_triangle(n2, n3, n1));
                        //} else if(det013 < 0 and det213 > 0){
                        //    triangles.push_back(ply_triangle(n0, n3, n1));
                        //    triangles.push_back(ply_triangle(n2, n1, n3));
                        //} else {
                        //    for(index_t p = 0; p < 6; ++p) {
                        //        points[n0][p] = (points[n0][p] + points[n2][p])/2;
                        //        points[n2][p] = points[n0][p];
                        //    }
                        //}

                        if(det013 >= 0) {
                            triangles.push_back(ply_triangle(n0, n1, n3));
                        } 
                        if(det013 < 0) {
                            triangles.push_back(ply_triangle(n0, n3, n1));
                        } 
                        if(det213 > 0) {
                            triangles.push_back(ply_triangle(n2, n1, n3));
                        } 
                        if(det213 <= 0) {
                            triangles.push_back(ply_triangle(n2, n3, n1));
                        } 

                        //if(det013 > 0 and det013 >= det213) {
                        //    triangles.push_back(ply_triangle(n0, n1, n3));
                        //} 
                        //if(det013 < 0 and det013 <= det213) {
                        //    triangles.push_back(ply_triangle(n0, n3, n1));
                        //} 
                        //if(det213 > 0 and det213 > det013) {
                        //    triangles.push_back(ply_triangle(n2, n1, n3));
                        //} 
                        //if(det213 < 0 and det213 < det013) {
                        //    triangles.push_back(ply_triangle(n2, n3, n1));
                        //} 
                        
                        //if(det013 > 0 and det213 < 0) {
                        //    triangles.push_back(ply_triangle(n0, n1, n3));
                        //    triangles.push_back(ply_triangle(n2, n3, n1));
                        //} else if(det013 < 0 and det213 > 0){
                        //    triangles.push_back(ply_triangle(n0, n3, n1));
                        //    triangles.push_back(ply_triangle(n2, n1, n3));
                        //} else if(det013 > det213) {
                        //    triangles.push_back(ply_triangle(n1, n2, n0));
                        //    triangles.push_back(ply_triangle(n3, n0, n2));
                        //} else {
                        //    triangles.push_back(ply_triangle(n1, n0, n2));
                        //    triangles.push_back(ply_triangle(n3, n2, n0));
                        //}
                    }
                }
            }
        }
    }
}

static void print_ply_points_normals(const std::vector<std::array<ply_float, 6>> & points,
                                     std::ofstream & os)
{
    os << "ply\n";
    os << "format binary_little_endian 1.0\n";
    os << "element vertex " << std::to_string(points.size()) <<  "\n";
    os << "property float x\n";
    os << "property float y\n";
    os << "property float z\n";
    os << "property float nx\n";
    os << "property float ny\n";
    os << "property float nz\n";
    os << "end_header\n";
    os.write(reinterpret_cast<const char*>(points.data()), points.size()*6*sizeof(ply_float));
}

static void print_ply_points_normals_triangles(const std::vector<std::array<ply_float, 6>> & points,
                                             const std::vector<struct ply_triangle> & triangles,
                                             std::ofstream & os)
{
    index_t f_size = triangles.size();
    os << "ply\n";
    os << "format binary_little_endian 1.0\n";
    os << "element vertex " << std::to_string(points.size()) <<  "\n";
    os << "property float x\n";
    os << "property float y\n";
    os << "property float z\n";
    os << "property float nx\n";
    os << "property float ny\n";
    os << "property float nz\n";
    os << "element face " << std::to_string(f_size) <<  "\n";
    os << "property list uchar int vertex_index\n";
    os << "end_header\n";
    os.write(reinterpret_cast<const char*>(points.data()), points.size()*6*sizeof(ply_float));
    os.write(reinterpret_cast<const char*>(triangles.data()), triangles.size()*(sizeof(unsigned char) + 3*sizeof(int32_t)));
}

static void print_ply_points_triangles(const std::vector<std::array<ply_float, 6>> & points,
                                             const std::vector<struct ply_triangle> & triangles,
                                             std::ofstream & os)
{
    index_t f_size = triangles.size();
    os << "ply\n";
    os << "format binary_little_endian 1.0\n";
    os << "element vertex " << std::to_string(points.size()) <<  "\n";
    os << "property float x\n";
    os << "property float y\n";
    os << "property float z\n";
    //os << "property double nx\n";
    //os << "property double ny\n";
    //os << "property double nz\n";
    os << "element face " << std::to_string(f_size) <<  "\n";
    os << "property list uchar int vertex_index\n";
    os << "end_header\n";
    std::vector<std::array<ply_float, 3>> simple_points(points.size());
    for(index_t i = 0; i < points.size(); ++i) {
        simple_points[i][0] = points[i][0];
        simple_points[i][1] = points[i][1];
        simple_points[i][2] = points[i][2];
    }
    os.write(reinterpret_cast<const char*>(simple_points.data()), simple_points.size()*3*sizeof(ply_float));
    os.write(reinterpret_cast<const char*>(triangles.data()), triangles.size()*(sizeof(unsigned char) + 3*sizeof(int32_t)));
}

// Print the box to an obj file
template<typename Numeric>
index_t append_obj_normals(std::ostream& os, const Tensor_box<Numeric> & box_set,
                           const std::vector<std::array<Numeric,3>> & normals,
                           const std::vector<index_t> axes, index_t start)
{
    if(box_set.get_number_elements() == 0) {
        return start;
    }
    index_t max = *std::max_element(axes.begin(), axes.end());
    index_t N = box_set.idx[max].size();
    std::vector<index_t> it_box(max+1, 0);
    std::vector<std::vector<index_t>> boxes;
    std::vector<index_t> indices(N);
    std::iota(indices.begin(), indices.end(), 0);
    boxes.resize(N);
    for(index_t k = 0; k < N; ++k) {
        boxes[k] = it_box;
        box_set.advance(it_box);
    }
    std::sort(indices.begin(), indices.end(), [&](index_t  u, index_t v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][boxes[u][axes[i]]];
                index_t vi = box_set.idx[axes[i]][boxes[v][axes[i]]];
                if(ui != vi) {
                    return ui < vi;
                }
            }
            return false;
    });
    auto last = std::unique(indices.begin(), indices.end(), [&](index_t u, index_t v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][boxes[u][axes[i]]];
                index_t vi = box_set.idx[axes[i]][boxes[v][axes[i]]];
                if(ui != vi) {
                    return false;
                }
            }
            return true;
    });
    indices.erase(last, indices.end());


    //typename Numeric::base_type first_corner[3];
    //typename Numeric::base_type second_corner[3];
    //index_t dim = box_set.get_number_axes() < 3 ? box_set.get_number_axes() : 3;
    //index_t N = box_set.idx[dim-1].size();
    //std::vector<Numeric> b(dim, 0);
    //std::vector<index_t> it_box(dim, 0);

    //std::stringstream buf;
    //buf.str().reserve(N*dim*200);
    //buf << std::setprecision(std::numeric_limits<double>::digits10);
    auto& buf = os;
    buf << std::setprecision(std::numeric_limits<double>::digits10);

    index_t dim = axes.size();
    std::vector<Numeric> b(max+1, 0);
    std::array<double, 3> normal;
    index_t n = start;
    double first_corner[3];
    double second_corner[3];
    double center[3];
    for(index_t k = 0; k < indices.size(); ++k) {
        get_box(box_set, boxes[indices[k]], b);
        //normal = normals[indices[k]];
        //box_set.advance(it_box);

        for(size_t i = 0; i < dim; ++i) {
            first_corner[i] = get_lower(b[axes[i]]);
            second_corner[i] = get_upper(b[axes[i]]);
            normal[i] = get_center(normals[indices[k]][i]);
            center[i] = get_lower(b[axes[i]]);
        }
        for(size_t i=dim; i<3; ++i) {
            first_corner[i] = 0;
            second_corner[i] = get_upper(b[axes[0]]) - get_lower(b[axes[0]]);
        }

        /* write the label of each box as comment on os
        for(const auto& i:b) {
            buf << "# " << i.get_log_label({0.5, 1.5}) << '\n';
        }
        */

        if(dim == 2) {
            buf << "v " << first_corner[0] << " " << first_corner[1] << " " << 0 << '\n';
            buf << "v " << second_corner[0] << " " << first_corner[1] << " " << 0 << '\n';
            buf << "v " << second_corner[0] << " " << second_corner[1] << " " << 0 << '\n';
            buf << "v " << first_corner[0] << " " << second_corner[1] << " " << 0 << '\n';
            buf << '\n';

            // Face and normal
            buf << "f " << n + 1 << "//" << 6 << " "
                       << n + 2 << "//" << 6 << " "
                       << n + 3 << "//" << 6 << " "
                       << n + 4 << "//" << 6 << '\n';
            n = start + 4*(k+1);
        } else  {
            // Normal
            buf << "vn " << normal[0] << " " << normal[1] << " " << normal[2] << '\n';
            buf << '\n';

            // Centers
            buf << "v " << center[0] << " " << center[1] << " " << center[2] +0.0001<< '\n';
            buf << "v " << center[0]-0.0001 << " " << center[1] << " " << center[2] << '\n';
            buf << "v " << center[0]+0.0001 << " " << center[1] << " " << center[2] << '\n';
            buf << '\n';

            index_t m = 6 + n/3 + 1;

            // Faces and normals
            buf << "f " << n + 1 << "//" << m << " "
                        << n + 2 << "//" << m << " "
                        << n + 3 << "//" << m << '\n';
            n = start + 3*(k+1);

            // buf << "f " << n + 1 << "//" << m << " "
            //             << n + 5 << "//" << m << " "
            //             << n + 7 << "//" << m << " "
            //             << n + 3 << "//" << m << '\n';
            // buf << "f " << n + 4 << "//" << m << " "
            //             << n + 8 << "//" << m << " "
            //             << n + 6 << "//" << m << " "
            //             << n + 2 << "//" << m << '\n';
            // buf << "f " << n + 1 << "//" << m << " "
            //             << n + 2 << "//" << m << " "
            //             << n + 6 << "//" << m << " "
            //             << n + 5 << "//" << m << '\n';
            // buf << "f " << n + 7 << "//" << m << " "
            //             << n + 8 << "//" << m << " "
            //             << n + 4 << "//" << m << " "
            //             << n + 3 << "//" << m << '\n';
            // buf << "f " << n + 3 << "//" << m << " "
            //             << n + 4 << "//" << m << " "
            //             << n + 2 << "//" << m << " "
            //             << n + 1 << "//" << m << '\n';
            // buf << "f " << n + 5 << "//" << m << " "
            //             << n + 6 << "//" << m << " "
            //             << n + 8 << "//" << m << " "
            //             << n + 7 << "//" << m << '\n';
            // buf << '\n';
            // n = start + 8*(k+1);
        }
    }
    //os.write(buf.str().c_str(), buf.str().size());
    return n;
}

// Print the box to an obj file
template<typename Numeric>
index_t append_obj_points(std::ostream& os, const Tensor_box<Numeric> & box_set, const std::vector<index_t> axes, index_t start)
{
    double center[3];

    if(box_set.get_number_elements() == 0) {
        return start;
    }
    index_t max = *std::max_element(axes.begin(), axes.end());
    index_t N = box_set.idx[max].size();
    std::vector<index_t> it_box(max+1, 0);
    std::vector<std::vector<index_t>> boxes;
    boxes.resize(N);
    for(index_t k = 0; k < N; ++k) {
        boxes[k] = it_box;
        box_set.advance(it_box);
    }
    std::sort(boxes.begin(), boxes.end(), [&](const std::vector<index_t> & u, const std::vector<index_t> & v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][u[axes[i]]];
                index_t vi = box_set.idx[axes[i]][v[axes[i]]];
                if(ui != vi) {
                    return ui < vi;
                }
            }
            return false;
    });
    auto last = std::unique(boxes.begin(), boxes.end(), [&](const std::vector<index_t> & u, const std::vector<index_t> & v) {
            for(index_t i = 0; i < axes.size(); ++i) {
                index_t ui = box_set.idx[axes[i]][u[axes[i]]];
                index_t vi = box_set.idx[axes[i]][v[axes[i]]];
                if(ui != vi) {
                    return false;
                }
            }
            return true;
    });
    boxes.erase(last, boxes.end());


    //typename Numeric::base_type first_corner[3];
    //typename Numeric::base_type second_corner[3];
    //index_t dim = box_set.get_number_axes() < 3 ? box_set.get_number_axes() : 3;
    //index_t N = box_set.idx[dim-1].size();
    //std::vector<Numeric> b(dim, 0);
    //std::vector<index_t> it_box(dim, 0);

    //std::stringstream buf;
    //buf.str().reserve(N*dim*200);
    //buf << std::setprecision(std::numeric_limits<double>::digits10);
    auto& buf = os;
    buf << std::setprecision(std::numeric_limits<double>::digits10);

    index_t dim = axes.size();
    std::vector<Numeric> b(max+1, 0);
    index_t n = start;
    for(index_t k = 0; k < boxes.size(); ++k) {
        get_box(box_set, boxes[k], b);
        //box_set.advance(it_box);

        for(size_t i = 0; i < dim; ++i) {
            center[i] = get_lower(b[axes[i]]);
        }
        for(size_t i=dim; i<3; ++i) {
            center[i] = 0;
        }

        /* write the label of each box as comment on os
        for(const auto& i:b) {
            buf << "# " << i.get_log_label({0.5, 1.5}) << '\n';
        }
        */

        if(dim == 2) {
            buf << "v " << center[0] << " " << center[1] << " " << 0 << '\n';
            buf << '\n';
            n = start + (k+1);
        } else  {
            buf << "v " << center[0] << " " << center[1] << " " << center[2] << '\n';
            buf << '\n';
            n = start + (k+1);
        }
    }
    //os.write(buf.str().c_str(), buf.str().size());
    return n;
}

template<typename Numeric>
std::ostream& operator<<(std::ostream& os, const Tensor_box<Numeric> & box_set)
{
    std::vector<index_t> axes(box_set.get_number_axes());
    std::iota(axes.begin(), axes.end(), 0);
    print_normal_obj(os);
    append_obj(os, box_set, axes, 0);
    return os;
}

// template Tensor_box<Interval> init_tensor_box(std::vector<Interval> box);
//#include "../interval.hpp"
//template<> std::ostream& operator<< <Interval>(std::ostream&, const Tensor_box<Interval>& box);
#endif

