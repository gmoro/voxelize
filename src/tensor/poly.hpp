/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#ifndef TENSOR_POLY_HPP
#define TENSOR_POLY_HPP
#include "tensor.hpp"
#include "box.hpp"
#include <iostream>
#include <algorithm>
#include <numeric>
#include <array>
#include <cfenv>
#include "../schemes/shift.hpp"
//#include <omp.h>

//#define OMP_NESTED

template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;

// forward declaration
template<typename Numeric> int rounding_mode();

template<typename Numeric>
class Tensor_poly : public Tensor
{
    public:
        std::vector<Numeric> data;

        Tensor_poly() {}
        Tensor_poly(const Tensor & t) : Tensor(t) {} 
};

template<typename Numeric1, typename Numeric2>
void move(Tensor_poly<Numeric1> & p, Tensor_poly<Numeric2> & q)
{
    std::swap(p.pos, q.pos);
    std::swap(p.idx, q.idx);
    q.data.resize(p.data.size(),0);
    for(index_t i = 0; i < p.data.size(); ++i) {
        q.data[i] = Numeric2(p.data[i]);
    }
}

// Transform this as a proper constructor
template<typename Numeric>
Tensor_poly<Numeric> make_tensor_poly(const std::vector<Numeric> & coefficients,
                                      const std::vector<std::vector<index_t> > & exponents)
{
    index_t N = coefficients.size();
    std::vector<index_t> p(N);
    std::iota(p.begin(), p.end(), 0);
    std::sort(p.begin(), p.end(), [&](index_t i, index_t j){return exponents[i] < exponents[j];});
    Tensor_poly<Numeric> result;
    if(N == 0) {
        return result;
    }
    index_t dim = exponents[0].size();

    result.pos = std::vector<std::vector<index_t> >(dim, {0});
    result.idx = std::vector<std::vector<small_index_t> >(dim);
    result.data = std::vector<Numeric>();
    result.pos[0].push_back(0);
    index_t k = 0;
    while(k < N) {
        for(index_t j = 0; j < dim; ++j) {
            index_t dp = result.pos[j].end()[-1] - result.pos[j].end()[-2];
            for(index_t i = dp; i <= exponents[p[k]][j]; ++i) {
                result.pos[j].back()++;
                result.idx[j].push_back(i);
                if(j < dim-1) {
                    index_t lp = result.pos[j+1].back();
                    result.pos[j+1].push_back(lp);
                } else {
                    result.data.push_back(0);
                }
            }
        }
        result.data.back() = coefficients[p[k]];
        k++;
    }
    return result;
}
                                        

// TODO : handle the case when number of value coordinates < number of polynomial variables
// TODO : handle empty polynomials
// TODO : handle begin and end iterator for values to evaluate
template<template<typename Numeric> class Scheme, typename Numeric>
void evaluate(const Tensor_poly<Numeric> & polynomial,
              const Tensor_box <Numeric> & values,
                    std::vector<Numeric> & result)
{
    // Initialize temp vectors
    const index_t N = polynomial.get_number_axes();
    index_t M = 0;
    for(index_t i = 0; i < N; ++i) { 
        index_t tmp = values.idx[i].size() * (polynomial.pos[N-1-i].size() - 1)
                      * Scheme<Numeric>::dimension(i+1);
        if(tmp > M) { M = tmp; }
    }
    std::vector<Numeric> data_tmp(M, 0);
    result.resize(M, 0);

    // Initialize rounding mode
    const int round_state = std::fegetround();
    std::fesetround(Scheme<Numeric>::rounding_mode);
                    
    // Evaluation loop over the variable axes
    auto it_input = polynomial.data.cbegin();
    for(index_t i = 0; i < N; ++i) {
        const index_t Nidx = polynomial.idx[N-1-i].size();
        const index_t Npos = polynomial.pos[N-1-i].size() - 1;
        // The evalution output has output_dim elements
        const index_t input_dim = Scheme<Numeric>::dimension(i);
        const index_t output_dim = Scheme<Numeric>::dimension(i+1);
        const index_t Npos_val = values.pos[i].size() - 1;
        
        // Loop over the slices of values
        #pragma omp parallel for shared(values, polynomial, data_tmp) schedule(dynamic) collapse(2)
        for(index_t j = 0; j < Npos_val; ++j) {
            //const index_t & u = values.pos[i][j];
            //const index_t & v = values.pos[i][j+1];
            //const index_t num_val_in = std::min(v-u, values.data[i].size());
            //const index_t num_val_out = v-u;
            //std::vector<Numeric> vals(num_val_in, 0);
            //// Populate values to evaluate
            //// The scheme shift only use one value so data size could be less than v-u
            //for(index_t k = 0; k < num_val_in; ++k) {
            //    index_t idxk = values.idx[i][u+k];
            //    vals[k] = values.data[i][idxk];
            //}

            // Loop over the univariate polynomials
            //if(values.pos[i].size() <= 2) {
            //    std::cout << "main: " << omp_get_num_threads() << "\n";
            //}
            //#pragma omp parallel for default(shared)//shared(vals, data_tmp)// if(Npos*
                    //shared(it_input, vals, polynomial, data_tmp) schedule(dynamic) if(values.pos[i].size() <= 2)
            for(index_t k = 0; k < Npos; ++k) {
                const index_t & u = values.pos[i][j];
                const index_t & v = values.pos[i][j+1];
                const index_t num_val_in = std::min(v-u, values.data[i].size());
                const index_t num_val_out = v-u;
                std::vector<Numeric> vals(num_val_in, 0);
                // Populate values to evaluate
                // The scheme shift only use one value so data size could be less than v-u
                for(index_t m = 0; m < num_val_in; ++m) {
                    index_t idxk = values.idx[i][u+m];
                    vals[m] = values.data[i][idxk];
                }
                //if(values.pos[i].size() <= 2) {
                //    std::cout << "below: " << omp_get_num_threads() << "\n";
                //}
                const index_t pol_start = input_dim*(j*Nidx + polynomial.pos[N-1-i][k]);
                const index_t pol_size = polynomial.pos[N-1-i][k+1] - polynomial.pos[N-1-i][k];
                //if(values.pos[i].size() <= 2) { std::cout << pol_size << "\n"; }
                const index_t output_start = output_dim*(u*Npos + k*num_val_out);
                Scheme<Numeric>::evaluate(it_input + pol_start, pol_size,
                                                 vals.cbegin(), num_val_out, i,
                                                 data_tmp.begin() + output_start);
            }
        }

        if(Npos == 1) {
            std::swap(data_tmp, result);
        } else {
            // Transpose the last two axes of the tensor data
            // Keeping the output values of evaluation adjacent
            // Parallelization through omp slows down the execution
            //#pragma omp parallel for shared(values, data_tmp) schedule(dynamic) collapse(2)
            for(index_t j = 0; j < values.pos[i].size()-1; ++j) {
                //#pragma omp parallel for shared(values, data_tmp) schedule(dynamic) if(values.pos[i].size() <= 2)
                for(index_t k = 0; k < Npos; ++k) {
                    const index_t & u = values.pos[i][j];
                    const index_t & v = values.pos[i][j+1];
                    for(index_t h = 0; h < v-u; ++h) {
                        std::copy(data_tmp.begin() + output_dim*(u*Npos + k*(v-u) + h),
                                  data_tmp.begin() + output_dim*(u*Npos + k*(v-u) + h+1),
                                  result.begin()   + output_dim*(u*Npos + h*Npos  + k));
                        //for(index_t m = 0; m < output_dim; ++m) {
                        //    result[output_dim*(u*Npos + h*Npos  + k) + m] =
                        //  data_tmp[output_dim*(u*Npos + k*(v-u) + h) + m];
                        //}
                    }
                }
            }
        }
            
        it_input = result.cbegin();
    }
    std::fesetround(round_state);
    index_t dim = Scheme<Numeric>::dimension(N);
    result.resize(dim*values.idx[N-1].size(), 0);
}

static void binomial(const std::vector<index_t> & indices, index_t k,
                     std::vector<std::vector<index_t>> & result_perm)
{
    index_t n = indices.size();
    std::vector<char> permutation(n, 0);
    vec_iterator<char> it_begin = permutation.begin();
    vec_iterator<char> it_end = permutation.end();
    std::fill(it_end - k , it_end, 1);
    result_perm.resize(0);
    do {
        result_perm.push_back(std::vector<index_t>(0));
        for(index_t i = 0; i < n; ++i) {
            if(permutation[i] == 1) {
                result_perm.back().push_back(indices[i]);
            }
        }
    } while(std::next_permutation(it_begin, it_end));
}

static void get_partitions(index_t n, index_t k,
                           std::vector<std::vector<index_t>> & result_part,
                           std::vector<std::vector<index_t>> & result_comp)
{
    std::vector<char> permutation(n, 0);
    vec_iterator<char> it_begin = permutation.begin();
    vec_iterator<char> it_end = permutation.end();
    std::fill(it_end - k , it_end, 1);
    result_part.resize(0);
    result_comp.resize(0);
    do {
        result_part.push_back(std::vector<index_t>(0));
        result_comp.push_back(std::vector<index_t>(0));
        for(index_t i = 0; i < n; ++i) {
            if(permutation[i] == 1) {
                result_part.back().push_back(i);
            } else {
                result_comp.back().push_back(i);
            }
        }
    } while(std::next_permutation(it_begin, it_end));
}

template<typename Numeric>
void fill_jet(const std::vector<std::vector<Numeric>> & jets,
              const std::vector<Numeric> & weights,
              const std::vector<index_t> & col_ind,
              const std::vector<std::vector<index_t>> & permutations,
              const index_t & i, const index_t & block_dim,
              std::vector<Numeric> & result_jet)
{
    typename Numeric::base_type Mn(0);
    typename Numeric::base_type Md(1);
    typename Numeric::base_type Dn(0);
    typename Numeric::base_type Dd(1);
    //std::vector<typename Numeric::base_type> Dn(permutations.size());
    //std::vector<typename Numeric::base_type> Dd(permutations.size());
    //std::vector<std::vector<Numeric>> normals(permutations.size(), std::vector<Numeric>(col_ind.size(), 0));
    std::vector<Numeric> best_normal(col_ind.size(), 0);
    std::vector<Numeric> normal(col_ind.size(), 0);
    std::vector<index_t> row_ind(weights.size() + 1);
    std::iota(row_ind.begin(), row_ind.end(), i*block_dim);
    //index_t best = 0;
    bool unstable = true;
    result_jet.resize(block_dim, 0);
    Mn = get_min_abs(jets[col_ind.back()][i*block_dim]);
    Md = 0;
    for(index_t k = 0; k < weights.size(); ++k) {
        Md += get_max_abs(jets[col_ind.back()][i*block_dim+1+k]) * get_upper(weights[k]);
    }
    //#pragma omp parallel for shared(permutations, Dn, Dd, normals)
    std::vector<index_t> current_permutation(col_ind.size()-1);
    std::vector<Numeric> new_jet(weights.size() + 1, 0);
    for(index_t j = 0; j < permutations.size(); ++j) {
        for(index_t k = 0; k < permutations[j].size(); ++k) {
            current_permutation[k] = permutations[j][k] + i*block_dim;
        }
        left_kernel(jets, col_ind, current_permutation, normal);
        if(contains_zero(normal.back())) {
            continue;
        }
        product_row_matrix(normal, jets, col_ind, row_ind, new_jet);
        Dn = get_min_abs(new_jet[0]);
        Dd = 0;
        for(index_t k = 0; k < weights.size(); ++k) {
            Dd += get_max_abs(new_jet[1+k]) * get_upper(weights[k]);
        }
    //}
    //for(index_t j = 0; j < permutations.size(); ++j) {
        if( (Md == 0 and Dd == 0 and Dn > Mn) or Dn*Md > Mn*Dd) {
            unstable = false;
            Mn = Dn;
            Md = Dd;
            //best = j;
            best_normal = normal;
        }
    }
    if(unstable) {
        //std::cout << "Went to unstable" << "\n";
        std::copy(jets[col_ind.back()].begin() + i*block_dim, jets[col_ind.back()].begin() + (i+1)*block_dim, result_jet.begin());
        return;
    }
    std::vector<index_t> row(block_dim);
    std::iota(row.begin(), row.end(), i*block_dim);
    product_row_matrix(best_normal, jets, col_ind, row, result_jet);
}

//template<template<typename Numeric> class Scheme, typename Numeric>
template<template<typename Numeric> class Scheme, typename Numeric, typename std::enable_if<Scheme<Numeric>::has_gradient,int>::type = 0>
void is_non_zero_modulo(const std::vector<std::vector<Numeric>> & jets, 
                        index_t ind,
                        const std::vector<index_t> & ind_equalities,
                        const Tensor_box<Numeric> & box_set,
                        const vec_iterator<char> it_result) {
    const index_t dim = box_set.get_number_axes();
    const index_t N   = box_set.get_number_elements();
    const index_t block_dim = Scheme<Numeric>::dimension(dim);
    //std::vector<index_t> it_boxes(dim, 0);
    std::vector<Numeric> delta_box(dim, 1);

    const int round_state = std::fegetround();
    std::fesetround(Scheme<Numeric>::rounding_mode);
    //std::vector<Numeric> b(dim, 0);
    std::vector<index_t> gradient_indices(dim);
    std::vector<std::vector<index_t>> permutations;
    for(index_t i = 0; i < dim; ++i) {
        gradient_indices[i] = 1+i;
        index_t k = box_set.idx[i][0];
        delta(box_set.data[i][k], delta_box[i]);
    }
    std::vector<index_t> col_ind = ind_equalities;
    if(col_ind.size() > dim) {
        col_ind.resize(dim, 0);
    }
    // to avoid to test too many points
    // if (col_ind.size() > 3 and dim - col_ind.size() > 3) {
    //     col_ind.resize(3, 0);
    // }
    col_ind.push_back(ind);
    index_t col_dim = col_ind.size();
    binomial(gradient_indices, col_dim - 1, permutations);
    #pragma omp parallel for shared(delta_box, permutations, col_ind)
    for(index_t i = 0; i < N; ++i) {
        std::vector<Numeric> current_jet(block_dim, 0);
        fill_jet(jets, delta_box, col_ind, permutations, i, block_dim, current_jet);
        //get_box(box_set, it_boxes, b);
        it_result[i] = Scheme<Numeric>::is_non_zero(current_jet.cbegin(), delta_box);
        //if(it_result[i] != 0) {
        //    it_result[i] = sign(jets[ind][i*block_dim]);
        //}
        //box_set.advance(it_boxes);
    }
    std::fesetround(round_state);
} 

template<template<typename Numeric> class Scheme, typename Numeric, typename std::enable_if<!Scheme<Numeric>::has_gradient,int>::type = 0>
void is_non_zero_modulo(const std::vector<std::vector<Numeric>> & jets, 
                        index_t ind,
                        const std::vector<index_t> & ind_equalities,
                        const Tensor_box<Numeric> & box_set,
                        const vec_iterator<char> result) {
    index_t N   = box_set.get_number_elements();
    for(index_t i = 0; i < N; ++i) {
        result[i] = sign(jets[ind][i]);
    }
}

template<template<typename Numeric> class Scheme, typename Numeric, typename std::enable_if<Scheme<Numeric>::has_gradient,int>::type = 0>
void is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                 const Tensor_box<Numeric> & box_set,
                 const vec_iterator<char> it_result) {
    const index_t dim = box_set.get_number_axes();
    const index_t N   = box_set.get_number_elements();
    const index_t block_dim = Scheme<Numeric>::dimension(dim);
    std::vector<index_t> it_boxes(dim, 0);
    if(N == 0) {
        return;
    }

    const int round_state = std::fegetround();
    std::fesetround(Scheme<Numeric>::rounding_mode);
    std::vector<Numeric> delta_box(dim, 0);
    for(index_t i = 0; i < dim; ++i) {
        index_t k = box_set.idx[i][0];
        delta(box_set.data[i][k], delta_box[i]);
    }
    #pragma omp parallel for shared(delta_box)
    for(index_t i = 0; i < N; ++i) {
        it_result[i] = Scheme<Numeric>::is_non_zero(it_jets + i*block_dim, delta_box);
    }
    std::fesetround(round_state);
} 

template<template<typename Numeric> class Scheme, typename Numeric, typename std::enable_if<!Scheme<Numeric>::has_gradient,int>::type = 0>
void is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                 const Tensor_box<Numeric> & box_set,
                 const vec_iterator<char> result) {
    index_t N   = box_set.get_number_elements();
    for(index_t i = 0; i < N; ++i) {
        result[i] = sign(it_jets[i]);
    }
}

template<template<typename Numeric> class Scheme, typename Numeric>
void is_zero(const const_vec_iterator<Numeric> it_jets, 
             const Tensor_box<Numeric> & box_set,
             const vec_iterator<char> it_zero) {
    index_t dim = box_set.get_number_axes();
    index_t N   = box_set.get_number_elements();
    index_t block_dim = Scheme<Numeric>::dimension(dim);
    std::vector<index_t> it_boxes(dim, 0);
    if(N == 0) {
        return;
    }

    const int round_state = std::fegetround();
    std::fesetround(Scheme<Numeric>::rounding_mode);
    std::vector<Numeric> delta_box(dim, 0);
    for(index_t i = 0; i < dim; ++i) {
        index_t k = box_set.idx[i][0];
        delta(box_set.data[i][k], delta_box[i]);
    }
    for(index_t i = 0; i < N; ++i) {
        //get_box(box_set, it_boxes, b);
        it_zero[i] = Scheme<Numeric>::is_zero(it_jets + i*block_dim, delta_box);
        box_set.advance(it_boxes);
    }
    std::fesetround(round_state);
} 

template<template<typename Numeric> class Scheme, typename Numeric>
void get_normals(const std::vector<std::vector<Numeric>> jets, 
                 index_t begin, index_t end,
                 const std::vector<char> & signs,
                 index_t dim,
                 const std::vector<index_t> & axes,
                 std::vector<std::array<Numeric, 4>> & normals) {
    const index_t block_dim = Scheme<Numeric>::dimension(dim);
    normals.resize(end - begin, {0,0,0,0});

    const int round_state = std::fegetround();
    std::fesetround(Scheme<Numeric>::rounding_mode);
    std::vector<index_t> ind_zero(0);
    std::vector<index_t> ind_nonaxes(0);
    for(index_t i = 0; i < signs.size(); ++i) {
        if(signs[i] == 0) {
            ind_zero.push_back(i);
        }
    }
    if(dim - ind_zero.size() != 2 or ind_zero.size() == 0) {
        return;
    }
    // get indices of non axes coordinates
    index_t offset = 0;
    ind_nonaxes.resize(dim - 3);
    // remove axes[0], axes[1] and axes[2] from the indices
    // they must be all different
    for(index_t i = 0; i < dim; ++i) {
        if(i == axes[0] or i == axes[1] or i == axes[2]) {
            offset += 1;
        } else {
            ind_nonaxes[i - offset] = 1+i;
        }
    }
    
    #pragma omp parallel for shared(ind_zero, ind_nonaxes, normals)
    for(index_t i = 0; i < end-begin; ++i) {
        std::vector<Numeric> ker(dim-2, 0);
        std::vector<index_t> ind_nonaxes_current(dim - 3);
        for(index_t j = 0; j < dim - 3; ++j) {
            ind_nonaxes_current[j] = ind_nonaxes[j] + (begin+i)*block_dim;
        }
        left_kernel(jets, ind_zero, ind_nonaxes_current, ker);
        std::vector<index_t> ind_current(4);
        ind_current[0] = (begin+i)*block_dim;
        for(index_t j = 0; j < 3; ++j) {
            ind_current[1+j] = 1 + axes[j] + (begin+i)*block_dim;
        }
        product_row_matrix(ker, jets, ind_zero, ind_current, normals[i]);
    }
    std::fesetround(round_state);
} 

template<typename Numeric>
void shift_center(const std::vector<Numeric> & evals,
                  const std::vector<std::vector<Numeric>> & slopes,
                  const std::vector<std::vector<Numeric>> & mid_gradient,
                  const std::vector<Numeric> & delta_box,
                  const std::vector<std::vector<index_t>> & permutations,
                  const std::vector<std::vector<index_t>> & comp_permutations,
                  std::vector<Numeric> & result)
{
    index_t dim = delta_box.size();
    index_t Nproj = delta_box.size() - evals.size() + 1;
    std::vector<index_t> pol_ind(evals.size());
    std::iota(pol_ind.begin(), pol_ind.end(), 0);
    std::vector<index_t> var_ind(evals.size() + 1);
    std::iota(var_ind.begin(), var_ind.end(), 0);
    typename Numeric::base_type r(2);
    typename Numeric::base_type current_r(0);
    typename Numeric::base_type max_proj(0);
    Numeric current_eval = 0;
    bool valid = false;
    std::vector<Numeric> normal(evals.size(), 0);
    std::vector<std::vector<Numeric>> matrix(evals.size(),
                                             std::vector<Numeric>(delta_box.size() + 1, 0));
    //std::vector<std::vector<Numeric>> matrix(delta_box.size() + 1,
    //                                         std::vector<Numeric>(evals.size(), 0));
    result.resize(dim, 0);
    std::vector<Numeric> candidate(evals.size(), 0);
    std::vector<Numeric> projection(Nproj, 0);
    std::vector<Numeric> radii(dim, 0);
    for(index_t j = 0; j < permutations.size(); ++j) {
        // Choose corner
        for(index_t n = 0; n < dim; ++n) {
            rad(delta_box[n], radii[n]);
        }
        //left_kernel(slopes, pol_ind, comp_permutations[j], normal);
        //left_kernel(mid_gradient, pol_ind, comp_permutations[j], normal);
        //product_row_matrix(normal, mid_gradient, pol_ind, permutations[j], projection);
        //max_proj = 0;
        //for(index_t n = 0; n < projection.size(); ++n) {
        //    if(get_max_abs(projection[n]) > max_proj) {
        //        max_proj = get_max_abs(projection[n]);
        //    }
        //}
        left_kernel(mid_gradient, pol_ind, comp_permutations[j], normal);
        product_row_matrix(normal, slopes, pol_ind, permutations[j], projection);
        product_row_vector(normal, evals, pol_ind, current_eval);
        typename Numeric::base_type sum_proj = 0;
        for(index_t n = 0; n < projection.size(); ++n) {
            sum_proj += get_min_abs(projection[n]) * delta_box[permutations[j][n]].upper;
        }
        for(index_t n = 0; n < projection.size(); ++n) {
            //TODO: change/explain magic number 0.05
            //if(get_max_abs(projection[n]) < max_proj * 0.05) {
            //if(contains_zero(projection[n]) and sum_proj < get_max_abs(current_eval)) {
            if(contains_zero(projection[n])) {
                radii[permutations[j][n]] = 0;
            } else if(get_far_from_zero(projection[n]) < 0) {
                neg(radii[permutations[j][n]], radii[permutations[j][n]]);
            }
        }
        // Compute minimal Linfinity norm with weight
        for(index_t m = 0; m < evals.size(); ++m) {
            matrix[m][0] = 0;
            for(index_t n = 0; n < permutations[j].size(); ++n) {
                fma(mid_gradient[m][permutations[j][n]], radii[permutations[j][n]], matrix[m][0],
                    matrix[m][0]);
            }
            for(index_t n = 0; n < comp_permutations[j].size(); ++n) {
                mul(mid_gradient[m][comp_permutations[j][n]], radii[comp_permutations[j][n]],
                    matrix[m][1+n]);
            }
            matrix[m][comp_permutations[j].size() + 1] = evals[m];
        }
        solve(matrix, candidate);
        //row_echelon(matrix, matrix);
        //solve_diagonal_dominant(matrix, candidate);

        bool current_valid = true;
        current_r = get_max_abs(candidate[0]);
        //if(get_min_abs(candidate[0]) >= 1) {
        //    current_valid = false;
        //}
        for(index_t k = 1; k < evals.size(); ++k) {
            if(get_min_abs(candidate[k]) > current_r) {
               // or get_min_abs(candidate[k]) >= 1) {
                //current_r = get_max_abs(candidate[k]);
                current_valid = false;
                //break;
            }
            if(get_max_abs(candidate[k]) > current_r) {
                current_r = get_max_abs(candidate[k]);
            }
            // if(candidate[k].upper > current_r) {
            //    candidate[k].upper = current_r;
            // } else if(candidate[k].neg_lower > current_r) {
            //    candidate[k].neg_lower = current_r;
            // }
        }
        //std::cout << Numeric(r) << " " << Numeric(current_r) << "\n";
        // update for a valid corner or non valid if no previous valid
        // corner
        if((current_r < r and (current_valid or !valid)) or (!valid and current_valid)) {
            crop(Numeric(-2,2), candidate[0]);
            for(index_t n = 0; n < permutations[j].size(); ++n) {
                mul(radii[permutations[j][n]], candidate[0], result[permutations[j][n]]);
            }
            for(index_t n = 0; n < comp_permutations[j].size(); ++n) {
                crop(Numeric(-2,2), candidate[1+n]);
                mul(radii[comp_permutations[j][n]], candidate[1+n], result[comp_permutations[j][n]]);
            }
            r = current_r;
            if(current_valid) {
                valid = true;
            }
        }
    }
}

//TODO : compute centers in nD then project on axes
//TODO : move delta_box multiplications above
template<template<typename Numeric> class Scheme, typename Numeric>
void get_centers_normals(const Tensor_box<Numeric> & box_set,
                 const std::vector<std::vector<Numeric>> jets, 
                 index_t begin, index_t end,
                 const std::vector<char> & signs,
                 const std::vector<index_t> & axes,
                 std::vector<std::array<ply_float, 6>> & centers_normals) {
    index_t dim = box_set.get_number_axes();
    std::vector<Numeric> delta_box(dim, 0);
    for(index_t i = 0; i < dim; ++i) {
        index_t k = box_set.idx[i][0];
        delta(box_set.data[i][k], delta_box[i]);
    }
    const index_t block_dim = Scheme<Numeric>::dimension(dim);
    centers_normals.resize(end - begin, {0,0,0,0,0,0});

    const int round_state = std::fegetround();
    std::fesetround(Scheme<Numeric>::rounding_mode);
    std::vector<index_t> ind_zero(0);
    std::vector<index_t> ind_nonaxes(0);
    std::vector<std::vector<index_t>> partitions;
    std::vector<std::vector<index_t>> comp_partitions;
    for(index_t i = 0; i < signs.size(); ++i) {
        if(signs[i] == 0) {
            ind_zero.push_back(i);
        }
    }
    if(dim - ind_zero.size() >= 3 or ind_zero.size() == 0) {
        return;
    }
    std::vector<index_t> pol_ind(ind_zero.size());
    std::iota(pol_ind.begin(), pol_ind.end(), 0);
    // get indices of non axes coordinates
    if(axes.size() == 3) {
        index_t offset = 0;
        ind_nonaxes.resize(dim - 3);
        // remove axes[0], axes[1] and axes[2] from the indices
        // they must be all different
        for(index_t i = 0; i < dim; ++i) {
            if(i == axes[0] or i == axes[1] or i == axes[2]) {
                offset += 1;
            } else {
                ind_nonaxes[i - offset] = i;
            }
        }
    }
    get_partitions(dim, dim-ind_zero.size()+1, partitions, comp_partitions);
    
    #pragma omp parallel for shared(ind_zero, ind_nonaxes, centers_normals)
    for(index_t i = 0; i < end-begin; ++i) {
        std::vector<typename Numeric::base_type> preshift(dim, 0);
        std::vector<Numeric> shift(dim, 0);
        std::vector<Numeric> evals(ind_zero.size(), 0);
        std::vector<std::vector<Numeric>> slopes(ind_zero.size(), std::vector<Numeric>(dim, 0));
        std::vector<std::vector<Numeric>> mid_gradient(ind_zero.size(), std::vector<Numeric>(dim, 0));
        for(index_t m = 0; m < ind_zero.size(); ++m) {
            //evals[m] = jets[ind_zero[m]][(begin + i)*block_dim];
            Scheme<Numeric>::get_evals_slopes(jets[ind_zero[m]].begin() + (begin + i)*block_dim, delta_box,
                                              evals[m], slopes[m].begin(), preshift.begin());
            for(index_t n = 0; n < dim; ++n) {
                mid(jets[ind_zero[m]][(begin+i)*block_dim + 1 + n], mid_gradient[m][n]);
                //mid(slopes[m][n], mid_gradient[m][n]);
            }
        }
        shift_center(evals, slopes, mid_gradient, delta_box, partitions, comp_partitions, shift);
        for(index_t j = 0; j < axes.size(); ++j) {
            //centers_normals[i][j] = get_near_zero(shift[axes[j]]);
            //centers_normals[i][j] = get_far_from_zero(shift[axes[j]]);
            centers_normals[i][j] = preshift[axes[j]] + get_center(shift[axes[j]]);
        }
        // get_normal(jets, begin+i, shift, axes, ind_nonaxes, centers_normals.begin() + 6*i + 3);
        // //TODO
        // typename Numeric::base_type r_num(1);
        // typename Numeric::base_type r_den(0);
        if(axes.size() == 3) {
            std::vector<std::vector<Numeric>> gradient(ind_zero.size(), std::vector<Numeric>(dim, 0));
            std::vector<Numeric> ker(dim-2, 0);
            std::vector<Numeric> normal(axes.size(), 0);
            for(index_t n = 0; n < shift.size(); ++n) {
                mid(shift[n], shift[n]);
            }
            for(index_t m = 0; m < ind_zero.size(); ++m) {
                Scheme<Numeric>::get_gradient(jets[ind_zero[m]].begin() + (begin + i)*block_dim, shift, gradient[m].begin());
            }
            left_kernel(gradient, pol_ind, ind_nonaxes, ker);
            product_row_matrix(ker, gradient, pol_ind, axes, normal);
            typename Numeric::base_type N = 1;
            for(index_t j = 0; j < axes.size(); ++j) {
                if(get_center(normal[j]) > N) {
                    N = get_center(normal[j]);
                } else if(get_center(normal[j]) < -N) {
                    N = -get_center(normal[j]);
                }
            };
            for(index_t j = 0; j < axes.size(); ++j) {
                centers_normals[i][3+j] = get_center(normal[j])/N;
            }
        }
    }
    std::fesetround(round_state);
} 

template<template<typename Numeric> class Scheme, typename Numeric>
void get_condition_numbers(const const_vec_iterator<Numeric> it_jets, 
                           const Tensor_box<Numeric> & box_set,
                           std::vector<typename Numeric::base_type> & result) {
    index_t dim = box_set.get_number_axes();
    index_t N   = box_set.get_number_elements();
    index_t block_dim = Scheme<Numeric>::dimension(dim);
    std::vector<index_t> it_boxes(dim, 0);

    const int round_state = std::fegetround();
    std::fesetround(Scheme<Numeric>::rounding_mode);
    std::vector<typename Numeric::base_type> weights(dim);
    result.resize(2*N);
    for(index_t i = 0; i < dim; ++i) {
        index_t k = box_set.idx[i][0];
        weights[i] =  get_rad(box_set.data[i][k]);
    }
    for(index_t i = 0; i < N; ++i) {
        Scheme<Numeric>::condition_number(it_jets + i*block_dim, weights, result[2*i], result[2*i+1]);
    }
    std::fesetround(round_state);
} 


// Not robust to aliasing
static void transpose(const Tensor & polynomial,
                            Tensor & result)
{
    index_t Na = polynomial.get_number_axes();
    index_t Ne = polynomial.get_number_elements();
    result.pos.resize(Na);
    result.idx.resize(Na);
    index_t N = 1;
    for(index_t i = 0; i < Na; ++i) {
        //index_t Npos = polynomial.pos[Na-1-i].size() - 1;
        result.pos[i].resize(N+1);
        // populate transposed positions
        std::vector<index_t> it_exponents = polynomial.begin();
        std::fill(result.pos[i].begin(), result.pos[i].end(), 0);
        for(index_t j = 0; j < Ne; ++j) {
            index_t pos = 0;
            for(index_t k = 0; k < i; ++k) {
                pos = result.pos[k][pos];
                pos += polynomial.idx[Na-1-k][it_exponents[Na-1-k]];
            }
            pos++;
            // update exponent at pos if greater than previous one
            if(result.pos[i][pos] < polynomial.idx[Na-1-i][it_exponents[Na-1-i]]) {
                result.pos[i][pos] = polynomial.idx[Na-1-i][it_exponents[Na-1-i]];
            }
            // std::cout << "it_exponents[Na-1-i] " << it_exponents[Na-1-i] << "\n";
            // std::cout << "result.pos[i].back() " << result.pos[i].back() << "\n";
            polynomial.advance(it_exponents);
        }
        // update positions from shift
        // going backward is a topological sort of the partial order on
        // vectors
        if(i>0){
            it_exponents.resize(i);
            result.get_last(it_exponents);
            for(index_t j = 0; j < N; ++j) {
                for(index_t d = 0; d < i; ++d) {
                    index_t pos = (d == 0) ? 0 : it_exponents[d-1];
                    index_t next_pos = it_exponents[d]+1;
                    for(index_t k = 0; k < i-d; ++k) {
                        if (next_pos < result.pos[d+k][pos+1]) {
                            pos = next_pos;
                            if(k+1 < i-d) {
                                next_pos = result.pos[d+k+1][pos] + result.idx[d+k+1][it_exponents[d+k+1]];
                            }
                        } else {
                            goto continueloop;
                        }
                    }
                    pos++;
                    if(result.pos[i][pos] > result.pos[i][it_exponents[i-1]+1]) {
                        result.pos[i][it_exponents[i-1]+1] = result.pos[i][pos];
                    }
                    continueloop:;
                }
                polynomial.advance_back(it_exponents);
            }
        }
        // change pos from relative exponents to absolute size positions
        for(index_t j = 0; j < N; ++j) {
            // size is exponent+1
            result.pos[i][1+j] += result.pos[i][j] + 1;
        }
        // create idx
        result.idx[i].resize(result.pos[i].back());
        // std::cout <<"Ne " << Ne << "\n";
        // std::cout << "result.pos[i].back() " << result.pos[i].back() << "\n";
        // std::cout << "result.idx[i].size() " << result.idx[i].size() << "\n";
        for(index_t j = 0; j < N; ++j) {
            for(index_t k = result.pos[i][j]; k < result.pos[i][j+1]; ++k) {
                result.idx[i][k] = k - result.pos[i][j];
            }
        }
        N = result.idx[i].size();
    }

}

// Robust to aliasing
template<typename Numeric>
void shift(const Tensor_poly<Numeric> & polynomial,
           const std::vector<Numeric> & center,
           Tensor_poly<Numeric> & result)
{
    index_t Na = polynomial.get_number_axes();
    Tensor_box<Numeric> derivations;
    std::vector<Numeric> new_coeffs;
    transpose(polynomial, derivations);
    derivations.data.resize(Na);
    for(index_t i = 0; i < Na; ++i) {
        derivations.data[i].resize(1, 0);
        derivations.data[i][0] = center[i];
    }
    evaluate<Shift>(polynomial, derivations, new_coeffs);
    std::swap(result.pos, derivations.pos);
    std::swap(result.idx, derivations.idx);
    std::swap(result.data, new_coeffs);
}

// To do reduce degree when centering
template<typename Interval, typename Numeric>
void trim(Tensor_poly<Interval> & polynomial,
          const Numeric & radius,
          const Numeric & tolerance)
{

}

constexpr const char* subscripts[10] = {"₀","₁","₂","₃","₄","₅","₆","₇","₈","₉"};
constexpr const char* superscripts[10] = {"⁰","¹","²","³","⁴","⁵","⁶","⁷","⁸","⁹"};
constexpr const char* alphabet[26] = {"a","b","c","d","e","f","g","h","i","j","k","l","m",
                                      "n","o","p","q","r","s","t","u","v","w","x","y","z"};

static void print_int(index_t num, const char* const (&indices)[10], std::ostream& result)
{
    std::vector<index_t> digits;
    index_t c = num;
    if(num == 0) {
        digits.push_back(0);
    }
    while(c != 0) {
        digits.push_back(c);
        c = c / 10;
    }
    index_t N = digits.size();
    c = digits[N-1];
    result << indices[c];
    for(index_t i = 1; i < N; ++i) {
        c = digits[N-i-1] - 10*digits[N-i];
        result << indices[c];
    }
}

template<typename Numeric>
void print_fancy_monomial(const Tensor_poly<Numeric>& poly,
                         const std::vector<index_t>& index,
                         std::ostream& result)
{
    for(index_t i = 0; i < index.size(); ++i) {
        result << "x";
        print_int(i, subscripts, result);
        print_int(poly.idx[i][index[i]], superscripts, result);
    }
}

template<typename Numeric>
bool print_monomial(const Tensor_poly<Numeric>& poly,
                         const std::vector<index_t>& index,
                         std::ostream& result)
{
    bool is_empty = true;
    for(index_t i = 0; i < index.size(); ++i) {
        size_t exp = poly.idx[i][index[i]];
        if(exp > 0) {
            is_empty = false;
            result << alphabet[i];
            if(exp > 1) {
                result << "^" << exp;
            }
            if(i < index.size() - 1){
                result << " ";
            }
        }
    }
    return is_empty;
}

template<typename Numeric>
std::ostream& print_polynomial(std::ostream& os, const Tensor_poly<Numeric> & poly, index_t N)
{
    index_t Ne = poly.get_number_elements();
    index_t Nchunk = N/3;
    index_t Nmiddle = Nchunk + (Ne - N)/2;
    index_t Nend = Ne - (N - 2*Nchunk);
    std::vector<index_t> it_poly = poly.begin();
    for(index_t k = 0; k < Ne; ++k) {
        if(k < Nchunk or k >= Nend or (k >= Nmiddle and k < Nmiddle + Nchunk)) {
            os << poly.data[k] << " ";
            bool is_empty = print_monomial(poly, it_poly, os);
            if(!is_empty) { os << " ";}
            os << ((k < N-1)?"+ ":"");
        } else if (k == Nchunk or k == Nmiddle + Nchunk) {
            os << "... + ";
        }
        poly.advance(it_poly);
    }
    return os;
}

template<typename Numeric>
std::ostream& operator<<(std::ostream& os, const Tensor_poly<Numeric> & poly)
{
    index_t N = poly.get_number_elements();
    return print_polynomial(os, poly, N);
}

static void fail_parsing_polynomial() {
    fprintf(stderr, "An input polynomial could not be parsed.\n");
    exit(EXIT_FAILURE);
}

template<typename Numeric>
std::istream& operator>>(std::istream& is, Tensor_poly<Numeric> & poly)
{
    index_t n = 0;
    std::vector<Numeric> coefficients(1, 0);
    std::vector<std::vector<index_t>> monomials(1);
    Numeric c(0);
    index_t m;

    // Read first line after comments
    std::string line;
    //std::getline(is, line);
    while(std::getline(is, line)) {
        if(line[0] != '#' and line[0] != '/' and !line.empty()) {
            break;
        }
    }
    std::stringstream first_line(line);
    first_line >> coefficients[0];
    while(first_line >> m) {
        monomials[0].push_back(m);
        n++;
    }

    // Read remaining lines
    while(is >> c) {
        coefficients.push_back(c);
        monomials.push_back(std::vector<index_t>(n));
        for(index_t i = 0; i < n; ++i) {
            if(!(is >> monomials.back()[i])) {fail_parsing_polynomial();}
        }
    }
    poly = make_tensor_poly(coefficients, monomials);
    return is;
}

#endif

