/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#ifndef TENSOR_TENSOR_HPP
#define TENSOR_TENSOR_HPP

#include <vector>
#include <cstdint>

using boolean = unsigned char;
using index_t = size_t;

#ifdef COMPACT
#define IDX_SIZE 8
using small_index_t = uint8_t;
#else
#ifdef EXTEND
#define IDX_SIZE 64
using small_index_t = uint64_t;
#else
#define IDX_SIZE 32
using small_index_t = uint_fast32_t;
#endif
#endif


// Tensor data structure : CSF format, based on
// Unified Sparse Formats for Tensor Algebra Compilers,
// Stephen Chou, Fredrik Kjolstad, Saman Amarasinghe
// and references therein
class Tensor
{
public:
        std::vector< std::vector<index_t> > pos;
        std::vector< std::vector<small_index_t> > idx;

    // Methods on shape
    bool is_empty() const;
    index_t get_number_axes(void) const;
    index_t get_number_elements(void) const;
    std::vector<index_t> begin() const;
    void advance(std::vector<index_t>& it) const;
    void advance_back(std::vector<index_t>& it) const;
    void get_last(std::vector<index_t>& it) const;
    void get_idx(const std::vector<index_t> & it_box,
                 std::vector<small_index_t>& result) const;
    bool contains(const index_t & start_level, const index_t & start_pos,
                  const std::vector<small_index_t>& indices,
                  std::vector<index_t> & it_box) const;
};

// Methods on shape
bool Tensor::is_empty() const
{
    return (pos.size() == 0 || pos[0].size() == 1);
}

index_t Tensor::get_number_axes(void) const
{
    return pos.size();
}

index_t Tensor::get_number_elements(void) const
{
    if(idx.size() == 0) {
        return 0;
    }
    return idx.back().size();
}

void Tensor::get_last(std::vector<index_t> & it) const
{
    for(index_t i = 0; i < it.size(); ++i) {
        it[i] = idx[i].size() - 1;
    }
}

void Tensor::get_idx(const std::vector<index_t> & it_box,
                     std::vector<small_index_t>& result) const
{
    for(index_t i = 0; i < result.size(); ++i) {
        result[i] = idx[i][it_box[i]];
    }
}

std::vector<index_t> Tensor::begin() const
{
    index_t Na = get_number_axes();
    std::vector<index_t> result(Na, 0);
    if(get_number_elements() == 0) {
        return result;
    }
    for(index_t k = 0; k < Na-1; ++k) {
        while(result[Na-1-k-1] < pos[Na-1-k].size() - 1
              and result[Na-1-k] >= pos[Na-1-k][result[Na-1-k-1]+1]) {
            result[Na-1-k-1]++;
        }
    }
    return result;
}

void Tensor::advance(std::vector<index_t> & current) const
{
    index_t N = current.size();
    current[N-1]++;
    for(index_t k = 0; k < N-1; ++k) {
        // test if we need to update the previous axe
        if(current[N-1-k] < pos[N-1-k][current[N-1-k-1]+1]) {
            break;
        }
        while(current[N-1-k-1] < pos[N-1-k].size() - 1
              and current[N-1-k] >= pos[N-1-k][current[N-1-k-1]+1]) {
            current[N-1-k-1]++;
        }
    }
}

void Tensor::advance_back(std::vector<index_t> & current) const
{
    index_t axe = current.size() - 1;
    current[axe]--;
    for(index_t k = 0; k < axe; ++k) {
        // test if we need to update the previous axe
        if(current[axe-k] >= pos[axe-k][current[axe-k-1]]) {
            break;
        }
        while(current[axe-k-1] != static_cast<index_t>(0)-1
              and current[axe-k] < pos[axe-k][current[axe-k-1]]) {
            current[axe-k-1]--;
        }
    }
}

bool Tensor::contains(const index_t & start_level, const index_t & start_pos,
                      const std::vector<small_index_t>& indices,
                      std::vector<index_t> & it_box) const
{
    index_t current_pos = start_pos;
    for(index_t i = start_level; i < indices.size(); ++i) {
        const auto lower = idx[i].begin() + pos[i][current_pos];
        const auto upper = idx[i].begin() + pos[i][current_pos+1];
        auto lb = std::lower_bound(lower, upper, indices[i]);
        if( lb == upper or indices[i] < *lb) {
            return false;
        }
        current_pos = lb - idx[i].begin();
        it_box[i] = current_pos;
    }
    return true;
}

#endif
