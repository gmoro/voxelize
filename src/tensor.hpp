/* Guillaume Moroz, December 2018 */

#ifndef TENSOR_HPP
#define TENSOR_HPP
#include <vector>
#include <iostream>

using boolean = unsigned char;
using index_t = size_t;

// Tensor data structure : CSF format, based on
// Unified Sparse Formats for Tensor Algebra Compilers,
// Stephen Chou, Fredrik Kjolstad, Saman Amarasinghe
// and references therein
#include "tensor/tensor.hpp"

// Box tensor
#include "tensor/box.hpp"

// Polynomial tensor
#include "tensor/poly.hpp"

#endif
