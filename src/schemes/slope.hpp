#include <vector>
#include <cfenv>
#include "tensor/box.hpp"

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;

template<typename coeff_t, typename val_t = coeff_t>
class Slope
{
    public:
        static const int rounding_mode = FE_UPWARD;

        static index_t dimension(const index_t i) {
            return i+1;
        }

        static void evaluate(const const_vec_iterator<coeff_t> it_pol,
                             const index_t pol_size,
                             const const_vec_iterator<val_t> it_vals,
                             const index_t vals_size,
                             const index_t level,
                             const vec_iterator<coeff_t> it_result)
        {
            const index_t input_dim = dimension(level);
            const index_t output_dim = dimension(level+1);
            if(vals_size == 0) {
                return;
            }

            if(pol_size == 0) {
                std::fill(it_result, it_result + output_dim*vals_size, 0);
                return;
            }

            // Initialize loop
            std::vector<val_t> mid_vals(vals_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                mid(it_vals[k], mid_vals[k]);
                it_result[output_dim*k] = it_pol[0];
                it_result[output_dim*k+1] = 0;
                for(index_t i = 0; i < input_dim - 1; ++i) {
                    it_result[output_dim*k+2+i] = it_pol[input_dim*(pol_size-1)+1+i];
                }
            }

            // Main evaluation loop
            for(index_t j = 1; j < pol_size; ++j) {
                for(index_t k = 0; k < vals_size; ++k) {
                    fma(it_result[output_dim*k+1], it_vals[k], it_pol[input_dim*(pol_size-j)],
                        it_result[output_dim*k+1]);
                    for(index_t i = 0; i < input_dim - 1; ++i) {
                        fma(it_result[output_dim*k+2+i], it_vals[k], it_pol[input_dim*(pol_size-1-j)+1+i],
                            it_result[output_dim*k+2+i]);
                    }
                }
            }
        }

        static void is_non_zero(const const_vec_iterator<coeff_t> jets, 
                                const Tensor_box<val_t> & box_set,
                                const vec_iterator<boolean> result) {
            index_t dim = box_set.get_number_axes();
            index_t N   = box_set.get_number_elements();
            std::vector<index_t> it_boxes(dim, 0);
            std::vector<val_t> b(dim, 0);
            coeff_t c(0);
            val_t   db(0);
            for(index_t i = 0; i < N; ++i) {
                get_box(box_set, it_boxes, b);
                c = jets[i*(dim+1)];
                for(index_t j = 0; j < dim; ++j) {
                    fma(jets[i*(dim+1) + j + 1], b[dim-1-j], c, c);
                }
                result[i] = !contains_zero(c);
                box_set.advance(it_boxes);
            }
        } 
};
