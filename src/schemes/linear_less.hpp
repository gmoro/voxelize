#include <vector>
#include <cfenv>
#include "tensor/box.hpp"

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;

template<typename coeff_t, typename val_t = coeff_t>
class Linear_less
{
    public:
        static const int rounding_mode = FE_UPWARD;

        static index_t dimension(const index_t ) {
            return 1;
        }

        static void evaluate(const const_vec_iterator<coeff_t> it_pol,
                             const index_t pol_size,
                             const const_vec_iterator<val_t> it_vals,
                             const index_t vals_size,
                             const index_t,
                             const vec_iterator<coeff_t> it_result)
        {
            if(vals_size == 0) {
                return;
            }

            if(pol_size == 0) {
                std::fill(it_result, it_result + vals_size, 0);
                return;
            }

            // Initialize loop
            std::vector<val_t> mid_vals(vals_size, 0);
            std::vector<val_t> tmp_vals(vals_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                mid(it_vals[k], mid_vals[k]);
                it_result[k] = it_pol[pol_size-1];
            }

            // Main evaluation loop
            for(index_t j = 1; j < pol_size; ++j) {
                for(index_t k = 0; k < vals_size; ++k) {
                    fma(tmp_vals[k], it_vals[k], it_result[k], tmp_vals[k]);
                    fma(it_result[k], mid_vals[k], it_pol[pol_size-1-j], it_result[k]);
                }
            }
            for(index_t k = 0; k < vals_size; ++k) {
                val_t dv(0);
                delta(it_vals[k], dv);
                fma(tmp_vals[k], dv, it_result[k], it_result[k]);
            }
        }

        static void is_non_zero(const const_vec_iterator<coeff_t> jets, 
                                const Tensor_box<val_t> & box_set,
                                const vec_iterator<boolean> result) {
            index_t N   = box_set.get_number_elements();
            for(index_t i = 0; i < N; ++i) {
                result[i] = !contains_zero(jets[i]);
            }
        } 
};
