/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#ifndef SCHEMES_HORNER_HPP
#define SCHEMES_HORNER_HPP
#include <vector>
#include <cfenv>

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;
using boolean = unsigned char;

template<typename Numeric>
class Horner
{
    public:
        static const int rounding_mode = FE_UPWARD;
        static const bool has_gradient = false;

        static index_t dimension(const index_t) {
            return 1;
        }

        static void evaluate(const const_vec_iterator<Numeric> it_pol_begin,
                             const index_t pol_size,
                             const const_vec_iterator<Numeric> it_vals_begin,
                             const index_t vals_size,
                             const index_t,
                             const vec_iterator<Numeric> it_result)
        {
            if(vals_size == 0) {
                return;
            }

            // By convention the empty polynomial is the 0 polynomial
            if(pol_size == 0) {
                std::fill(it_result, it_result + vals_size, 0);
                return;
            }

            std::fill(it_result, it_result + vals_size, it_pol_begin[pol_size-1]);
            for(index_t j = 1; j < pol_size; ++j) {
                for(index_t k = 0; k < vals_size; ++k) {
                    fma(it_result[k], it_vals_begin[k], it_pol_begin[pol_size-1-j], it_result[k]);
                }
                //for(index_t k = 0; k < vals_size; ++k) {
                //    mul(it_result[k], it_vals_begin[k], it_result[k]);
                //}
                //#pragma GCC ivdep
                //for(index_t k = 0; k < vals_size; ++k) {
                //    add(it_pol_begin[pol_size-1-j], it_result[k]);
                //}
            }
        }

        static void condition_number(const const_vec_iterator<Numeric> it_jets,
                                     const std::vector<typename Numeric::base_type> weights,
                                     typename Numeric::base_type & numerator,
                                     typename Numeric::base_type & denominator)
        {
            // numerator = get_rad(it_jets[0]);
            // denominator = get_min_abs(it_jets[0]);
            numerator = 0;
            denominator = 1;
        }

        static char is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                                      std::vector<Numeric> & )
        {
            return sign(it_jets[0]);
        }

        static char is_zero(const const_vec_iterator<Numeric>, 
                                  std::vector<Numeric> &)
        {
            return false;
        }
};
#endif
