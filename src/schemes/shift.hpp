/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#include <vector>
#include <cfenv>

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;

template<typename Numeric>
class Shift
{
    public:
        static const int rounding_mode = FE_UPWARD;

        static index_t dimension(const index_t) {
            return 1;
        }

        static void evaluate(const const_vec_iterator<Numeric> it_pol_begin,
                             const index_t pol_size,
                             const const_vec_iterator<Numeric> it_vals_begin,
                             const index_t vals_size,
                             const index_t,
                             const vec_iterator<Numeric> it_result)
        {
            if(vals_size == 0) {
                return;
            }

            // By convention the empty polynomial is the 0 polynomial
            if(pol_size == 0) {
                std::fill(it_result, it_result + vals_size, 0);
                return;
            }

            index_t size = vals_size;

            // std::cout << pol_size << " ";
            // std::cout << vals_size << "\n";
            if(size > pol_size) {
                std::fill(it_result + pol_size, it_result + size, 0);
                size = pol_size;
            }
            //it_result[size-1] = it_pol_begin[size-1];
            for(index_t j = 0; j < size; ++j) {
                it_result[j] = it_pol_begin[j];
            }
            for(index_t j = 1; j < size; ++j) {
                for(index_t k = 0; k < j; ++k) {
                    fma(it_result[size-j+k], it_vals_begin[0], it_result[size-1-j+k],
                        it_result[size-1-j+k]);
                }
            }
            //std::vector<Numeric> vec(size-1, 0);
            //for(index_t j = 1; j < size; ++j) {
            //    for(index_t k = 0; k < j; ++k) {
            //        mul(it_result[size-j+k], it_vals_begin[0], vec[k]);
            //    }
            //    for(index_t k = 0; k < j; ++k) {
            //        add(it_result[size-1-j+k], vec[k], it_result[size-1-j+k]);
            //    }
            //}

        }

        static bool is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                                      std::vector<Numeric> & )
        {
            return !contains_zero(it_jets[0]);
        }
};
