#include <vector>
#include <cfenv>

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;

template<typename Numeric>
class Quadratic_chebyshev
{
    public:
        static const int rounding_mode = FE_UPWARD;

        static index_t dimension(const index_t i) {
            return (i+2)*(i+1)/2; // 1 + i + i*(i+1)/2;
        }

        static void evaluate(const const_vec_iterator<Numeric> it_pol,
                             const index_t pol_size,
                             const const_vec_iterator<Numeric> it_vals,
                             const index_t vals_size,
                             const index_t level,
                             const vec_iterator<Numeric> it_result)
        {
            const index_t input_dim = dimension(level);
            const index_t output_dim = dimension(level+1);
            if(vals_size == 0) {
                return;
            }

            if(pol_size == 0) {
                std::fill(it_result, it_result + output_dim*vals_size, 0);
                return;
            }

            //const int round_state = std::fegetround();
            //std::fesetround(rounding_mode);
            // Initialize loop
            std::vector<Numeric> mid_vals(vals_size, 0);
            std::vector<Numeric> rad_vals(vals_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                mid(it_vals[k], mid_vals[k]);
                rad(it_vals[k], rad_vals[k]);
                mul(rad_vals[k], rad_vals[k], rad_vals[k]);
                mul(rad_vals[k], Numeric(0.5), rad_vals[k]);
                it_result[output_dim*k + 1 + level] = 0;
                for(index_t i = 0; i < level+1; ++i) {
                    it_result[output_dim*k+i] = it_pol[input_dim*(pol_size-1)+i];
                }
                for(index_t i = 0; i < level*(level+1)/2; ++i) {
                    it_result[output_dim*k+2+level+i] = it_pol[input_dim*(pol_size-1)+1+level+i];
                }
                for(index_t i = 0; i < level+1; ++i) {
                    it_result[output_dim*k+output_dim-1-i] = 0;
                }
            }

            // Main evaluation loop
            // Compute f(x) = a + b x + [c] T2(x) where T2(x) = x^2 - 1/2
            for(index_t j = 1; j < pol_size; ++j) {
                for(index_t k = 0; k < vals_size; ++k) {
                    // Order 2
                    Numeric tmp(0);
                    fma(it_result[output_dim*k+1+level], rad_vals[k], it_pol[input_dim*(pol_size-1-j)], tmp);
                    fma(it_result[output_dim*k+output_dim-1], it_vals[k], it_result[output_dim*k+1+level],
                        it_result[output_dim*k+output_dim-1]);
                    fma(it_result[output_dim*k+1+level], mid_vals[k], it_result[output_dim*k],
                        it_result[output_dim*k+1+level]);
                    fma(it_result[output_dim*k], mid_vals[k], tmp, it_result[output_dim*k]);
                    // Order 1
                    for(index_t i = 0; i < level; ++i) {
                        fma(it_result[output_dim*k+output_dim-i-2], it_vals[k], it_result[output_dim*k+level-i],
                            it_result[output_dim*k+output_dim-i-2]);
                        fma(it_result[output_dim*k+level-i], mid_vals[k], it_pol[input_dim*(pol_size-1-j)+level-i],
                            it_result[output_dim*k+level-i]);
                    }
                    // Order 0
                    for(index_t i = 0; i < level*(level+1)/2; ++i) {
                        fma(it_result[output_dim*k+2+level+i], it_vals[k], it_pol[input_dim*(pol_size-1-j)+1+level+i],
                            it_result[output_dim*k+2+level+i]);
                    }
                }
            }
            //std::fesetround(round_state);
        }

        static bool is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                                      std::vector<Numeric> & box)
        {
            index_t dim = box.size();
            std::vector<Numeric> br(dim, 0);
            Numeric c(0);
            Numeric   v(0);
            for(index_t j = 0; j < dim; ++j) {
                delta(box[j], box[j]);
            }
            // Compute gradient to check monotonicity
            for(index_t j = 0; j < dim; ++j) {
                index_t n;
                c = it_jets[j + 1];
                for(index_t k = 0; k < j; ++k) {
                    n = 1 + dim + j*(j+1)/2 + k;
                    fma(it_jets[n], box[k], c, c);
                }
                n = 1 + dim + j*(j+1)/2 + j;
                mul(box[j], Numeric(2), v);
                fma(it_jets[n], v, c, c);
                for(index_t k = j+1; k < dim; ++k) {
                    n = 1 + dim + k*(k+1)/2 + j;
                    fma(it_jets[n], box[k], c, c);
                }
                if(get_lower(c) > 0) {
                    rad(box[j], br[j]);
                } else if(get_upper(c) < 0) {
                    rad(box[j], br[j]);
                    neg(br[j], br[j]);
                } else {
                    br[j] = box[j];
                }

            }
            // Compute approximation with specialized variables
            c = it_jets[0];
            for(index_t j = 0; j < dim; ++j) {
                fma(it_jets[j + 1], box[j], c, c);
                for(index_t k = 0; k < j+1; ++k) {
                    index_t n = 1 + dim + j*(j+1)/2 + k;
                    mul(br[j], br[k], v);
                    if(k==j) { mul(v, Numeric(0.5), v); }
                    fma(it_jets[n], v, c, c);
                }
            }
            return !contains_zero(c);
        } 
};
