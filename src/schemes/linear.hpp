/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#ifndef SCHEMES_LINEAR_HPP
#define SCHEMES_LINEAR_HPP
#include <vector>
#include <array>
#include <cfenv>

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;
using boolean = unsigned char;

template<typename Numeric>
class Linear
{
    public:
        static const int rounding_mode = FE_UPWARD;
        static const int has_gradient = true;

        static index_t dimension(const index_t i) {
            return i+1;
        }

        static void evaluate(const const_vec_iterator<Numeric> it_pol,
                             const index_t pol_size,
                             const const_vec_iterator<Numeric> it_vals,
                             const index_t vals_size,
                             const index_t level,
                             const vec_iterator<Numeric> it_result)
        {
            const index_t input_dim = dimension(level);
            const index_t output_dim = dimension(level+1);
            if(vals_size == 0) {
                return;
            }

            if(pol_size == 0) {
                std::fill(it_result, it_result + output_dim*vals_size, 0);
                return;
            }

            // Initialize loop
            std::vector<Numeric> mid_vals(vals_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                mid(it_vals[k], mid_vals[k]);
                it_result[output_dim*k] = it_pol[input_dim*(pol_size-1)];
                it_result[output_dim*k + level + 1] = 0;
                for(index_t i = 0; i < input_dim - 1; ++i) {
                    it_result[output_dim*k+1+i] = it_pol[input_dim*(pol_size-1)+1+i];
                }
            }

            // Main evaluation loop
            for(index_t j = 1; j < pol_size; ++j) {
                for(index_t k = 0; k < vals_size; ++k) {
                    fma(it_result[output_dim*k+level+1], it_vals[k], it_result[output_dim*k],
                        it_result[output_dim*k+level+1]);
                    fma(it_result[output_dim*k], mid_vals[k], it_pol[input_dim*(pol_size-1-j)],
                        it_result[output_dim*k]);
                    for(index_t i = 0; i < input_dim - 1; ++i) {
                        fma(it_result[output_dim*k+1+i], it_vals[k], it_pol[input_dim*(pol_size-1-j)+1+i],
                            it_result[output_dim*k+1+i]);
                    }
                }
            }
        }

        static char is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                                const std::vector<Numeric> & delta_box)
        {
            //Numeric   db(0);
            Numeric c(it_jets[0]);
            for(index_t j = 0; j < delta_box.size(); ++j) {
                //delta(box[j], db);
                fma(it_jets[j + 1], delta_box[j], c, c);
            }
            return sign(c);
        }

        static char is_zero(const const_vec_iterator<Numeric> it_jets, 
                            const std::vector<Numeric> & delta_box)
                  
        {
            Numeric r(0);
            Numeric c(0);
            Numeric c_low(it_jets[0]);
            Numeric c_up(it_jets[0]);
            bool monotoneus = false;
            for(index_t j = 0; j < delta_box.size(); ++j) {
                if(!contains_zero(it_jets[j + 1])) {
                    monotoneus = true;
                    rad(delta_box[j], r);
                    if(get_upper(it_jets[j+1]) < 0) {
                        neg(r, r);
                    }
                    mul(it_jets[j + 1], r, c);
                    add(c, c_up, c_up);
                    neg(c, c);
                    add(c, c_low, c_low);
                }
            }
            return monotoneus and get_lower(c_up) > 0 and get_upper(c_low) < 0;
        }

        static void condition_number(const const_vec_iterator<Numeric> it_jets,
                                     const std::vector<typename Numeric::base_type> weights,
                                     typename Numeric::base_type & numerator,
                                     typename Numeric::base_type & denominator)
        {
            numerator = get_max_abs(it_jets[0]);
            denominator = 0;
            for(index_t i = 0; i < weights.size(); ++i) {
                //numerator += get_rad(it_jets[1+i])*weights[i];
                denominator += get_min_abs(it_jets[1+i]);
            }
        }

        static void get_evals_slopes(const const_vec_iterator<Numeric> & it_jets,
                               const std::vector<Numeric> & delta_box,
                               Numeric & result_eval,
                               const vec_iterator<Numeric> & result_slopes,
                               const vec_iterator<typename Numeric::base_type> & result_preshift)
        {
            result_eval = it_jets[0];
            for(index_t i = 0; i < delta_box.size(); ++i) {
                result_slopes[i] = it_jets[1+i];
                result_preshift[i] = 0;
            }
        }

        static void get_gradient(const const_vec_iterator<Numeric> & it_jets,
                               const std::vector<Numeric> & shift,
                               const vec_iterator<Numeric> & result)
        {
            for(index_t i = 0; i < shift.size(); ++i) {
                result[i] = it_jets[1+i];
            }
        }

        static index_t indice_gradient(index_t , index_t variable)
        {
            return 1 + variable;
        }
};
#endif
