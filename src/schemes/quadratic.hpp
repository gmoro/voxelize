/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#ifndef SCHEMES_QUADRATIC_HPP
#define SCHEMES_QUADRATIC_HPP
#include <vector>
#include <array>
#include <cfenv>

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;
using boolean = unsigned char;

template<typename Numeric>
class Quadratic
{
    public:
        static const int rounding_mode = FE_UPWARD;
        static const int has_gradient = true;

        static index_t dimension(const index_t i) {
            return (i+2)*(i+1)/2; // 1 + i + i*(i+1)/2;
        }

        static void evaluate(const const_vec_iterator<Numeric> it_pol,
                             const index_t pol_size,
                             const const_vec_iterator<Numeric> it_vals,
                             const index_t vals_size,
                             const index_t level,
                             const vec_iterator<Numeric> it_result)
        {
            const index_t input_dim = dimension(level);
            const index_t output_dim = dimension(level+1);
            if(vals_size == 0) {
                return;
            }

            if(pol_size == 0) {
                std::fill(it_result, it_result + output_dim*vals_size, 0);
                return;
            }

            //const int round_state = std::fegetround();
            //std::fesetround(rounding_mode);
            // Initialize loop
            std::vector<Numeric> mid_vals(vals_size, 0);
            std::vector<Numeric> error_vals(input_dim*vals_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                mid(it_vals[k], mid_vals[k]);
                it_result[output_dim*k + 1 + level] = 0;
                for(index_t i = 0; i < level+1; ++i) {
                    it_result[output_dim*k+i] = it_pol[input_dim*(pol_size-1)+i];
                }
                for(index_t i = 0; i < level*(level+1)/2; ++i) {
                    it_result[output_dim*k+2+level+i] = it_pol[input_dim*(pol_size-1)+1+level+i];
                }
                for(index_t i = 0; i < level+1; ++i) {
                    it_result[output_dim*k+output_dim-1-i] = 0;
                }
            }

            // Main evaluation loop
            // Compute f(x) = a + b x + c x^2 + [d] x^3
            for(index_t j = 1; j < pol_size; ++j) {
                for(index_t k = 0; k < vals_size; ++k) {
                    // Order 3 for constants
                    fma(error_vals[input_dim*k], it_vals[k], it_result[output_dim*k + output_dim - 1], error_vals[input_dim*k]);
                    fma(it_result[output_dim*k+output_dim-1], mid_vals[k], it_result[output_dim*k+1+level],
                        it_result[output_dim*k+output_dim-1]);
                    fma(it_result[output_dim*k+1+level], mid_vals[k], it_result[output_dim*k],
                        it_result[output_dim*k+1+level]);
                    fma(it_result[output_dim*k], mid_vals[k], it_pol[input_dim*(pol_size-1-j)],
                        it_result[output_dim*k]);
                    // Order 2
                    for(index_t i = 0; i < level; ++i) {
                        fma(error_vals[input_dim*k+level-i], it_vals[k], it_result[output_dim*k+output_dim-i-2],
                            error_vals[input_dim*k+level-i]);
                        fma(it_result[output_dim*k+output_dim-i-2], mid_vals[k], it_result[output_dim*k+level-i],
                            it_result[output_dim*k+output_dim-i-2]);
                        fma(it_result[output_dim*k+level-i], mid_vals[k], it_pol[input_dim*(pol_size-1-j)+level-i],
                            it_result[output_dim*k+level-i]);
                    }
                    // Order 1
                    for(index_t i = 0; i < level*(level+1)/2; ++i) {
                        fma(error_vals[input_dim*k+1+level+i], it_vals[k], it_result[output_dim*k+2+level+i],
                            error_vals[input_dim*k+1+level+i]);
                        //TODO: test simple mul split according to mid_vals[k] pos or neg and vectorize
                        fma(it_result[output_dim*k+2+level+i], mid_vals[k], it_pol[input_dim*(pol_size-1-j)+1+level+i],
                            it_result[output_dim*k+2+level+i]);
                    }
                    //index_t start = input_dim*k+1+level;
                    //index_t delta_output = (output_dim-input_dim)*k+1;
                    //index_t delta_pol = input_dim*(pol_size-1-j-k);
                    //for(index_t i = start; i < start + level*(level+1)/2; ++i) {
                    //    fma(error_vals[i], it_vals[k], it_result[i+delta_output],
                    //        error_vals[i]);
                    //    fma(it_result[i+delta_output], mid_vals[k], it_pol[i+delta_pol],
                    //        it_result[i+delta_output]);
                    //}
                }
            }
            // Adding error [d] x to c
            Numeric dv(0);
            for(index_t k = 0; k < vals_size; ++k) {
                delta(it_vals[k], dv);
                fma(error_vals[input_dim*k], dv, it_result[output_dim*k+output_dim-1], it_result[output_dim*k+output_dim-1]);
                for(index_t i = 0; i < level; ++i) {
                    fma(error_vals[input_dim*k+level-i], dv, it_result[output_dim*k+output_dim-i-2],
                         it_result[output_dim*k+output_dim-i-2]);
                }
                for(index_t i = 0; i < level*(level+1)/2; ++i) {
                    fma(error_vals[input_dim*k+1+level+i], dv, it_result[output_dim*k+2+level+i],
                        it_result[output_dim*k+2+level+i]);
                }
            }
            //std::fesetround(round_state);
        }

        static char is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                                const std::vector<Numeric> & delta_box)
        {
            index_t dim = delta_box.size();
            std::vector<Numeric> db = delta_box;
            std::vector<Numeric> slopes(dim, 0);
            std::vector<char> shrinked(dim, false);
            typename Numeric::base_type r(0);
            Numeric c(0);
            Numeric v(0);
            if(contains_zero(it_jets[0])) {
                return 0;
            }
            char sign_center = sign(it_jets[0]);
            //for(index_t j = 0; j < dim; ++j) {
            //    delta(delta_box[j], delta_box[j]);
            //}
            // Compute gradient to check monotonicity
            bool has_shrinked;
            do {
                has_shrinked = false;
                for(index_t j = 0; j < dim; ++j) {
                    if(shrinked[j]) {
                        continue;
                    }
                    index_t n;
                    c = it_jets[1+j];
                    //c = 0;
                    for(index_t k = 0; k < j; ++k) {
                        n = 1 + dim + j*(j+1)/2 + k;
                        fma(it_jets[n], db[k], c, c);
                        //r += get_max_abs(it_jets[n]) * delta_box[k].upper;
                    }
                    //r += get_max_abs(it_jets[n]) * delta_box[j].upper * 2;
                    for(index_t k = j+1; k < dim; ++k) {
                        n = 1 + dim + k*(k+1)/2 + j;
                        fma(it_jets[n], db[k], c, c);
                        //r += get_max_abs(it_jets[n]) * delta_box[k].upper;
                    }
                    n = 1 + dim + j*(j+1)/2 + j;
                    mul(it_jets[n], db[j], v);
                    fma(v, Numeric(2), c, c);
                    //add(v, it_jets[1+j], v);
                    // only need to check that lower face has same sign as
                    // center
                    if(sign(c) == sign_center) {
                        rad(delta_box[j], db[j]);
                        neg(db[j], db[j]);
                        shrinked[j] = true;
                        has_shrinked = true;
                    // only need to check that upper face has same sign as
                    // center
                    } else if(sign(c) == -sign_center) {
                        rad(delta_box[j], db[j]);
                        shrinked[j] = true;
                        has_shrinked = true;
                    }
                    //fma(c, Numeric(0.5), it_jets[1+j], slopes[j]);
                    //fma(it_jets[n], db[j], slopes[j], slopes[j]);
                }
            } while(has_shrinked);
            // Compute approximation with specialized variables
            c = it_jets[0];
            for(index_t j = 0; j < dim; ++j) {
                //fma(slopes[j], db[j], c, c);
                fma(it_jets[j + 1], db[j], c, c);
                index_t m = 1 + dim + j*(j+1)/2;
                for(index_t k = 0; k < j+1; ++k) {
                    mul(db[j], db[k], v);
                    fma(it_jets[m+k], v, c, c);
                }
            }
            return sign(c) == sign_center ? sign_center : 0;
        } 

        static char is_zero(const const_vec_iterator<Numeric> it_jets, 
                            const std::vector<Numeric> & delta_box)
        {
            index_t dim = delta_box.size();
            std::vector<Numeric> br(dim, 0);
            typename Numeric::base_type r(0);
            Numeric c(0);
            Numeric c_low(0);
            Numeric c_up(0);
            Numeric v(0);
            bool monotoneus = false;
            //for(index_t j = 0; j < dim; ++j) {
            //    delta(box[j], box[j]);
            //}
            // Compute slope to choose the two points
            // TODO: simplify computation as in is_non_zero
            for(index_t j = 0; j < dim; ++j) {
                index_t n;
                r = 0;
                for(index_t k = 0; k < j; ++k) {
                    n = 1 + dim + j*(j+1)/2 + k;
                    r += get_max_abs(it_jets[n]) * delta_box[k].upper;
                }
                n = 1 + dim + j*(j+1)/2 + j;
                r += get_max_abs(it_jets[n]) * delta_box[j].upper * 2;
                for(index_t k = j+1; k < dim; ++k) {
                    n = 1 + dim + k*(k+1)/2 + j;
                    r += get_max_abs(it_jets[n]) * delta_box[k].upper;
                }
                if(get_lower(it_jets[1+j]) > r) {
                    monotoneus = true;
                    rad(delta_box[j], br[j]);
                } else if(get_upper(it_jets[1+j]) < -r) {
                    monotoneus = true;
                    rad(delta_box[j], br[j]);
                    neg(br[j], br[j]);
                } else {
                    br[j] = 0;
                }
            }

            //for(index_t j = 0; j < dim; ++j) {
            //    index_t n = 1 + dim + j*(j+1)/2 + j;
            //    mul(it_jets[n], delta_box[j], c);
            //    for(index_t k = 0; k < j; ++k) {
            //        n = 1 + dim + j*(j+1)/2 + k;
            //        fma(it_jets[n], delta_box[k], c, c);
            //    }
            //    for(index_t k = j+1; k < dim; ++k) {
            //        n = 1 + dim + k*(k+1)/2 + j;
            //        fma(it_jets[n], delta_box[k], c, c);
            //    }
            //    add(it_jets[j + 1], c, c_up);
            //    neg(c, c);
            //    add(it_jets[j + 1], c, c_low);
            //    if(get_lower(c_up) > 0 and get_lower(c_low) > 0) {
            //        monotoneus = true;
            //        rad(delta_box[j], delta_box[j]);
            //    } else if(get_upper(c_up) < 0 and get_upper(c_low) < 0) {
            //        monotoneus = true;
            //        rad(delta_box[j], delta_box[j]);
            //        neg(delta_box[j], delta_box[j]);
            //    } else {
            //        //we will only evaluate at the middle afterwards
            //        delta_box[j] = 0;
            //    }
            //}

            // Compute approximation with specialized variables
            c_low = it_jets[0];
            c_up = it_jets[0];
            for(index_t j = 0; j < dim; ++j) {
                mul(it_jets[j + 1], br[j], c);
                add(c, c_up, c_up);
                neg(c, c);
                add(c, c_low, c_low);
                for(index_t k = 0; k < j+1; ++k) {
                    index_t n = 1 + dim + j*(j+1)/2 + k;
                    mul(br[j], br[k], v);
                    mul(it_jets[n], v, c);
                    add(c, c_up, c_up);
                    add(c, c_low, c_low);
                }
            }
            return monotoneus and get_lower(c_up) > 0 and get_upper(c_low) < 0;
            //return get_lower(c_up) > 0 and get_upper(c_low) < 0;
        } 

        // static void condition_number(const const_vec_iterator<Numeric> it_jets,
        //                              const std::vector<typename Numeric::base_type> weights,
        //                              typename Numeric::base_type & numerator,
        //                              typename Numeric::base_type & denominator)
        // {
        //     index_t dim = weights.size();
        //     numerator = 0;
        //     denominator = 0;
        //     numerator = 0;
        //     denominator = get_min_abs(it_jets[0]);
        //     for(index_t j = 0; j < dim; ++j) {
        //         index_t n;
        //         numerator += get_max_abs(it_jets[1+j])*weights[j];
        //         denominator -= get_max_abs(it_jets[1+j])*weights[j];
        //         for(index_t k = 0; k < j; ++k) {
        //             n = 1 + dim + j*(j+1)/2 + k;
        //             numerator += get_max_abs(it_jets[n]) * weights[k] * weights[j];
        //             denominator -= get_max_abs(it_jets[n]) * weights[k] * weights[j];
        //         }
        //         n = 1 + dim + j*(j+1)/2 + j;
        //         numerator += 2 * get_max_abs(it_jets[n]) * weights[j] * weights[j];
        //         for(index_t k = j+1; k < dim; ++k) {
        //             n = 1 + dim + k*(k+1)/2 + j;
        //             numerator += get_max_abs(it_jets[n]) * weights[k] * weights[j];
        //         }
        //     }
        //     if(denominator < 0 ) {
        //         denominator = 0;
        //     }
        // }

        static void condition_number(const const_vec_iterator<Numeric> it_jets,
                                     const std::vector<typename Numeric::base_type> weights,
                                     typename Numeric::base_type & numerator,
                                     typename Numeric::base_type & denominator)
        {
            index_t dim = weights.size();
            numerator = get_max_abs(it_jets[0]);
            denominator = 0;
            for(index_t i = 0; i < weights.size(); ++i) {
                //numerator += get_rad(it_jets[1+i])*weights[i];
                denominator += get_min_abs(it_jets[1+i]);
            }
            //for(index_t j = 0; j < dim; ++j) {
            //    index_t n;
            //    typename Numeric::base_type current_denominator = get_min_abs(it_jets[1+j]);
            //    typename Numeric::base_type current_numerator = 0;
            //    for(index_t k = 0; k < j; ++k) {
            //        n = 1 + dim + j*(j+1)/2 + k;
            //        current_numerator += get_max_abs(it_jets[n]) * weights[k];
            //        //current_denominator -= get_max_abs(it_jets[n]) * weights[k];
            //    }
            //    n = 1 + dim + j*(j+1)/2 + j;
            //    current_numerator += get_max_abs(it_jets[n]) * weights[j] * 2;
            //    //current_denominator -= get_max_abs(it_jets[n]) * weights[j] * 2;
            //    for(index_t k = j+1; k < dim; ++k) {
            //        n = 1 + dim + k*(k+1)/2 + j;
            //        current_numerator += get_max_abs(it_jets[n]) * weights[k];
            //        //current_denominator -= get_max_abs(it_jets[n]) * weights[k];
            //    }
            //    if(current_denominator < 0 ) {
            //        current_denominator = 0;
            //    }
            //    if ((current_denominator == 0 and denominator == 0 and current_numerator < numerator) or
            //        (current_numerator * denominator < numerator * current_denominator)) {
            //        numerator = current_numerator;
            //        denominator = current_denominator;
            //    }
            //}
        }

        static void get_evals_slopes(const const_vec_iterator<Numeric> & it_jets,
                                     const std::vector<Numeric> & delta_box,
                                     Numeric & result_eval,
                                     const vec_iterator<Numeric> & result_slope,
                                     const vec_iterator<typename Numeric::base_type> & result_preshift)
        {
            const index_t dim = delta_box.size();
            typename Numeric::base_type r(0);
            // Numeric d(0);
            // std::vector<typename Numeric::base_type> rad_box(dim);
            // for(index_t j = 0; j < delta_box.size(); ++j) {
            //     if(contains_zero(it_jets[1+j])) {
            //         rad_box[j] = 0;
            //     } else if((it_jets[1+j].upper > 0) == (get_center(it_jets[0]) > 0)) {
            //         rad_box[j] = -delta_box[j].upper;
            //     } else {
            //         rad_box[j] = delta_box[j].upper;
            //     }
            // }
            result_eval = it_jets[0];
            for(index_t j = 0; j < delta_box.size(); ++j) {
                result_slope[j] = it_jets[1+j];
                result_preshift[j] = 0;

                index_t n;
                r = 0;
                for(index_t k = 0; k < j; ++k) {
                    n = 1 + dim + j*(j+1)/2 + k;
                    r += get_max_abs(it_jets[n]) * delta_box[k].upper;
                }
                n = 1 + dim + j*(j+1)/2 + j;
                r += get_max_abs(it_jets[n]) * delta_box[j].upper * 2;
                for(index_t k = j+1; k < dim; ++k) {
                    n = 1 + dim + k*(k+1)/2 + j;
                    r += get_max_abs(it_jets[n]) * delta_box[k].upper;
                }
                //slope with r/2 and gradient with r
                result_slope[j].upper = it_jets[1+j].upper + r/2;
                result_slope[j].neg_lower = it_jets[1+j].neg_lower + r/2;

                // index_t n;
                // r = 0;
                // // slope in the quadrant with transverse surface
                // for(index_t k = 0; k < j; ++k) {
                //     n = 1 + dim + j*(j+1)/2 + k;
                //     r += get_center(it_jets[n]) * rad_box[k] / 2;
                // }
                // n = 1 + dim + j*(j+1)/2 + j;
                // r += get_center(it_jets[n]) * rad_box[j];
                // for(index_t k = j+1; k < dim; ++k) {
                //     n = 1 + dim + k*(k+1)/2 + j;
                //     r += get_center(it_jets[n]) * rad_box[k] / 2;
                // }
                // result_slope[j].upper = it_jets[1+j].upper + r;
                // result_slope[j].neg_lower = it_jets[1+j].neg_lower - r;
                // fma(it_jets[1+j], Numeric(rad_box[j]/2), result_eval, result_eval);
                // add(Numeric(r/2 * rad_box[j]/2), result_eval, result_eval);
                // result_preshift[j] = rad_box[j]/2;
            }
        }

        static void get_gradient(const const_vec_iterator<Numeric> & it_jets,
                                 const std::vector<Numeric> & shift,
                                 const vec_iterator<Numeric> & result)
        {
            const index_t dim = shift.size();
            Numeric tmp(0);
            for(index_t j = 0; j < shift.size(); ++j) {
                index_t n;
                result[j] = it_jets[1+j];
                for(index_t k = 0; k < j; ++k) {
                    n = 1 + dim + j*(j+1)/2 + k;
                    //r += get_max_abs(it_jets[n]) * shift[k].upper / 2;
                    fma(it_jets[n], shift[k], result[j], result[j]);
                }
                n = 1 + dim + j*(j+1)/2 + j;
                //r += get_max_abs(it_jets[n]) * shift[j].upper;
                mul(it_jets[n], shift[j], tmp);
                add(tmp, result[j], result[j]);
                add(tmp, result[j], result[j]);
                for(index_t k = j+1; k < dim; ++k) {
                    n = 1 + dim + k*(k+1)/2 + j;
                    //r += get_max_abs(it_jets[n]) * shift[k].upper / 2;
                    fma(it_jets[n], shift[k], result[j], result[j]);
                }
                //result[j].upper = it_jets[1+j].upper + r;
                //result[j].neg_lower = it_jets[1+j].neg_lower + r;
            }
        }

        // returns the coordinate of the linear term in a variable of a jet
        static index_t indice_gradient(index_t, index_t variable)
        {
            return 1 + variable;
        }
};
#endif
