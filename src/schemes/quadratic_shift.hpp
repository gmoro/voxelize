#include <vector>
#include <cfenv>

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;
using boolean = unsigned char;

template<typename Numeric>
class Quadratic_shift
{
    public:
        static const int rounding_mode = FE_UPWARD;

        static index_t dimension(const index_t i) {
            return (i+2)*(i+1)/2; // 1 + i + i*(i+1)/2;
        }

        static void evaluate(const const_vec_iterator<Numeric> it_pol,
                             const index_t pol_size,
                             const const_vec_iterator<Numeric> it_vals,
                             const index_t vals_size,
                             const index_t level,
                             const vec_iterator<Numeric> it_result)
        {
            const index_t input_dim = dimension(level);
            const index_t output_dim = dimension(level+1);
            if(vals_size == 0) {
                return;
            }

            if(pol_size == 0) {
                std::fill(it_result, it_result + output_dim*vals_size, 0);
                return;
            }

            std::vector<Numeric> mid_vals(vals_size, 0);
            std::vector<Numeric> delta_vals(vals_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                mid(it_vals[k], mid_vals[k]);
                delta(it_vals[k], delta_vals[k]);
            }
            std::vector<Numeric> shift_pol(vals_size*input_dim*pol_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                for(index_t m = 0; m < input_dim; ++m) {
                    index_t shift_pos = pol_size*(input_dim*k+m); 
                    // Shift polynomial
                    shift_pol[shift_pos + pol_size-1] = it_pol[input_dim*(pol_size-1) + m];
                    for(index_t i = 1; i < pol_size; ++i) {
                        fma(shift_pol[shift_pos+pol_size-i], mid_vals[k], it_pol[input_dim*(pol_size-1-i)+m],
                            shift_pol[shift_pos+pol_size-1-i]);
                        for(index_t j = 0; j < i-1; ++j) {
                            fma(shift_pol[shift_pos+pol_size-i+j+1], mid_vals[k], shift_pol[shift_pos+pol_size-i+j],
                                shift_pol[shift_pos+pol_size-i+j]);
                        }
                    }
                    // Evaluate shifted polynomial
                    index_t pos = 0;
                    index_t offset = 0;
                    if(m==0) {
                        pos = output_dim - 1;
                        offset = 2;
                        it_result[output_dim*k] = shift_pol[shift_pos];
                        it_result[output_dim*k + level + 1] = shift_pol[shift_pos + 1];
                    } else if (m<=level) {
                        pos = output_dim - level - 2 + m;
                        offset = 1;
                        it_result[output_dim*k + m] = shift_pol[shift_pos];
                    } else {
                        pos = m + 1;
                        offset = 0;
                    }
                    it_result[output_dim*k + pos] = shift_pol[shift_pos + pol_size - 1];
                    for(index_t i = 1; i+offset < pol_size; ++i) {
                        fma(it_result[output_dim*k + pos], delta_vals[k], shift_pol[shift_pos + pol_size - 1 - i],
                            it_result[output_dim*k + pos]);
                    }
                }
            }
            //std::fesetround(round_state);
        }

        static bool is_non_zero(const const_vec_iterator<Numeric> it_jets, 
                                      std::vector<Numeric> & box)
        {
            index_t dim = box.size();
            std::vector<Numeric> br(dim, 0);
            Numeric c(0);
            Numeric   v(0);
            for(index_t j = 0; j < dim; ++j) {
                delta(box[j], box[j]);
            }
            // Compute gradient to check monotonicity
            for(index_t j = 0; j < dim; ++j) {
                index_t n;
                c = it_jets[j + 1];
                for(index_t k = 0; k < j; ++k) {
                    n = 1 + dim + j*(j+1)/2 + k;
                    fma(it_jets[n], box[k], c, c);
                }
                n = 1 + dim + j*(j+1)/2 + j;
                mul(box[j], Numeric(2), v);
                fma(it_jets[n], v, c, c);
                for(index_t k = j+1; k < dim; ++k) {
                    n = 1 + dim + k*(k+1)/2 + j;
                    fma(it_jets[n], box[k], c, c);
                }
                if(get_lower(c) > 0) {
                    rad(box[j], br[j]);
                } else if(get_upper(c) < 0) {
                    rad(box[j], br[j]);
                    neg(br[j], br[j]);
                } else {
                    br[j] = box[j];
                }

            }
            // Compute approximation with specialized variables
            c = it_jets[0];
            for(index_t j = 0; j < dim; ++j) {
                fma(it_jets[j + 1], box[j], c, c);
                for(index_t k = 0; k < j+1; ++k) {
                    index_t n = 1 + dim + j*(j+1)/2 + k;
                    mul(br[j], br[k], v);
                    fma(it_jets[n], v, c, c);
                }
            }
            return !contains_zero(c);
        } 

        static void is_non_zero_monotoneous(const const_vec_iterator<Numeric> it_jets, 
                                            std::vector<Numeric> & box,
                                            char & non_zero, char & monotoneous)
        {
            index_t dim = box.size();
            std::vector<Numeric> br(dim, 0);
            Numeric c(0);
            Numeric   v(0);
            monotoneous = false;
            for(index_t j = 0; j < dim; ++j) {
                delta(box[j], box[j]);
            }
            // Compute gradient to check monotonicity
            for(index_t j = 0; j < dim; ++j) {
                index_t n;
                c = it_jets[j + 1];
                for(index_t k = 0; k < j; ++k) {
                    n = 1 + dim + j*(j+1)/2 + k;
                    fma(it_jets[n], box[k], c, c);
                }
                n = 1 + dim + j*(j+1)/2 + j;
                mul(box[j], Numeric(2), v);
                fma(it_jets[n], v, c, c);
                for(index_t k = j+1; k < dim; ++k) {
                    n = 1 + dim + k*(k+1)/2 + j;
                    fma(it_jets[n], box[k], c, c);
                }
                if(get_lower(c) > 0) {
                    rad(box[j], br[j]);
                    monotoneous = true;
                } else if(get_upper(c) < 0) {
                    rad(box[j], br[j]);
                    neg(br[j], br[j]);
                    monotoneous = true;
                } else {
                    br[j] = box[j];
                }

            }
            // Compute approximation with specialized variables
            c = it_jets[0];
            for(index_t j = 0; j < dim; ++j) {
                fma(it_jets[j + 1], box[j], c, c);
                for(index_t k = 0; k < j+1; ++k) {
                    index_t n = 1 + dim + j*(j+1)/2 + k;
                    mul(br[j], br[k], v);
                    fma(it_jets[n], v, c, c);
                }
            }
            non_zero = sign(c);
        } 
};
