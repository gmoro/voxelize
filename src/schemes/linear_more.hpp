#include <vector>
#include <cfenv>
#include "tensor/box.hpp"

using index_t = size_t;
template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;
template<typename T>
using vec_iterator = typename std::vector<T>::iterator;

template<typename coeff_t, typename val_t = coeff_t>
class Linear_more
{
    public:
        static const int rounding_mode = FE_UPWARD;

        static index_t dimension(const index_t i) {
            return 1 << i;
        }

        static void evaluate(const const_vec_iterator<coeff_t> it_pol,
                             const index_t pol_size,
                             const const_vec_iterator<val_t> it_vals,
                             const index_t vals_size,
                             const index_t level,
                             const vec_iterator<coeff_t> it_result)
        {
            const index_t input_dim = dimension(level);
            const index_t output_dim = dimension(level+1);
            if(vals_size == 0) {
                return;
            }

            if(pol_size == 0) {
                std::fill(it_result, it_result + output_dim*vals_size, 0);
                return;
            }

            // Initialize loop
            std::vector<val_t> mid_vals(vals_size, 0);
            for(index_t k = 0; k < vals_size; ++k) {
                mid(it_vals[k], mid_vals[k]);
                for(index_t i = 0; i < input_dim; ++i) {
                    it_result[output_dim*k+2*i] = it_pol[input_dim*(pol_size-1)+i];
                    it_result[output_dim*k+2*i+1] = 0;
                }
            }

            // Main evaluation loop
            for(index_t j = 1; j < pol_size; ++j) {
                for(index_t k = 0; k < vals_size; ++k) {
                    for(index_t i = 0; i < input_dim; ++i) {
                        fma(it_result[output_dim*k+2*i+1], it_vals[k], it_result[output_dim*k+2*i],
                            it_result[output_dim*k+2*i+1]);
                        fma(it_result[output_dim*k+2*i], mid_vals[k], it_pol[input_dim*(pol_size-1-j)+i],
                            it_result[output_dim*k+2*i]);
                    }
                }
            }
        }

        static void is_non_zero(const const_vec_iterator<coeff_t> jets, 
                                const Tensor_box<val_t> & box_set,
                                const vec_iterator<boolean> result) {
            index_t dim = box_set.get_number_axes();
            index_t output_dim = dimension(dim);
            index_t N   = box_set.get_number_elements();
            std::vector<index_t> it_boxes(dim, 0);
            std::vector<val_t> b(dim, 0);
            std::vector<coeff_t> c(output_dim, 0);
            val_t db(0);
            for(index_t i = 0; i < N; ++i) {
                get_box(box_set, it_boxes, b);
                std::copy(jets + i*output_dim, jets + (i+1)*output_dim, c.begin());
                for(index_t j = 0; j < dim; ++j) {
                    delta(b[dim-1-j], db);
                    index_t shift_dim = output_dim >> (j+1);
                    for(index_t k = 0; k < shift_dim;++k) {
                        fma(c[2*k+1], db, c[2*k], c[k]);
                    }
                }
                result[i] = !contains_zero(c[0]);
                box_set.advance(it_boxes);
            }
        } 
};
