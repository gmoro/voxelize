/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#ifndef INTERVAL_SMALL_HPP
#define INTERVAL_SMALL_HPP
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include <cfenv>
#include <algorithm>
#ifdef FLOAT128
#include <quadmath.h>
#endif

using index_t = size_t;

template<typename Numeric>
class Interval
{
    public:
        typedef Numeric base_type;
        Numeric neg_lower;
        Numeric upper;

        Interval(const Numeric & d) {
            neg_lower = -d;
            upper = d;
        }

        Interval(const Numeric & x, const Numeric & y) {
            neg_lower = -x;
            upper = y;
        }

        template<typename Numeric2>
        Interval(const Interval<Numeric2> & i) {
            const int round_state = std::fegetround();
            std::fesetround(FE_UPWARD);
            neg_lower = static_cast<Numeric>(i.neg_lower);
            upper = static_cast<Numeric>(i.upper);
            std::fesetround(round_state);
        }
};

template<typename Numeric>
inline Numeric get_lower(const Interval<Numeric> & a) { return -a.neg_lower; }
template<typename Numeric>
inline Numeric get_upper(const Interval<Numeric> & a) { return a.upper; }
template<typename Numeric>
inline Numeric get_max_abs(const Interval<Numeric> & a) { return a.upper > a.neg_lower? a.upper : a.neg_lower; }
template<typename Numeric>
inline Numeric get_min_abs(const Interval<Numeric> & a) {
    if(a.upper < 0) {
        return -a.upper;
    } else if(a.neg_lower < 0) {
        return -a.neg_lower;
    } else {
        return 0;
    }
}
template<typename Numeric>
inline Numeric get_near_zero(const Interval<Numeric> & a) {
    if(a.upper < 0) {
        return a.upper;
    } else if(a.neg_lower < 0) {
        return -a.neg_lower;
    } else {
        return 0;
    }
}
template<typename Numeric>
inline Numeric get_far_from_zero(const Interval<Numeric> & a) {
    return a.upper >= a.neg_lower? a.upper : -a.neg_lower;
}
template<typename Numeric>
inline Numeric get_center(const Interval<Numeric> & a) { return (a.upper - a.neg_lower)/2; }
template<typename Numeric>
inline Numeric get_rad(const Interval<Numeric> & a) { return (a.upper + a.neg_lower)/2; }
template<typename Numeric>
inline bool   contains_zero(const Interval<Numeric> & a) {
    // NaN should match
    return !(a.neg_lower < 0) and !(a.upper < 0);
}
template<typename Numeric>
inline char sign(const Interval<Numeric> & a) {
    // NaN should match
    if(a.upper < 0) {
        return -1;
    } else if (a.neg_lower < 0) {
        return 1;
    } else {
        return 0;
    }
}

template<template<typename Numeric> class Interval>
int rounding_mode() { return FE_UPWARD; }


template<typename Numeric>
inline std::ostream& operator<< (std::ostream& os, const Interval<Numeric> & a)
{
    os << "[" << get_lower(a) << ", " << get_upper(a) << "]";
    return os;
}

static void fail_parsing_interval() {
    fprintf(stderr, "An number could not be parsed as an interval.\n");
    exit(EXIT_FAILURE);
}

template<typename Numeric>
inline std::istream& operator>> (std::istream& is, Interval<Numeric> & a)
{
    std::string number_text;
    std::stringstream number_stream;
    const int round_state = std::fegetround();
    is >> std::ws;
    if(is.peek() == '[') {
        int pos;
        std::getline(is, number_text, ']');
        std::fesetround(FE_DOWNWARD);
        std::sscanf(number_text.c_str(), "[ %Lf %*[,;] %n", &a.neg_lower, &pos);
        a.neg_lower = -a.neg_lower;
        std::fesetround(FE_UPWARD);
        std::sscanf(number_text.c_str() + pos, "%Lf", &a.upper);
    } else {
        is >> number_text;
        std::fesetround(FE_UPWARD);
        number_stream = std::stringstream(number_text);
        number_stream >> a.upper;
        std::fesetround(FE_DOWNWARD);
        number_stream = std::stringstream(number_text);
        number_stream >> a.neg_lower;
        a.neg_lower = -a.neg_lower;
    }
    std::fesetround(round_state);
    return is;
}

// Center of the interval
// Robust to aliasing
template<typename Numeric>
inline void mid(const Interval<Numeric> & input, Interval<Numeric> & output)
{
    Numeric d = (input.upper - input.neg_lower)/2;
    output.neg_lower = -d;
    output.upper = d;
}
// Interval [radius, radius]
template<typename Numeric>
inline void rad(const Interval<Numeric> & input, Interval<Numeric> & output)
{
    Numeric d_up = (input.upper + input.neg_lower)/2;
    Numeric d_down = (-input.upper - input.neg_lower)/2;
    output.upper = d_up;
    output.neg_lower = d_down;
}
// Interval [-radius, radius]
template<typename Numeric>
inline void delta(const Interval<Numeric> & input, Interval<Numeric> & output)
{
    Numeric d = (input.upper + input.neg_lower)/2;
    output.neg_lower = d;
    output.upper = d;
}

template<typename Numeric>
inline void crop(const Interval<Numeric> & input, Interval<Numeric> & output)
{
    if(output.upper > input.upper) {
        output.upper = input.upper;
        if( output.neg_lower < -output.upper) {
            output.neg_lower = -output.upper;
        }
    }
    if(output.neg_lower > input.neg_lower) {
        output.neg_lower = input.neg_lower;
        if( output.upper < -output.neg_lower) {
            output.upper = -output.neg_lower;
        }
    }
}

template<typename Numeric>
inline void shift_to_zero(const Interval<Numeric> & a, Interval<Numeric> & output) {
    if(a.upper < 0) {
        output.upper = 0;
        output.neg_lower = a.upper + a.neg_lower;
    } else if(a.neg_lower < 0) {
        output.neg_lower = 0;
        output.upper = a.upper + a.neg_lower;
    } else {
        output.upper = a.upper;
        output.neg_lower = a.neg_lower;
    }
}

// lower and upper should not overlap; they can overlap with input
template<typename Numeric>
inline void split(const Interval<Numeric> & input, Interval<Numeric> & lower, Interval<Numeric> & upper)
{
    Numeric d = (input.upper - input.neg_lower)/2;
    upper.upper = input.upper;
    lower.neg_lower = input.neg_lower;
    upper.neg_lower = -d;
    lower.upper = d;
}

// Robust to aliasing
template<typename Numeric>
inline void split(const Interval<Numeric> & input, int n, std::vector<Interval<Numeric>> & result)
{
    Numeric d = (input.upper + input.neg_lower)/n;
    Numeric nl = input.neg_lower;
    result.resize(n, 0);
    for(int i = 0; i < n; ++i) {
        result[i].neg_lower = nl + (-i)*d;
        result[i].upper = (i+1)*d - nl;

    }
}

// Robust to aliasing
template<typename Numeric>
inline void add(const Interval<Numeric> & a, const Interval<Numeric> & b, Interval<Numeric> & c)
{
    c.neg_lower = a.neg_lower + b.neg_lower;
    c.upper = a.upper + b.upper;
}

// Robust to aliasing
template<typename Numeric>
inline void neg(const Interval<Numeric> & a, Interval<Numeric> & result)
{
    Numeric neg_lower;
    neg_lower = a.upper;
    result.upper = a.neg_lower;
    result.neg_lower = neg_lower;
}

// Robust to aliasing
// TODO: Bug if interval contains zero !!
template<typename Numeric>
inline void inv(const Interval<Numeric> & a, Interval<Numeric> & result)
{
    Numeric neg_upper, lower;
    neg_upper = -a.upper;
    lower = -a.neg_lower;
    if(neg_upper == 0) {
        result.neg_lower = std::numeric_limits<Numeric>::infinity();
    } else {
        result.neg_lower = 1/neg_upper;
    }
    if(lower == 0) {
        result.upper = std::numeric_limits<Numeric>::infinity();
    } else {
        result.upper = 1/lower;
    }
}

// Robust to aliasing
template<typename Numeric>
inline void add(const Interval<Numeric> & a, Interval<Numeric> & res)
{
    res.neg_lower += a.neg_lower;
    res.upper += a.upper;
}

// Robust to aliasing
template<typename Numeric>
inline void mul(const Interval<Numeric> & a, const Interval<Numeric> & b, Interval<Numeric> & c) 
{
    Numeric neg_lower, upper;
    if(a.neg_lower <= 0) {
        if(b.neg_lower <= 0) {
            neg_lower = (-a.neg_lower) * b.neg_lower;
            upper = a.upper * b.upper;
        } else {
            if(b.upper > 0) {
                neg_lower = a.upper * b.neg_lower;
                upper = a.upper * b.upper;
            } else {
                neg_lower =  a.upper * b.neg_lower;
                upper = (-a.neg_lower) * b.upper;
            }
        }
    } else {
        if(a.upper > 0) {
            if(b.neg_lower <= 0) {
                neg_lower = a.neg_lower * b.upper;
                upper = a.upper * b.upper;
            } else {
                if(b.upper > 0) {
                    Numeric u = a.neg_lower * b.upper;
                    Numeric v = a.upper * b.neg_lower;
                    neg_lower = (u>v)?u:v;
                    u = a.neg_lower * b.neg_lower;
                    v = a.upper * b.upper;
                    upper = (u>v)?u:v;
                } else {
                    neg_lower = a.upper * b.neg_lower;
                    upper = a.neg_lower * b.neg_lower;
                }
            }
        } else {
            if(b.neg_lower <= 0) {
                neg_lower = a.neg_lower * b.upper;
                upper = a.upper * (-b.neg_lower);
            } else {
                if(b.upper > 0) {
                    neg_lower = a.neg_lower * b.upper;
                    upper = a.neg_lower * b.neg_lower;
                } else {
                    neg_lower = (-a.upper) * b.upper;
                    upper = a.neg_lower * b.neg_lower;
                }
            }
        }
    }
    c.neg_lower = neg_lower;
    c.upper = upper;
}

template<typename Numeric>
inline void naive_fma(const Interval<Numeric> & a, const Interval<Numeric> & b, const Interval<Numeric> & c, Interval<Numeric> & res) 
{
    mul(a, b, res);
    add(res, c, res);
}

// Robust to aliasing
template<typename Numeric>
inline void fma(const Interval<Numeric> & a, const Interval<Numeric> & b, const Interval<Numeric> & c, Interval<Numeric> & res) 
{
    Numeric neg_lower, upper;
    if(a.neg_lower <= 0) {
        if(b.neg_lower <= 0) {
            neg_lower = (-a.neg_lower) * b.neg_lower + c.neg_lower;
            upper = a.upper * b.upper + c.upper;
        } else {
            if(b.upper > 0) {
                neg_lower = a.upper * b.neg_lower + c.neg_lower;
                upper = a.upper * b.upper + c.upper;
            } else {
                neg_lower =  a.upper * b.neg_lower + c.neg_lower;
                upper = (-a.neg_lower) * b.upper + c.upper;
            }
        }
    } else {
        if(a.upper > 0) {
            if(b.neg_lower <= 0) {
                neg_lower = a.neg_lower * b.upper + c.neg_lower;
                upper = a.upper * b.upper + c.upper;
            } else {
                if(b.upper > 0) {
                    Numeric u = a.neg_lower * b.upper;
                    Numeric v = a.upper * b.neg_lower;
                    neg_lower = ((u>v)?u:v) + c.neg_lower;
                    u = a.neg_lower * b.neg_lower;
                    v = a.upper * b.upper;
                    upper = ((u>v)?u:v) + c.upper;
                } else {
                    neg_lower = a.upper * b.neg_lower + c.neg_lower;
                    upper = a.neg_lower * b.neg_lower + c.upper;
                }
            }
        } else {
            if(b.neg_lower <= 0) {
                neg_lower = a.neg_lower * b.upper + c.neg_lower;
                upper = a.upper * (-b.neg_lower) + c.upper;
            } else {
                if(b.upper > 0) {
                    neg_lower = a.neg_lower * b.upper + c.neg_lower;
                    upper = a.neg_lower * b.neg_lower + c.upper;
                } else {
                    neg_lower = (-a.upper) * b.upper + c.neg_lower;
                    upper = a.neg_lower * b.neg_lower + c.upper;
                }
            }
    }
    }
    res.neg_lower = neg_lower;
    res.upper = upper;
}



template<typename T>
using const_vec_iterator = typename std::vector<T>::const_iterator;

template<typename Interval>
void determinant(const std::vector<std::vector<Interval>> & M,
                 const std::vector<index_t> & ind_axe_0,
                 const std::vector<index_t> & ind_axe_1,
                 Interval & result)
{
    index_t N = ind_axe_0.size();
    if(N == 1) {
        result = M[ind_axe_0[0]][ind_axe_1[0]];
        return;
    }
    std::vector<index_t> new_axe_0 = ind_axe_0;
    std::vector<index_t> new_axe_1 = ind_axe_1;
    new_axe_0.resize(N-1);
    new_axe_1.resize(N-1);
    Interval tmp(0);
    result = 0;
    for(index_t i = 0; i < N; ++i) {
        determinant(M, new_axe_0, new_axe_1, tmp);
        if( i % 2 == 1) {
            neg(tmp, tmp);
        }
        fma(tmp, M[ind_axe_0[N-1]][ind_axe_1[N-1-i]], result, result);
        if(i < N-1) {
            new_axe_1[N - 2 - i] = ind_axe_1[N - 1 - i];
        }
    }
}

template<typename Interval>
void left_kernel(const std::vector<std::vector<Interval>> & M,
                 const std::vector<index_t> & ind_axe_0,
                 const std::vector<index_t> & ind_axe_1,
                 std::vector<Interval> & result)
{
    using Numeric = typename Interval::base_type;
    // this function handles only the case n = m + 1
    index_t n = ind_axe_0.size();
    index_t m = ind_axe_1.size();
    result.resize(n, 0);
    // Handle trivial cases
    if (m == 0) {
        result[0] = 1;
        return;
    } else if(m == 1) {
        //if(contains_zero(M[ind_axe_0[0]][ind_axe_1[0]])) {
        //    result[0] = 0;
        //    result[1] = 0;
        //} else {
        //    result[0] = - get_center(M[ind_axe_0[1]][ind_axe_1[0]]) / get_center(M[ind_axe_0[0]][ind_axe_1[0]]);
        //    result[1] = 1;
        //}
        mid(M[ind_axe_0[0]][ind_axe_1[0]], result[1]);
        mid(M[ind_axe_0[1]][ind_axe_1[0]], result[0]);
        // keep last coordinate positive
        //if(M[ind_axe_0[0]][ind_axe_1[0]].neg_lower < 0) {
        if(result[1].neg_lower < 0) {
            neg(result[0], result[0]);
        //} else if(M[ind_axe_0[0]][ind_axe_1[0]].upper < 0) {
        } else if(result[1].upper < 0) {
            neg(result[1], result[1]);
        } else {
            result[0] = 1;
            result[1] = 0;
        }
        return;
    }

    // transpose and get the centers
    std::vector<std::vector<Numeric>> matrix(m, std::vector<Numeric>(n, 0));
    for(index_t i = 0; i < n; ++i) {
        for(index_t j = 0; j < m; ++j) {
           matrix[j][i] = get_center(M[ind_axe_0[i]][ind_axe_1[j]]); 
        }
    }
    // do Gaussian elimination
    index_t defect = m;
    for(index_t i = 0; i < m; ++i) {
        Numeric max = matrix[i][i]>=0?matrix[i][i]:-matrix[i][i];
        index_t argmax = i;
        for(index_t j = i+1; j < m; ++j) {
            if(max < matrix[j][i]) {
                max = matrix[j][i];
                argmax = j;
            } else if(max < -matrix[j][i]) {
                max = -matrix[j][i];
                argmax = j;
            }
        }
        std::swap(matrix[i], matrix[argmax]);
        if(matrix[i][i] == 0) {
            std::fill(result.begin(), result.end(), 0);
            defect = i;
            //return;
            break;
        }
        Numeric p = 1/matrix[i][i];
        for(index_t k = i + 1; k < n; ++k) {
            matrix[i][k] *= p;
        }
        for(index_t j = i + 1; j < m; ++j) {
            for(index_t k = i + 1; k < n; ++k) {
                matrix[j][k] -= matrix[i][k] * matrix[j][i];
            }
        }
    }
    // solve triangular system
    result[defect] = Interval(1);
    for(index_t i = 0; i < defect; ++i) {
        result[n-2-i] = Interval(-matrix[m-1-i][n-1-i]);
        for(index_t j = 0; j < defect - i - 1; ++j) {
            matrix[j][defect-1-i] = matrix[j][defect-i] - matrix[j][defect-1-i] * matrix[defect-1-i][defect-i];
        }
    }
}

template<typename Interval>
void solve(const std::vector<std::vector<Interval>> & M,
           std::vector<Interval> & result)
{
    using Numeric = typename Interval::base_type;
    // this function handles only the case n = m + 1
    index_t m = M.size();
    result.resize(m+1, 0);
    // Handle trivial cases
    if (m == 0) {
        return;
    } else if(m == 1) {
        if(contains_zero(M[0][0])) {
            result[0] = Interval(-1,1);
        } else {
            result[0] = - get_center(M[0][1]) / get_center(M[0][0]);
        }
        //mid(M[ind_axe_0[0]][ind_axe_1[0]], result[1]);
        //mid(M[ind_axe_0[1]][ind_axe_1[0]], result[0]);
        //// keep last coordinate positive
        //if(M[ind_axe_0[0]][ind_axe_1[0]].neg_lower < 0) {
        //    neg(result[0], result[0]);
        //} else if(M[ind_axe_0[0]][ind_axe_1[0]].upper < 0) {
        //    neg(result[1], result[1]);
        //} else {
        //    result[0] = 0;
        //    result[1] = 1;
        //}
        return;
    } else if(m == 2) {
        Numeric c[2][2] = {{get_center(M[1][1]),
                             -get_center(M[0][1])},
                            {-get_center(M[1][0]),
                             get_center(M[0][0])}};
        Numeric d = c[0][0]*c[1][1] - c[0][1]*c[1][0];
        if(d == 0) {
            result[0] = Interval(-1,1);
            result[1] = Interval(-1,1);
        } else {
            result[0] = - (c[0][0]*get_center(M[0][2]) + c[0][1]*get_center(M[1][2]))/d;
            result[1] = - (c[1][0]*get_center(M[0][2]) + c[1][1]*get_center(M[1][2]))/d;
        }
        return;
    }

    // get the centers
    std::vector<std::vector<Numeric>> matrix(m, std::vector<Numeric>(m+1, 0));
    for(index_t i = 0; i < m; ++i) {
        for(index_t j = 0; j < m+1; ++j) {
           matrix[i][j] = get_center(M[i][j]); 
        }
    }
    // do Gaussian elimination
    for(index_t i = 0; i < m; ++i) {
        Numeric max = matrix[i][i]>=0?matrix[i][i]:-matrix[i][i];
        index_t argmax = i;
        for(index_t j = i+1; j < m; ++j) {
            if(max < matrix[j][i]) {
                max = matrix[j][i];
                argmax = j;
            } else if(max < -matrix[j][i]) {
                max = -matrix[j][i];
                argmax = j;
            }
        }
        std::swap(matrix[i], matrix[argmax]);
        if(matrix[i][i] == 0) {
            //std::cout << "Warning: nul pivot:\n";
            //for(index_t j = 0; j < m; ++j) {
            //    for(index_t k = 0; k < m+1; ++k) {
            //        std::cout << Interval(matrix[j][k]) << "; ";
            //    }
            //    std::cout << "\n";
            //}
            //std::cout << "\n";
            if(matrix[i][m] == 0) {
                std::fill(result.begin(), result.end(), Interval(-1,1));
            } else if(matrix[i][m] > 0) {
                std::fill(result.begin(), result.end(), Interval(-1));
            } else {
                std::fill(result.begin(), result.end(), Interval(1));
            }
            // result.back() = 1;
            return;
        }
        Numeric p = 1/matrix[i][i];
        for(index_t k = i + 1; k < m+1; ++k) {
            matrix[i][k] *= p;
        }
        for(index_t j = i + 1; j < m; ++j) {
            for(index_t k = i + 1; k < m+1; ++k) {
                matrix[j][k] -= matrix[i][k] * matrix[j][i];
            }
        }
    }
    // solve triangular system
    for(index_t i = 0; i < m; ++i) {
        result[m-1-i] = Interval(-matrix[m-1-i][m-i]);
        for(index_t j = 0; j < m - i - 1; ++j) {
            //matrix[j][m-1-i] = matrix[j][m-i] - matrix[j][m-1-i] * matrix[m-1-i][m-i];
            matrix[j][m-1-i] *= matrix[m-1-i][m-i];
            matrix[j][m-1-i] = matrix[j][m-i] - matrix[j][m-1-i];
        }
    }
}


// Robust to aliasing if ind_axe_0 and ind_axe_1 are identity
template<typename Interval>
void row_echelon(const std::vector<std::vector<Interval>> & M,
                 std::vector<std::vector<Interval>> & result)
{
    using Numeric = typename Interval::base_type;
    // this function handles only the case n = m + 1
    index_t m = M.size();
    if (m == 0) {
        return;
    }
    index_t n = M[0].size();
    result.resize(m, std::vector<Interval>(n, 0));
    // Handle trivial cases
    if(m == 1) {
        Interval c = M[0][0];
        result[0][1] = M[0][1];
        result[0][0] = c;
        return;
    } else if(m == 2) {
        Interval c[2][2] = {{get_center(M[1][1]),
                             -get_center(M[0][1])},
                            {-get_center(M[1][0]),
                             get_center(M[0][0])}};
        Interval col[2] = {0, 0};
        for(index_t j = 0; j < 3; ++j) {
            for(index_t i = 0; i < 2; ++i) {
                //result[i][j] = 0;
                mul(c[i][0], M[0][j], col[i]);
                fma(c[i][1], M[1][j], col[i],
                    col[i]);
            }
            result[0][j] = col[0];
            result[1][j] = col[1];
        }
        return;
    }

    // get the centers along identity matrix
    std::vector<std::vector<Numeric>> matrix(m, std::vector<Numeric>(2*m, 0));
    for(index_t i = 0; i < m; ++i) {
        for(index_t j = 0; j < m; ++j) {
           matrix[i][j] = get_center(M[i][j]); 
        }
        matrix[i][m+i] = 1;
    }
    // do Gaussian elimination
    for(index_t i = 0; i < m; ++i) {
        Numeric max = matrix[i][i]>=0?matrix[i][i]:-matrix[i][i];
        index_t argmax = i;
        for(index_t j = i+1; j < m; ++j) {
            if(max < matrix[j][i]) {
                max = matrix[j][i];
                argmax = j;
            } else if(max < -matrix[j][i]) {
                max = -matrix[j][i];
                argmax = j;
            }
        }
        std::swap(matrix[i], matrix[argmax]);
        if(matrix[i][i] == 0) {
            continue;
            // std::fill(result.begin(), result.end(), 0);
            // // result.back() = 1;
            // return;
        }
        Numeric p = 1/matrix[i][i];
        for(index_t k = i + 1; k < 2*m; ++k) {
            matrix[i][k] *= p;
        }
        for(index_t j = 0; j < m; ++j) {
            if(j != i) {
                for(index_t k = i + 1; k < 2*m; ++k) {
                    matrix[j][k] -= matrix[i][k] * matrix[j][i];
                }
            }
        }
    }
    // apply inverse to original matrix
    std::vector<Interval> col(m, 0);;
    for(index_t j = 0; j < n; ++j) {
        for(index_t i = 0; i < m; ++i) {
            col[i] = 0;
            for(index_t k = 0; k < m; ++k) {
                fma(Interval(matrix[i][m + k]), M[k][j], col[i],
                    col[i]);
            }
        }
        for(index_t i = 0; i < m; ++i) {
            result[i][j] = col[i];
        }
    }
}

// Solve in the box [-1, 1]^m
template<typename Interval>
void solve_diagonal_dominant(const std::vector<std::vector<Interval>> & M,
                                    std::vector<Interval> & result)
{
    index_t m = M.size();
    index_t n = m+1;
    result.resize(m, 0);
    Interval unit(-1,1);
    //Interval double_disk(-2,2);
    for(index_t i = 0; i < m; ++i) {
        Interval c(0);
        if(contains_zero(M[i][i])) {
            //std::cout << "unstable\n";
            if(M[i][i].upper * get_far_from_zero(M[i][n-1]) >= M[i][i].neg_lower * get_far_from_zero(M[i][n-1])) {
                result[i] = 1;
            } else {
                result[i] = -1;
            }
            //mul(unit, Interval(2), result[i]);
            //result[i] = double_disk;
            continue;
        } else {
            inv(M[i][i], c);
        }
        Interval a(M[i][n-1]);
        for(index_t j = 0; j < n-1; ++j) {
            if(j != i) {
                fma(M[i][j], unit, a, a);
            }
        }
        mul(c, a, a);
        neg(a, result[i]);
        //if(get_upper(result[i]) > 1) {
        //    result[i].upper = 1;
        //} else if(get_upper(result[i]) < -1) {
        //    result[i].upper = -1;
        //}

        //if(get_lower(result[i]) < -1) {
        //    result[i].neg_lower = 1;
        //} else if(get_lower(result[i]) > 1) {
        //    result[i].neg_lower = -1;
        //}
    }
}

//template<typename Interval>
//void left_kernel(const std::vector<std::vector<Interval>> & M,
//                 const std::vector<index_t> & ind_axe_0,
//                 const std::vector<index_t> & ind_axe_1,
//                 std::vector<Interval> & result)
//{
//    index_t N = ind_axe_0.size();
//    std::vector<index_t> new_axe_0 = ind_axe_0;
//    new_axe_0.resize(N-1);
//    Interval tmp(0);
//    result.resize(N,0);
//    for(index_t i = 0; i < N; ++i) {
//        determinant(M, new_axe_0, ind_axe_1, result[N-1-i]);
//        if( i % 2 == 1) {
//            neg(result[N-i-1], result[N-i-1]);
//        }
//        if(i < N-1) {
//            new_axe_0[N - 2 - i] = ind_axe_0[N - 1 - i];
//        }
//    }
//}

template<typename Interval, typename vector>
void product_row_matrix(const std::vector<Interval> & row,
                        const std::vector<std::vector<Interval>> & M,
                        const std::vector<index_t> & ind_axe_0,
                        const std::vector<index_t> & ind_axe_1,
                        vector & result)
{
    //result.resize(ind_axe_1.size(), 0);
    std::fill(result.begin(), result.end(), 0);
    for(index_t i = 0; i < ind_axe_0.size(); ++i) {
        for(index_t j = 0; j < ind_axe_1.size(); ++j) {
            fma(row[i], M[ind_axe_0[i]][ind_axe_1[j]], result[j], result[j]);
        }
    }
}

template<typename Interval>
void product_row_vector(const std::vector<Interval> & row,
                        const std::vector<Interval> & V,
                        const std::vector<index_t> & ind_axe_0,
                        Interval & result)
{
    result = 0;
    for(index_t i = 0; i < ind_axe_0.size(); ++i) {
        fma(row[i], V[ind_axe_0[i]], result, result);
    }
}

using Interval32 = Interval<float>;
using Interval64 = Interval<double>;
using Interval80 = Interval<long double>;

// Specific __flot128 implementation
#ifdef FLOAT128
template<>
inline std::ostream& operator<< <__float128> (std::ostream& os, const Interval<__float128> & a)
{
    char buf[128];
    os << "[";
    quadmath_snprintf(buf, sizeof(buf), "%+-#6.20Qe", get_lower(a));
    os << buf << ", ";
    quadmath_snprintf(buf, sizeof(buf), "%+-#6.20Qe", get_upper(a));
    os << buf << "]";
    return os;
}


template<>
inline std::istream& operator>> <__float128>(std::istream& is, Interval<__float128> & a)
{
    std::string number_text;
    std::stringstream number_stream;
    const int round_state = std::fegetround();
    if(! (is >> std::ws)) { return is; }
    if(is.peek() == '[') {
        is.ignore();
        // read the lower part
        std::getline(is, number_text, ',');
        std::fesetround(FE_DOWNWARD);
        a.neg_lower = strtoflt128(number_text.c_str(), nullptr);
        a.neg_lower = -a.neg_lower;
        // read the upper part
        std::getline(is, number_text, ']');
        std::fesetround(FE_UPWARD);
        a.upper = strtoflt128(number_text.c_str(), nullptr);
        std::fesetround(round_state);
    } else {
        is >> number_text;
        std::fesetround(FE_UPWARD);
        a.upper = strtoflt128(number_text.c_str(), nullptr);
        std::fesetround(FE_DOWNWARD);
        a.neg_lower = strtoflt128(number_text.c_str(), nullptr);
        a.neg_lower = -a.neg_lower;
        std::fesetround(round_state);
    }
    return is;
}

using Interval128 = Interval<__float128>;
#endif

#endif
