/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#include <fstream>
#include <sstream>
#include <chrono>
#include <unistd.h>
#include <cstring>
#include <algorithm>
#include <deque>
#include <vector>
#include "../tensor.hpp"
#include "../interval.hpp"
#include "../schemes/horner.hpp"
#include "../schemes/linear.hpp"
#include "../schemes/quadratic.hpp"
//#include "../schemes/quadratic_shift.hpp"


constexpr double l_default = -2;
constexpr double u_default = 2;
#ifdef FLOAT128
using Iprecise = Interval128;
#else
using Iprecise = Interval80;
#endif


template<typename T>
void remove(std::vector<T> & vec, const std::vector<char> & table, index_t dim = 1)
{
    index_t c = 0;
    for(index_t k=0; k < table.size(); ++k) {
        if(table[k]) {
            c++;
        } else if(c > 0) {
            for(index_t i = 0; i < dim; ++i) {
                vec[dim*(k-c) + i] = vec[dim*k + i];
            }
        }
    }
    vec.resize(vec.size() - dim*c, 0);
}

template<typename T>
void stable_partition(std::vector<T> & vec, const std::vector<char> & table, index_t dim = 1)
{
    index_t c = 0;
    std::vector<T> buffer;
    buffer.resize(vec.size(), 0);
    for(index_t k=0; k < table.size(); ++k) {
        if(!table[k]) {
            for(index_t i = 0; i < dim; ++i) {
                buffer[dim*c + i] = vec[dim*k + i];
            }
            c++;
        } else if(c > 0) {
            for(index_t i = 0; i < dim; ++i) {
                vec[dim*(k-c) + i] = vec[dim*k + i];
            }
        }
    }
    for(index_t i = 0; i < dim*c; ++i) {
        vec[vec.size() - dim*c + i] = buffer[i];
    }
}

template<template<typename I> class Scheme, typename I>
void voxelize_loop(const std::vector<Tensor_poly<I>> & system, const std::vector<char> & signs,
                   Tensor_box<I> & box_set, Tensor_box<I> & box_set_certified,
                   std::vector<std::vector<I>> & evals, 
                   bool guarantee, bool clean_eval, index_t verbose, double inflation_scale)
{
    std::vector<char> nonzeroes;
    std::vector<char> table;
    std::vector<char> match_signs;
    std::vector<char> match_nonzeroes;
    std::vector<char> zeroes;
    index_t dim = Scheme<I>::dimension(system[0].get_number_axes());
    
    bool has_signs_zero = std::find(signs.begin(), signs.end(), 0) != signs.end();
    index_t num_equalities = std::count(signs.begin(), signs.end(), 0);
    std::vector<index_t> ind_equalities(0);
    if(guarantee and num_equalities == 1 and has_signs_zero and Scheme<I>::has_gradient) {
        box_set_certified = box_set;
        inflate(box_set_certified, I(inflation_scale));
    }
    match_signs.resize(box_set.get_number_elements(), 1);
    match_nonzeroes.resize(box_set.get_number_elements(), 0);
    for(index_t j = 0; j < system.size(); ++j) {
        std::vector<I> & last_eval = Scheme<I>::has_gradient?
            evals[j] : evals[0];
        if(guarantee and system.size() == 1 and signs[j] == 0 and Scheme<I>::has_gradient) {
            evaluate<Scheme>(system[j], box_set_certified, last_eval);
        } else {
            evaluate<Scheme>(system[j], box_set, last_eval);
        }
        if(verbose >= 2) {
            std::cout << "First jet of polynomial " << j << ":\n";
            if(last_eval.size() > 0) {
                for(index_t k = 0; k < Scheme<I>::dimension(system[j].get_number_axes()); ++k) {
                    std::cout << last_eval[k] << '\n';
                }
            }
        }
        nonzeroes.resize(box_set.get_number_elements());
        if(ind_equalities.size() >= 1 and Scheme<I>::has_gradient) {
            is_non_zero_modulo<Scheme>(evals, j, ind_equalities, box_set, nonzeroes.begin());
        } else {
            is_non_zero<Scheme>(last_eval.cbegin(), box_set, nonzeroes.begin());
        }
        if(signs[j] == 0) {
            ind_equalities.push_back(j);
        } else {
            table.resize(box_set.get_number_elements());
            for(index_t k = 0; k < nonzeroes.size(); ++k) {
                if(nonzeroes[k] == 0) {
                    match_signs[k] = 0;
                } else if(nonzeroes[k] == -signs[j]) {
                    nonzeroes[k] = 1;
                } else {
                    nonzeroes[k] = 0;
                }
            }
        }
        remove(box_set, nonzeroes);
        remove(match_signs, nonzeroes);
        if(guarantee and Scheme<I>::has_gradient) {
            remove(box_set_certified, nonzeroes);
        }
        if(Scheme<I>::has_gradient and
                (guarantee or clean_eval or (ind_equalities.size() > 0 and j < system.size()-1))) {
            for(index_t k = 0; k < ind_equalities.size(); ++k) {
                remove(evals[ind_equalities[k]], nonzeroes, dim);
            }
        }
    }
    // Certified boxes for case of inequalities only
    if(!has_signs_zero) {
        box_set_certified = box_set;
        remove(box_set, match_signs);
        for(index_t i=0; i<match_signs.size(); ++i) {match_signs[i] = !match_signs[i];}
        remove(box_set_certified, match_signs);
    }
    // Certified boxes for the case of 1 equality
    if(guarantee and num_equalities == 1 and has_signs_zero and Scheme<I>::has_gradient) {
        zeroes.resize(box_set_certified.get_number_elements());
        is_zero<Scheme>(evals[ind_equalities[0]].cbegin(), box_set_certified, zeroes.begin());
        for(index_t i = 0; i < zeroes.size(); ++i) {zeroes[i] = zeroes[i] and match_signs[i];}
        remove(box_set, zeroes);
        stable_partition(evals[0], zeroes, dim);
        for(index_t i = 0; i < zeroes.size(); ++i) {zeroes[i] = !zeroes[i];}
        remove(box_set_certified, zeroes);
    }
}

static void check_input(bool normal, double inflation, index_t refine, index_t max_depth, const std::string & scheme,
              const std::vector<double> & lower, const std::vector<double> & upper,
              index_t center, std::vector<index_t> & grid_sizes,
              std::vector<index_t> & axes, index_t verbose,
              const std::vector<std::string> & infile, std::vector<char> & signs,
              const std::string & outfile, const index_t & num_vars)
{
    if(inflation < 1) {
        fprintf(stderr, "Inflation given with -i option should be greater or equal to 1.\n");
        exit(EXIT_FAILURE);
    }
    index_t max_g = 1;
    if(grid_sizes.size() > 0) {
        max_g = *std::max_element(grid_sizes.begin(), grid_sizes.end());
    }
    uint64_t one = 1;
    if(max_depth <= 0 or max_g*(one << max_depth) > (one<<IDX_SIZE) or max_g*(one<<refine) > (one<<IDX_SIZE)) {
        fprintf(stderr, "Two power depth given with option -d times maximum grid size\n");
        fprintf(stderr, "should be more than 0 and less than 2^%d. Recompile with -DEXTEND\n", IDX_SIZE);
        fprintf(stderr, "to extend the bound to 2^64.\n");
        exit(EXIT_FAILURE);
    }
    if(static_cast<long>(center) < 0) {
        fprintf(stderr, "Maximum center depth given with -c option should be non-negative.\n");
        exit(EXIT_FAILURE);
    }
    for(index_t i = 0; i < grid_sizes.size(); ++i) {
        if(static_cast<long>(grid_sizes[i]) < 0) {
            fprintf(stderr, "Grid sizes given with -g option should be greater or equal to 0.\n");
            exit(EXIT_FAILURE);
        };
    }
    for(index_t i = 0; i < axes.size(); ++i) {
        if(static_cast<long>(axes[i]) < 0 or axes[i] >= num_vars) {
            fprintf(stderr, "Axes given with option -a should be between 0 and %lu included.\n", num_vars-1);
            exit(EXIT_FAILURE);
        };
        for(index_t j = 0; j < i; ++j) {
            if(axes[i] == axes[j]) {
                fprintf(stderr, "Axes given with option -a should be different.\n");
                exit(EXIT_FAILURE);
            };
        }   
    }
    for(index_t i = 0; i < signs.size(); ++i) {
        if(signs[i] > 1 or signs[i] < -1) {
            fprintf(stderr, "Signs given with -s option should be -1, 0 or 1.\n");
            exit(EXIT_FAILURE);
        }
    }
    if(refine < max_depth) {
        fprintf(stderr, "Warning: refine bound given with -r option should be greater or equal to\n");
        fprintf(stderr, "         maximum depth given with option -d. Set to %zu.\n", max_depth);
    }
    if(verbose > 2) {
        fprintf(stderr, "Warning: verbose option should be 0, 1 or 2. Set to 2.\n");
    }
}

template<typename I>
void voxelize(bool normal, bool mesh, double inflation, index_t refine, index_t max_depth, std::string & scheme,
              const std::vector<double> & lower, const std::vector<double> & upper,
              index_t center, std::vector<index_t> & grid_sizes,
              std::vector<index_t> & axes, index_t verbose, bool trim,
              const std::vector<std::string> & infile, std::vector<char> & signs,
              const std::string & outfile)
{
    std::fesetround(FE_UPWARD);
    using f = typename I::base_type;
    std::ifstream input;
    std::ifstream output(outfile);
    std::vector<Tensor_poly<Iprecise>> poly(infile.size());
    if(refine < max_depth) {
        refine = max_depth;
    }

    index_t num_vars = 0;
    for(index_t i = 0; i < infile.size(); ++i) {
        input = std::ifstream(infile[i]);
        if(!input.good()) {
            fprintf(stderr, "Input file \"%s\" does not exist.", infile[i].c_str());
            if(infile[i].find('.') == std::string::npos) {
                fprintf(stderr, " Note that no space is allowed\n");
                fprintf(stderr, "in comma separated lists for options -s, -l, -u, -g and -a.");
            }
            fprintf(stderr, "\n");
            exit(EXIT_FAILURE);
        };
        input >> poly[i];
        if(poly[i].get_number_axes() > num_vars) {
            num_vars = poly[i].get_number_axes();
            if(i > 0) {
                fprintf(stderr, "The number of variables in the input files doesn't match.\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    check_input(normal, inflation, refine, max_depth, scheme, lower, upper, center, grid_sizes, axes, verbose, infile, signs, outfile, num_vars);
    if(verbose >= 1) {
        for(index_t i = 0; i < poly.size(); ++i) {
            std::cout << "Polynomial from " << infile[i] << ":\n";
            if(poly[i].get_number_elements() < 100) {
                std::cout << poly[i] << '\n';
            } else {
                print_polynomial(std::cout, poly[i], 100);
                std::cout << '\n';
            }
        }
    }
    // normalize polynomials
    for(index_t i = 0; i < poly.size(); ++i) {
        Iprecise::base_type m = 1;
        for(index_t j = 0; j < poly[i].data.size(); ++j) {
            Iprecise::base_type lm = get_max_abs(poly[i].data[j]);
            if(lm > m) {
                m = lm;
            }
        }
        for(index_t j = 0; j < poly[i].data.size(); ++j) {
            mul(Iprecise(1/m), poly[i].data[j], poly[i].data[j]);
        }
    }
    
    char last_s = (signs.size() >= 1)? signs.back() : 0;
    signs.resize(poly.size(), last_s);
    index_t first_equ = std::find(signs.begin(), signs.end(), 0) - signs.begin();
    bool has_signs_zero = (first_equ != signs.size());

    f l = l_default;
    f u = u_default;
    if(lower.size() >= 1) {
        l = lower.back();
    }
    if(upper.size() >= 1) {
        u = upper.back();
    }
    std::vector<I> b(num_vars, {l, u});
    for(index_t i = 0; i < lower.size(); ++i) {
        b[i].neg_lower = -lower[i];
    }
    for(index_t i = 0; i < upper.size(); ++i) {
        b[i].upper = upper[i];
    }
    index_t g = (grid_sizes.size() >= 1)? grid_sizes.back() : 1;
    grid_sizes.resize(num_vars, g);
    for(index_t i = 0; i < grid_sizes.size(); ++i) {
        if(get_lower(b[i]) == get_upper(b[i])) {
            grid_sizes[i] = 0;
        }
    }
    std::deque<std::vector<Tensor_poly<I>>> systems;
    std::deque<Tensor_box<I>> box_sets;
    std::vector<std::vector<Tensor_box<I>>> box_sets_certified(refine);
    std::vector<std::vector<std::vector<std::array<ply_float,6>>>> centers_normals(refine+1);
    std::vector<std::vector<Iprecise>> centers;

    // set axes
    index_t na;
    if(axes.size() == 0) {
        na = num_vars > 3 ? 3 : num_vars;
        axes.resize(na);
        for(index_t i=0; i<na; ++i) { axes[i] = i; }
    } else {
        na = axes.size();
    }

    auto t0 = std::chrono::high_resolution_clock::now();
    auto t1 = std::chrono::high_resolution_clock::now();
    t0 = std::chrono::high_resolution_clock::now();
    std::vector<Tensor> derivations(poly.size());
    std::vector<Tensor_box<Iprecise>> generic_boxes(poly.size());
    std::vector<Tensor_poly<I>> generic_system(poly.size());
    if(center > 0 ) {
        for(index_t i = 0; i < poly.size(); ++i) {
            transpose(poly[i], derivations[i]);
            generic_boxes[i] = Tensor_box<Iprecise>(derivations[i]);
            generic_boxes[i].data.resize(num_vars, {0});
            generic_system[i] = Tensor_poly<I>(derivations[i]);
            generic_system[i].data.resize(generic_system[i].idx.back().size(), 0);
        }
        systems.resize(1);
        systems[0] = generic_system;
        box_sets.resize(1);
        box_sets[0] = make_tensor_box(b);
        box_sets_certified[0].resize(1);
        centers.resize(1);
        centers[0] = std::vector<Iprecise>(num_vars, 0);
    } else {
        // to make order of evaluation match polynomial evaluation order
        std::reverse(b.begin(), b.end());
        std::reverse(grid_sizes.begin(), grid_sizes.end());
        for(index_t i=0; i<na; ++i) { axes[i] = num_vars-1-axes[i]; }
        box_sets.resize(1);
        box_sets[0] = make_tensor_box(b);
        systems.resize(1);
        systems[0].resize(poly.size());
        for(index_t i = 0; i < poly.size(); ++i) {
            move(poly[i], systems[0][i]);
        }
    }
    std::vector<index_t> directions = grid_sizes;
    for(index_t i = 0; i < directions.size(); ++i) {
        if(grid_sizes[i] > 0) {
            directions[i] = 2;
        }
    }
    split(box_sets[0], grid_sizes);

    // TODO: split box with respect to grid_sizes
    for(index_t i = 0; i < refine; ++i) {
        if(verbose >= 1) {
            std::cout << "depth: " << i << '\n';
        }
        bool lazy = (i >= max_depth-1) and (max_depth < refine);
        if(i >= max_depth) {
            scheme = "Quadratic";
        }
        std::vector<f> condition_numbers;

        if(center > i) {
            // recenter the systems at each box
            index_t Nb = box_sets.size();
            std::vector<index_t> ind(Nb+1);
            ind[0] = Nb;
            for(index_t j = 0; j < Nb; ++j) {
                ind[j+1] = ind[j] + box_sets[j].get_number_elements();
            }
            box_sets.resize(ind.back());
            centers.resize(ind.back(), std::vector<Iprecise>(num_vars,0));
            if(verbose >= 1) {
                std::cout << "Centering " << (ind.back() - Nb)*poly.size()
                          << " polynomial" << ((ind.back()-Nb)*poly.size()>1?"s.\n":".\n");
            }
            for(index_t j = 0; j < Nb; ++j) {
                std::vector<I> lb(num_vars, 0);
                std::vector<I> cb(num_vars, 0);
                std::vector<I> db(num_vars, 0);
                std::vector<index_t> it_box = box_sets[j].begin();  
                std::vector<small_index_t> idx(num_vars);  
                for(index_t k = 0; k < box_sets[j].get_number_elements(); ++k) {
                    get_box(box_sets[j], it_box, lb);
                    box_sets[j].get_idx(it_box, idx);
                    for(index_t m = 0; m < lb.size(); ++m) {
                        //mid(lb[m], cb[m]);
                        //delta(lb[m], db[m]);
                        cb[m] = I(get_near_zero(lb[m]));
                        shift_to_zero(lb[m], db[m]);
                        add(centers[j][m], Iprecise(cb[m]), centers[ind[j] + k][m]);
                    }
                    box_sets[ind[j] + k] = make_tensor_box(db, idx);
                    box_sets[j].advance(it_box);
                }
            }
            box_sets.erase(box_sets.begin(), box_sets.begin() + Nb);
            centers.erase(centers.begin(), centers.begin() + Nb);
            systems.resize(ind.back()-Nb, generic_system);

            #pragma omp parallel for shared(systems, centers, generic_boxes, ind, poly)
            for(index_t k = 0; k < centers.size(); ++k) {
                std::vector<Tensor_box<Iprecise>> local_generic_boxes = generic_boxes;
                std::vector<Iprecise> new_coeffs;
                for(index_t p = 0; p < poly.size(); ++p) {
                    for(index_t m = 0; m < num_vars; ++m) {
                        local_generic_boxes[p].data[m][0] = centers[k][num_vars-1-m];
                    }
                    evaluate<Shift>(poly[p], local_generic_boxes[p], new_coeffs);
                    for(index_t m = 0; m < new_coeffs.size(); ++m) {
                        systems[k][p].data[m] = I(new_coeffs[m]);
                    }
                }
            }
            //systems.erase(systems.begin(), systems.begin() + Nb);

            if(verbose >= 2) {
                for(index_t j = 0; j < box_sets.size(); ++j) {
                    std::cout << "Centered at ";
                    for(index_t k = 0; k < num_vars; ++k) {
                        std::cout << centers[j][k];
                    }
                    std::cout << ":\n";
                    for(index_t k = 0; k < systems[j].size(); ++k) {
                        std::cout << "Polynomial " << k << ":\n";
                        if(systems[j][k].get_number_elements() < 100) {
                            std::cout << systems[j][k] << '\n';
                        } else {
                            print_polynomial(std::cout, systems[j][k], 100);
                            std::cout << '\n';
                        }
                    }
                }
            }
        }
        

        box_sets_certified[i].resize(box_sets.size());
        centers_normals[i].resize(box_sets.size());
        bool last = (i == refine - 1) and (normal or mesh);
        bool recenter = (center > 0) and (i >= center-1) and has_signs_zero and (i < refine - 1);
        //bool recenter = false;
        bool clean_eval = last or recenter;
        if(i == refine - 1) {
            centers_normals[refine].resize(box_sets.size());
        }
        if(verbose >= 1) {
            index_t Nb = 0;
            for(index_t j = 0; j < box_sets.size(); ++j) {Nb += box_sets[j].get_number_elements();}
            std::cout << "Evaluating " << (Nb<<num_vars) << " box" << (Nb>0?"es.\n":".\n");
        }
        for(index_t j = 0; j < box_sets.size(); ++j) {
            if(verbose >= 2) {
                std::cout << "Cell " << j << " contains " << (box_sets[j].get_number_elements()<<num_vars) << " boxes.\n";
            }
            std::vector<std::vector<I>> evals(systems[j].size());
            
            split(box_sets[j], directions);

            if(scheme.compare("Linear") == 0) {
                voxelize_loop<Linear>(systems[j], signs, box_sets[j], box_sets_certified[i][j], evals, lazy, clean_eval, verbose, inflation);
                if(normal or mesh) {
                    index_t N = box_sets_certified[i][j].get_number_elements();
                    if(N > 0) {
                        get_centers_normals<Linear>(box_sets_certified[i][j], evals, 0, N, signs, axes, centers_normals[i][j]);
                    }
                    if(i == refine-1) {
                        index_t M = box_sets[j].get_number_elements();
                        if(M > 0) {
                            get_centers_normals<Linear>(box_sets[j], evals, N, N+M, signs, axes, centers_normals[refine][j]);
                        }
                    }
                }
                if(recenter) {
                    get_condition_numbers<Linear>(evals[first_equ].begin(), box_sets[j], condition_numbers);
                }
            } else if(scheme.compare("Quadratic") == 0) {
                voxelize_loop<Quadratic>(systems[j], signs, box_sets[j], box_sets_certified[i][j], evals, lazy, clean_eval, verbose, inflation);
                if(normal or mesh) {
                    index_t N = box_sets_certified[i][j].get_number_elements();
                    if(N > 0) {
                        get_centers_normals<Quadratic>(box_sets_certified[i][j], evals, 0, N, signs, axes, centers_normals[i][j]);
                    }
                    if(i == refine-1) {
                        index_t M = box_sets[j].get_number_elements();
                        if(M > 0) {
                            get_centers_normals<Quadratic>(box_sets[j], evals, N, N+M, signs, axes, centers_normals[refine][j]);
                        }
                    }
                }
                if(recenter) {
                    get_condition_numbers<Quadratic>(evals[first_equ].begin(), box_sets[j], condition_numbers);
                }
            } else {
                voxelize_loop<Horner>(systems[j], signs, box_sets[j], box_sets_certified[i][j], evals, lazy, clean_eval, verbose, inflation);
                if(i == refine-1) {
                    index_t M = box_sets[j].get_number_elements();
                    centers_normals[refine][j].resize(M, {0,0,0,0,0,0});
                }
                if(recenter) {
                    get_condition_numbers<Horner>(evals[first_equ].begin(), box_sets[j], condition_numbers);
                }
            }
            if(center > 0) {
                // the center is not reversed to match the transposed polynomial
                std::vector<I> c(num_vars, 0);
                for(index_t k = 0; k < num_vars; ++k) {
                    c[k] = I(centers[j][k]);
                }
                shift(box_sets_certified[i][j], c);
                if(recenter) {
                    // recenter the systems at each box
                    bool unstable = false;
                    index_t unstable_ind = 0;
                    index_t unstable_num = 0;
                    index_t unstable_den = 1;
                    for(index_t k = 0; k < box_sets[j].get_number_elements(); ++k) {
                        if(condition_numbers[2*k+1] == 0) {
                            unstable = true;
                            if((unstable_den > 0) or (unstable_den == 0 and unstable_num < condition_numbers[2*k])) {
                                unstable_num = condition_numbers[2*k+1];
                                unstable_ind = k;
                            }
                        } else if (unstable_num * condition_numbers[2*k+1] < condition_numbers[2*k] * unstable_den) {
                                unstable_num = condition_numbers[2*k];
                                unstable_den = condition_numbers[2*k+1];
                                unstable_ind = k;
                        }
                    }
                    if(unstable) {
                        std::vector<I> lb(num_vars, 0);
                        std::vector<I> cb(num_vars, 0);
                        std::vector<I> db(num_vars, 0);
                        std::vector<index_t> it_box = box_sets[j].begin();  
                        for(index_t k = 0; k < unstable_ind; ++k) {
                            box_sets[j].advance(it_box);
                        }
                        get_box(box_sets[j], it_box, lb);
                        for(index_t m = 0; m < lb.size(); ++m) {
                            mid(lb[m], cb[m]);
                            add(centers[j][m], Iprecise(cb[m]), centers[j][m]);
                            neg(cb[m], cb[m]);
                        }
                        shift(box_sets[j], cb);
                        if(verbose >= 1) {
                            std::cout << "Recentering polynomials in cell " << j << "\n";
                        }
                        //#pragma omp parallel for shared(systems, centers, generic_boxes, ind, poly)
                        std::vector<Tensor_box<Iprecise>> local_generic_boxes = generic_boxes;
                        std::vector<Iprecise> new_coeffs;
                        for(index_t p = 0; p < poly.size(); ++p) {
                            for(index_t m = 0; m < num_vars; ++m) {
                                local_generic_boxes[p].data[m][0] = centers[j][num_vars-1-m];
                            }
                            evaluate<Shift>(poly[p], local_generic_boxes[p], new_coeffs);
                            for(index_t m = 0; m < new_coeffs.size(); ++m) {
                                systems[j][p].data[m] = I(new_coeffs[m]);
                            }
                        }
                        //systems.erase(systems.begin(), systems.begin() + Nb);

                        if(verbose >= 2) {
                            std::cout << "Centered at ";
                            for(index_t k = 0; k < num_vars; ++k) {
                                std::cout << centers[j][k];
                            }
                            std::cout << ":\n";
                            for(index_t k = 0; k < systems[j].size(); ++k) {
                                std::cout << "Polynomial " << k << ":\n";
                                if(systems[j][k].get_number_elements() < 100) {
                                    std::cout << systems[j][k] << '\n';
                                } else {
                                    print_polynomial(std::cout, systems[j][k], 100);
                                    std::cout << '\n';
                                }
                            }
                        }
                    }
                }
            }
            if(verbose >= 2 and (normal or mesh)) {
                if(lazy) {
                    if(centers_normals[i][j].size() > 0) {
                        std::cout << "First normal of certified boxes of cell " << j << ":\n";
                        std::cout << centers_normals[i][j][0][4] << ' ';
                        std::cout << centers_normals[i][j][0][5] << ' ';
                        std::cout << centers_normals[i][j][0][6] << '\n';
                    }
                }
                if(last) {
                    if(centers_normals[refine][j].size() > 0) {
                        std::cout << "First normal of non certified boxes cell " << j << ":\n";
                        std::cout << centers_normals[refine][j][0][4] << ' ';
                        std::cout << centers_normals[refine][j][0][5] << ' ';
                        std::cout << centers_normals[refine][j][0][6] << '\n';
                    }
                }
            }
        }
    }
    if(center > 0) {
        if(verbose >= 1) {
            std::cout << "Shifting boxes. \n";
        }
        std::vector<I> c(num_vars, 0);
        for(index_t j = 0; j < box_sets.size(); ++j) {
            for(index_t k = 0; k < num_vars; ++k) {
                c[k] = I(centers[j][k]);
            }
            shift(box_sets[j], c);
        }
    }
    t1 = std::chrono::high_resolution_clock::now();
    if(verbose >= 1) {
        std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << ".\n";
    }
    if(verbose >= 1) {
        std::cout << "Post treatment of data. \n";
    }
    t0 = std::chrono::high_resolution_clock::now();

    index_t Nb = 0;
    for(index_t i = 0; i < refine; ++i) {
        for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {Nb += box_sets_certified[i][j].get_number_elements();}
    }
    for(index_t j = 0; j < box_sets.size(); ++j) {
        Nb += box_sets[j].get_number_elements();
    }
    if(normal or mesh) {
        std::vector<std::array<ply_float,6>> points;
        points.reserve(Nb);
        index_t skipped = 0;
        std::vector<std::vector<index_t>> box_sizes(0);
        index_t Nlast = 0;
        for(index_t i = 0; i < refine; ++i) {
            box_sizes.push_back(std::vector<index_t>(1,Nlast));
            for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {
                index_t s = append_points_normals(box_sets_certified[i][j], centers_normals[i][j], axes, points, trim);
                if(verbose >= 2 and s > 0) {
                    std::cout << s << " ill-conditioned points in cell " << j << " of level " << i << ".\n";
                }
                skipped += s;
                index_t N = box_sizes[i].back() + box_sets_certified[i][j].get_number_elements();
                box_sizes[i].push_back(N);
            }
            Nlast = box_sizes[i].back();
        }
        box_sizes.push_back(std::vector<index_t>(1,Nlast));
        for(index_t k = 0; k < box_sets.size(); ++k) {
            index_t s = append_points_normals(box_sets[k], centers_normals[refine][k], axes, points, trim);
            if(verbose >= 2 and s > 0) {
                std::cout << s << " ill-conditioned points in cell " << k << ".\n";
            }
            skipped += s;
            index_t N = box_sizes[refine].back() + box_sets[k].get_number_elements();
            box_sizes[refine].push_back(N);
        }
        if(verbose >= 1 and skipped > 0) {
            std::cout << skipped << " ill-conditioned points removed.\n";
        }
        if(mesh) {
            // number of neighbours of a hyercube at distance 2 in norm 1
            index_t N2 = 2*num_vars + 2*num_vars*(num_vars-1);
            // a block is sorted by size, tie in lexicographical order :
            // For dim 3 :
            //  case -1 and 1 : [-e1, -e2, -e3, e1, e2, e3,
            //  case -1,-1 and -1, 1: [-e1-e2, -e1-e3, -e2-e3, -e1+e2, -e1+e3, -e2+e3]
            //  case 1,-1 and 1, 1: [e1-e2, e1-e3, e2-e3, e1+e2, e1+e3, e2+e3]
            std::vector<int32_t> neighbours(N2*box_sizes.back().back(), -1);
            // mesh certified boxes
            for(index_t i = 0; i < refine; ++i) {
                for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {
                    for(index_t m = 0; m <= i; ++m) {
                        for(index_t n = 0; n < box_sets_certified[m].size(); ++n) {
                            append_neighbours(box_sets_certified[i][j], box_sizes[i][j], i,
                                              box_sets_certified[m][n], box_sizes[m][n], m,
                                              neighbours);
                        }
                    }
                }
            }
            // mesh remaining boxes
            for(index_t j = 0; j < box_sets.size(); ++j) {
                for(index_t n = 0; n < box_sets.size(); ++n) {
                    append_neighbours(box_sets[j], box_sizes[refine][j], refine - 1,
                                      box_sets[n], box_sizes[refine][n], refine - 1,
                                      neighbours);
                }
                for(index_t n = 0; n < box_sets_certified[refine-1].size(); ++n) {
                    append_neighbours(box_sets_certified[refine-1][n], box_sizes[refine-1][n], refine-1,
                                      box_sets[j], box_sizes[refine][j], refine - 1,
                                      neighbours);
                }
                for(index_t m = 0; m < refine; ++m) {
                    for(index_t n = 0; n < box_sets_certified[m].size(); ++n) {
                        append_neighbours(box_sets[j], box_sizes[refine][j], refine - 1,
                                          box_sets_certified[m][n], box_sizes[m][n], m,
                                          neighbours);
                    }
                }
            }
            std::vector<struct ply_triangle> triangles(0);
            append_triangles(points, neighbours, num_vars, triangles);
            t1 = std::chrono::high_resolution_clock::now();
            if(verbose >= 1) {
                std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << ".\n";
            }
            if(verbose >= 1) {
                std::cout << "Writing file. \n";
            }
            t0 = std::chrono::high_resolution_clock::now();
            std::ofstream file(outfile, std::ios::out | std::ios::binary);
            if(normal) {
                print_ply_points_normals_triangles(points, triangles, file);
            } else {
                print_ply_points_triangles(points, triangles, file);
            }
        } else {
            t1 = std::chrono::high_resolution_clock::now();
            if(verbose >= 1) {
                std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << ".\n";
            }
            if(verbose >= 1) {
                std::cout << "Writing file. \n";
            }
            t0 = std::chrono::high_resolution_clock::now();
            std::ofstream file(outfile, std::ios::out | std::ios::binary);
            print_ply_points_normals(points, file);
        }
    } else {
        std::vector<std::array<ply_float,3>> points;
        points.reserve(8*Nb);
        for(index_t i = 0; i < refine; ++i) {
            for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {
                append_cubes(box_sets_certified[i][j], axes, points);
            }
        }
        for(index_t k = 0; k < box_sets.size(); ++k) {
            append_cubes(box_sets[k], axes, points);
        }
        t1 = std::chrono::high_resolution_clock::now();
        if(verbose >= 1) {
            std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << ".\n";
        }
        if(verbose >= 1) {
            std::cout << "Writing file. \n";
        }
        t0 = std::chrono::high_resolution_clock::now();
        std::ofstream file(outfile, std::ios::out | std::ios::binary);
        print_ply_cubes(points, axes.size(), file);

        //std::ofstream zeroes(outfile);
        //print_normal_obj(zeroes);
        //index_t m = 0;
        //for(index_t i = 0; i < refine; ++i) {
        //    for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {
        //        m = append_obj(zeroes, box_sets_certified[i][j], axes, m);
        //    }
        //}
        //for(index_t k = 0; k < box_sets.size(); ++k) {
        //    m = append_obj(zeroes, box_sets[k], axes, m);
        //}
    }
    t1 = std::chrono::high_resolution_clock::now();
    if(verbose >= 1) {
        std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << '\n';
    }
}

int main(int argc, char* argv[]) {

    index_t refine = 0;
    index_t max_depth = 7;
    long precision = 64;
    std::string scheme = "Quadratic";
    std::string outfile = "";
    std::vector<double> lower(0);
    std::vector<double> upper(0);
    index_t verbose = 1;
    std::stringstream number_stream;
    std::vector<std::string> infile;
    std::string basename = "";
    index_t center = 0;
    std::vector<index_t> grid_sizes(0);
    std::vector<index_t> axes(0);
    std::vector<char> signs(0);
    double inflation = 1;
    bool normal = false;
    bool trim = false;
    bool mesh = false;

    char tmp;
    double d;
    int i;
    
    int opt;
    while ((opt = getopt(argc, argv, "d:p:s:l:u:o:v:b:c:g:a:r:i:ntmh012")) != -1) {
        switch(opt) {
        case '0':
            scheme = "Horner";
            break;
        case '1':
            scheme = "Linear";
            break;
        case '2':
            scheme = "Quadratic";
            break;
        //case '3':
        //    scheme = "Quadratic_shift";
        //    break;
        case 'd':
            max_depth = std::stol(optarg);
            break;
        case 'p':
            precision = std::stol(optarg);
            break;
        case 's':
            number_stream = std::stringstream(optarg); 
            while(number_stream >> i) {
                signs.push_back(static_cast<char>(i));
                number_stream >> tmp;
            }
            break;
        case 'o':
            outfile = optarg;
            break;
        case 'l':
            number_stream = std::stringstream(optarg); 
            while(number_stream >> d) {
                lower.push_back(d);
                number_stream >> tmp;
            }
            break;
        case 'u':
            number_stream = std::stringstream(optarg); 
            while(number_stream >> d) {
                upper.push_back(d);
                number_stream >> tmp;
            }
            break;
        case 'v':
            verbose = std::stol(optarg);
            break;
        case 'b':
            basename = optarg;
            break;
        case 'c':
            center = std::stol(optarg);
            break;
        case 'g':
            number_stream = std::stringstream(optarg); 
            grid_sizes.resize(0);
            while(number_stream >> i) {
                grid_sizes.push_back(i);
                if((number_stream >> tmp) and tmp != ',') {
                    fprintf(stderr, "Warning: grid sizes given with option -g should be integer.\n");
                    fprintf(stderr, "         Character '%c' is treated as a separator.\n", tmp);
                }
            }
            break;
        case 'a':
            number_stream = std::stringstream(optarg); 
            while(number_stream >> i) {
                // shift axes index by 1 for better user interface with
                // non computer scientists
                axes.push_back(i-1);
                number_stream >> tmp;
            }
            break;
        case 'r':
            refine = std::stol(optarg);
            break;
        case 'i':
            inflation = std::stod(optarg);
            break;
        case 'n':
            normal = true;
            break;
        case 't':
            trim = true;
            break;
        case 'm':
            mesh = true;
            break;
        case 'h':

        default:
            fprintf(stderr, "Usage: %s [-d depth] [-s signs] [-p precision] [-l lower corner]\n",
                    argv[0]);
            fprintf(stderr, "       %*s [-u upper corner] [-c depth] [-g grid size] [-a axes]\n",
                    static_cast<int>(std::strlen(argv[0])), "");
            fprintf(stderr, "       %*s [-o output file] [-b basename] [-n] [-m] [-t] [-0] [-1] [-2] [-3]\n",
                    static_cast<int>(std::strlen(argv[0])), "");
            fprintf(stderr, "       %*s [-r depth] [-i scale] [-v level] filename1 filename2 ...\n",
                    static_cast<int>(std::strlen(argv[0])), "");
            fprintf(stderr, "\n");
            fprintf(stderr, "Given a list of polynomials and a list of signs, this command outputs a\n");
            fprintf(stderr, "file of format ply encoding the solutions of the corresponding system.\n");
            fprintf(stderr, "\n");
            fprintf(stderr, "   -s signs:        comma separated list of values -1, 0 or 1 representing\n");
            fprintf(stderr, "                    the sign (-1, 0 or 1) for each polynomial file; if the\n");
            fprintf(stderr, "                    number of signs is less than the number of files, the\n");
            fprintf(stderr, "                    last sign is associated to the remaining polynomials\n");
            fprintf(stderr, "                    (default: 0)\n");
            fprintf(stderr, "   -p precision:    the precision of the intermediate computations\n");
            fprintf(stderr, "                    either 32, 64, 80 or 128 (if compiled with -DFLOAT128)\n");
            fprintf(stderr, "                    (default: 64)\n");
            fprintf(stderr, "   -i scale:        inflation as a double; the scale at which boxes are \n");
            fprintf(stderr, "                    inflated when checking for zeroes\n");
            fprintf(stderr, "                    (default: 1.0)\n");
            fprintf(stderr, "   -l lower corner: the coordinates of the lower corner of the view box\n");
            fprintf(stderr, "                    as a comma separated list of floating points; if the\n");
            fprintf(stderr, "                    number of values is less than the number of variables,\n");
            fprintf(stderr, "                    the last value is used for the remaining coordinates\n");
            fprintf(stderr, "                    (default: -2)\n");
            fprintf(stderr, "   -u upper corner: the coordinates of the upper corner of the view box\n");
            fprintf(stderr, "                    as a comma separated list of floating points; if the\n");
            fprintf(stderr, "                    number of values is less than the number of variables,\n");
            fprintf(stderr, "                    the last value is used for the remaining coordinates\n");
            fprintf(stderr, "                    (default: 2)\n");
            fprintf(stderr, "   -g grid size:    comma separated list of integers; the initial view box\n");
            fprintf(stderr, "                    is uniformly divided in each direction according to\n");
            fprintf(stderr, "                    the corresponding number in grid size; if the number\n");
            fprintf(stderr, "                    of values is less than the number of variables, the\n");
            fprintf(stderr, "                    last value is used for the remaining coordinates\n");
            fprintf(stderr, "                    (default: 1)\n");
            fprintf(stderr, "   -a axes:         comma separated list of integers; the indices of the\n");
            fprintf(stderr, "                    variables to visualize; if the number of values is\n");
            fprintf(stderr, "                    greater than the number of variables, the first three\n");
            fprintf(stderr, "                    indices of variables are kept\n");
            fprintf(stderr, "                    (default: 1,2,3)\n");
            fprintf(stderr, "   -d depth:        integer for the depth of the subdivision tree; until\n");
            fprintf(stderr, "                    the value d, all the boxes are subdivided\n");
            fprintf(stderr, "                    (default: 7)\n");
            fprintf(stderr, "   -c depth:        integer for a depth in the subdivision tree; until the\n");
            fprintf(stderr, "                    value depth, on each box the polynomials are evaluated\n");
            fprintf(stderr, "                    after being recentered\n");
            fprintf(stderr, "                    (default: 0)\n");
            fprintf(stderr, "   -r depth:        integer for a depth in the subdivision tree; the boxes\n");
            fprintf(stderr, "                    at a depth greater or equal to d and less than r are\n");
            fprintf(stderr, "                    subdivided only if they are not guaranteed to contain\n");
            fprintf(stderr, "                    a solution of the input system\n");
            fprintf(stderr, "                    (default: 0)\n");
            fprintf(stderr, "   -n               specifies that the output file will contain a points\n");
            fprintf(stderr, "                    cloud with normals instead of a set of cubes\n");
            fprintf(stderr, "                    (default: false)\n");
            fprintf(stderr, "   -m               specifies that the output file will contain a mesh\n");
            fprintf(stderr, "                    with normals instead of a set of cubes\n");
            fprintf(stderr, "                    (default: false)\n");
            fprintf(stderr, "   -t               when the option -n is active, the option -t specifies\n");
            fprintf(stderr, "                    that centers of boxes not contracting are removed\n");
            fprintf(stderr, "                    (default: false)\n");
            fprintf(stderr, "   -0\n");
            fprintf(stderr, "   -1\n");
            fprintf(stderr, "   -2:              selects the order of the expansion for the evaluation:\n");
            fprintf(stderr, "                    0 is direct, 1 is linear and 2 is quadratic\n");
            fprintf(stderr, "                    (default: 2)\n");
            fprintf(stderr, "   -v verbose:      integer 0, 1 or 2 for the level of verbose\n");
            fprintf(stderr, "                    (default: 1)\n");

            exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc) {
        fprintf(stderr, "Expected input file of type poly.\n");
        exit(EXIT_FAILURE);
    }
    
    infile.resize(argc - optind);
    for(int i = 0; i < argc - optind; ++i) {
        infile[i] = argv[optind + i];
    }
    if(outfile.compare("") == 0) {
        if(basename.compare("") != 0) {
            outfile = basename;
        } else {
            outfile = infile[0].substr(0, infile[0].find_last_of("."));
        }
        outfile += "_d";
        outfile += std::to_string(max_depth);
        outfile += "_";
        outfile += scheme;
        outfile += "_p";
        outfile += std::to_string(precision);
        if(signs.size() > 0) {
            outfile += "_s";
            for(index_t i = 0; i < signs.size(); ++i) {
                outfile += "_";
                outfile += std::to_string(static_cast<int>(signs[i]));
            }
        }
        if(axes.size() > 0) {
            outfile += "_a";
            for(index_t i = 0; i < axes.size(); ++i) {
                outfile += "_";
                outfile += std::to_string(axes[i]);
            }
        }
        outfile += "_l";
        if(lower.size() == 0) {
            outfile += "_";
            outfile += std::to_string(l_default);
        }
        for(index_t i = 0; i < lower.size(); ++i) {
            outfile += "_";
            outfile += std::to_string(lower[i]);
        }
        outfile += "_u";
        if(upper.size() == 0) {
            outfile += "_";
            outfile += std::to_string(u_default);
        }
        for(index_t i = 0; i < upper.size(); ++i) {
            outfile += "_";
            outfile += std::to_string(upper[i]);
        }
        if(center) {
            outfile += "_c";
        }
        outfile += ".ply";
    }

    switch (precision) {
        case 32:
            voxelize<Interval32> (normal, mesh, inflation, refine, max_depth, scheme, lower, upper, center, grid_sizes, axes, verbose, trim, infile, signs, outfile);
            break;
        case 64:
            voxelize<Interval64> (normal, mesh, inflation, refine, max_depth, scheme, lower, upper, center, grid_sizes, axes, verbose, trim, infile, signs, outfile);
            break;
        case 80:
            voxelize<Interval80> (normal, mesh, inflation, refine, max_depth, scheme, lower, upper, center, grid_sizes, axes, verbose, trim, infile, signs, outfile);
            break;
#ifdef FLOAT128
        case 128:
            voxelize<Interval128>(normal, mesh, inflation, refine, max_depth, scheme, lower, upper, center, grid_sizes, axes, verbose, trim, infile, signs, outfile);
            break;
#endif
    }
}

