/****************************************************************************
  Copyright (C) 2018--2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#include <fstream>
#include <sstream>
#include <chrono>
#include <unistd.h>
#include <cstring>
#include <algorithm>
#include <deque>
#include <vector>
#include <csignal>
#include "../src/tensor.hpp"
#include "../src/interval.hpp"
#include "../src/schemes/horner.hpp"
#include "../src/schemes/linear.hpp"
#include "../src/schemes/quadratic.hpp"
//#include "../schemes/quadratic_shift.hpp"

bool stop = false;
bool hardstop = false;
constexpr double l_default = -2;
constexpr double u_default = 2;
#ifdef FLOAT128
using Iprecise = Interval128;
#else
using Iprecise = Interval80;
#endif

void stop_loop(int signum)
{
    if(!stop){
        std::cout << "Subdivision interrupted, will return current enclosing boxes.\n";
        stop = true;
    } else {
        std::cout << "Subdivision interrupted twice, will return.\n";
        hardstop = true;
    }
}

template<typename T>
void remove(std::vector<T> & vec, const std::vector<char> & table, index_t dim = 1)
{
    index_t c = 0;
    for(index_t k=0; k < table.size(); ++k) {
        if(table[k]) {
            c++;
        } else if(c > 0) {
            for(index_t i = 0; i < dim; ++i) {
                vec[dim*(k-c) + i] = vec[dim*k + i];
            }
        }
    }
    vec.resize(vec.size() - dim*c, 0);
}

template<typename T>
void stable_partition(std::vector<T> & vec, const std::vector<char> & table, index_t dim = 1)
{
    index_t c = 0;
    std::vector<T> buffer;
    buffer.resize(vec.size(), 0);
    for(index_t k=0; k < table.size(); ++k) {
        if(!table[k]) {
            for(index_t i = 0; i < dim; ++i) {
                buffer[dim*c + i] = vec[dim*k + i];
            }
            c++;
        } else if(c > 0) {
            for(index_t i = 0; i < dim; ++i) {
                vec[dim*(k-c) + i] = vec[dim*k + i];
            }
        }
    }
    for(index_t i = 0; i < dim*c; ++i) {
        vec[vec.size() - dim*c + i] = buffer[i];
    }
}

template<template<typename I> class Scheme, typename I>
void voxelize_loop(const std::vector<Tensor_poly<I>> & system, const std::vector<char> & signs,
                   Tensor_box<I> & box_set, Tensor_box<I> & box_set_certified,
                   std::vector<std::vector<I>> & evals, 
                   bool guarantee, bool clean_eval, index_t verbose, double inflation_scale)
{
    std::vector<char> nonzeroes;
    std::vector<char> table;
    std::vector<char> match_signs;
    std::vector<char> match_nonzeroes;
    std::vector<char> zeroes;
    index_t dim = Scheme<I>::dimension(system[0].get_number_axes());
    
    bool has_signs_zero = std::find(signs.begin(), signs.end(), 0) != signs.end();
    index_t num_equalities = std::count(signs.begin(), signs.end(), 0);
    std::vector<index_t> ind_equalities(0);
    if(guarantee and num_equalities == 1 and has_signs_zero and Scheme<I>::has_gradient) {
        box_set_certified = box_set;
        inflate(box_set_certified, I(inflation_scale));
    }
    match_signs.resize(box_set.get_number_elements(), 1);
    match_nonzeroes.resize(box_set.get_number_elements(), 0);
    for(index_t j = 0; j < system.size(); ++j) {
        std::vector<I> & last_eval = Scheme<I>::has_gradient?
            evals[j] : evals[0];
        if(guarantee and system.size() == 1 and signs[j] == 0 and Scheme<I>::has_gradient) {
            evaluate<Scheme>(system[j], box_set_certified, last_eval);
        } else {
            evaluate<Scheme>(system[j], box_set, last_eval);
        }
        if(verbose >= 2) {
            std::cout << "First jet of polynomial " << j << ":\n";
            if(last_eval.size() > 0) {
                for(index_t k = 0; k < Scheme<I>::dimension(system[j].get_number_axes()); ++k) {
                    std::cout << last_eval[k] << '\n';
                }
            }
        }
        nonzeroes.resize(box_set.get_number_elements());
        if(ind_equalities.size() >= 1 and Scheme<I>::has_gradient) {
            is_non_zero_modulo<Scheme>(evals, j, ind_equalities, box_set, nonzeroes.begin());
        } else {
            is_non_zero<Scheme>(last_eval.cbegin(), box_set, nonzeroes.begin());
        }
        if(signs[j] == 0) {
            ind_equalities.push_back(j);
        } else {
            table.resize(box_set.get_number_elements());
            for(index_t k = 0; k < nonzeroes.size(); ++k) {
                if(nonzeroes[k] == 0) {
                    match_signs[k] = 0;
                } else if(nonzeroes[k] == -signs[j]) {
                    nonzeroes[k] = 1;
                } else {
                    nonzeroes[k] = 0;
                }
            }
        }
        remove(box_set, nonzeroes);
        remove(match_signs, nonzeroes);
        if(guarantee and Scheme<I>::has_gradient) {
            remove(box_set_certified, nonzeroes);
        }
        if(Scheme<I>::has_gradient and
                (guarantee or clean_eval or (ind_equalities.size() > 0 and j < system.size()-1))) {
            for(index_t k = 0; k < ind_equalities.size(); ++k) {
                remove(evals[ind_equalities[k]], nonzeroes, dim);
            }
        }
    }
    // Certified boxes for case of inequalities only
    if(!has_signs_zero) {
        box_set_certified = box_set;
        remove(box_set, match_signs);
        for(index_t i=0; i<match_signs.size(); ++i) {match_signs[i] = !match_signs[i];}
        remove(box_set_certified, match_signs);
    }
    // Certified boxes for the case of 1 equality
    if(guarantee and num_equalities == 1 and has_signs_zero and Scheme<I>::has_gradient) {
        zeroes.resize(box_set_certified.get_number_elements());
        is_zero<Scheme>(evals[ind_equalities[0]].cbegin(), box_set_certified, zeroes.begin());
        for(index_t i = 0; i < zeroes.size(); ++i) {zeroes[i] = zeroes[i] and match_signs[i];}
        remove(box_set, zeroes);
        stable_partition(evals[0], zeroes, dim);
        for(index_t i = 0; i < zeroes.size(); ++i) {zeroes[i] = !zeroes[i];}
        remove(box_set_certified, zeroes);
    }
}

static void check_input(bool normal, double inflation, index_t refine, index_t max_depth, const std::string & scheme,
              const std::vector<double> & lower, const std::vector<double> & upper,
              index_t center, std::vector<index_t> & grid_sizes,
              std::vector<index_t> & axes, index_t verbose,
              std::vector<char> & signs, const index_t & num_vars)
{
    if(inflation < 1) {
        fprintf(stderr, "Inflation given with -i option should be greater or equal to 1.\n");
        stop = true;
    }
    index_t max_g = 1;
    if(grid_sizes.size() > 0) {
        max_g = *std::max_element(grid_sizes.begin(), grid_sizes.end());
    }
    if(max_depth <= 0 or max_g*(1ul << max_depth) > (1ul<<IDX_SIZE) or max_g*(1ul<<refine) > (1ul<<IDX_SIZE)) {
        fprintf(stderr, "Two power depth given with option -d times maximum grid size\n");
        fprintf(stderr, "should be more than 0 and less than 2^%d. Recompile with -DEXTEND\n", IDX_SIZE);
        fprintf(stderr, "to extend the bound to 2^64.\n");
        stop = true;
    }
    if(static_cast<long>(center) < 0) {
        fprintf(stderr, "Maximum center depth given with -c option should be non-negative.\n");
        stop = true;
    }
    for(index_t i = 0; i < grid_sizes.size(); ++i) {
        if(static_cast<long>(grid_sizes[i]) < 0) {
            fprintf(stderr, "Grid sizes given with -g option should be greater or equal to 0.\n");
            stop = true;
        };
    }
    for(index_t i = 0; i < axes.size(); ++i) {
        if(static_cast<long>(axes[i]) < 0 or axes[i] >= num_vars) {
            fprintf(stderr, "Axes given with option -a should be between 0 and %lu included.\n", num_vars-1);
            stop = true;
        };
        for(index_t j = 0; j < i; ++j) {
            if(axes[i] == axes[j]) {
                fprintf(stderr, "Axes given with option -a should be different.\n");
                stop = true;
            };
        }   
    }
    for(index_t i = 0; i < signs.size(); ++i) {
        if(signs[i] > 1 or signs[i] < -1) {
            fprintf(stderr, "Signs given with -s option should be -1, 0 or 1.\n");
            stop = true;
        }
    }
    if(refine < max_depth) {
        fprintf(stderr, "Warning: refine bound given with -r option should be greater or equal to\n");
        fprintf(stderr, "         maximum depth given with option -d. Set to %zu.\n", max_depth);
    }
    if(lower.size() > num_vars) {
        fprintf(stderr, "Warning: number of values for the lower corner should be less than the \n");
        fprintf(stderr, "         number of variables, %lu.\n", num_vars);
    }
    if(upper.size() > num_vars) {
        fprintf(stderr, "Warning: number of values for the upper corner should be less than the \n");
        fprintf(stderr, "         number of variables, %lu.\n", num_vars);
    }
    if(verbose > 2) {
        fprintf(stderr, "Warning: verbose option should be 0, 1 or 2. Set to 2.\n");
    }
}

template<typename I>
void voxelize(bool normal, bool mesh, double inflation, index_t refine, index_t max_depth, std::string & scheme,
              const std::vector<double> & lower, const std::vector<double> & upper,
              index_t center, std::vector<index_t> & grid_sizes,
              std::vector<index_t> & axes, index_t verbose, bool trim,
              const std::vector<std::string> & inpoly, std::vector<char> & signs,
              std::vector<I>* result,
              std::vector<float>* vertices,
              std::vector<float>* normals,
              std::vector<int32_t>* faces)
{
    int old_rounding = std::fegetround();
    std::fesetround(FE_UPWARD);
    using f = typename I::base_type;
    std::stringstream input;
    std::vector<Tensor_poly<Iprecise>> poly(inpoly.size());
    if(refine < max_depth) {
        refine = max_depth;
    }

    index_t num_vars = 0;
    for(index_t i = 0; i < inpoly.size(); ++i) {
        input = std::stringstream(inpoly[i]);
        input >> poly[i];
        if(poly[i].get_number_axes() > num_vars) {
            num_vars = poly[i].get_number_axes();
            if(i > 0) {
                fprintf(stderr, "The number of variables in the input files doesn't match.\n");
                stop = true;
            }
        }
    }
    check_input(normal, inflation, refine, max_depth, scheme, lower, upper, center, grid_sizes, axes, verbose, signs, num_vars);
    if(verbose >= 2) {
        for(index_t i = 0; i < poly.size(); ++i) {
            std::cout << "Polynomial number " << i << ":\n";
            if(poly[i].get_number_elements() < 100) {
                std::cout << poly[i] << '\n';
            } else {
                print_polynomial(std::cout, poly[i], 100);
                std::cout << '\n';
            }
        }
    }
    // normalize polynomials
    for(index_t i = 0; i < poly.size(); ++i) {
        Iprecise::base_type m = 1;
        for(index_t j = 0; j < poly[i].data.size(); ++j) {
            Iprecise::base_type lm = get_max_abs(poly[i].data[j]);
            if(lm > m) {
                m = lm;
            }
        }
        for(index_t j = 0; j < poly[i].data.size(); ++j) {
            mul(Iprecise(1/m), poly[i].data[j], poly[i].data[j]);
        }
    }
    
    char last_s = (signs.size() >= 1)? signs.back() : 0;
    signs.resize(poly.size(), last_s);
    index_t first_equ = std::find(signs.begin(), signs.end(), 0) - signs.begin();
    bool has_signs_zero = (first_equ != signs.size());

    f l = l_default;
    f u = u_default;
    if(lower.size() >= 1) {
        l = lower.back();
    }
    if(upper.size() >= 1) {
        u = upper.back();
    }
    std::vector<I> b(num_vars, {l, u});
    for(index_t i = 0; i < lower.size(); ++i) {
        b[i].neg_lower = -lower[i];
    }
    for(index_t i = 0; i < upper.size(); ++i) {
        b[i].upper = upper[i];
    }
    index_t g = (grid_sizes.size() >= 1)? grid_sizes.back() : 1;
    grid_sizes.resize(num_vars, g);
    for(index_t i = 0; i < grid_sizes.size(); ++i) {
        if(get_lower(b[i]) == get_upper(b[i])) {
            grid_sizes[i] = 0;
        }
    }
    std::deque<std::vector<Tensor_poly<I>>> systems;
    std::deque<Tensor_box<I>> box_sets;
    std::vector<std::vector<Tensor_box<I>>> box_sets_certified(refine);
    std::vector<std::vector<std::vector<std::array<ply_float,6>>>> centers_normals(refine+1);
    std::vector<std::vector<Iprecise>> centers;

    // set axes
    index_t na;
    if(axes.size() == 0) {
        na = num_vars > 3 ? 3 : num_vars;
        axes.resize(na);
        for(index_t i=0; i<na; ++i) { axes[i] = i; }
    } else {
        na = axes.size();
    }

    auto t0 = std::chrono::high_resolution_clock::now();
    auto t1 = std::chrono::high_resolution_clock::now();
    t0 = std::chrono::high_resolution_clock::now();
    std::vector<Tensor> derivations(poly.size());
    std::vector<Tensor_box<Iprecise>> generic_boxes(poly.size());
    std::vector<Tensor_poly<I>> generic_system(poly.size());
    bool reversed = false;
    if(center > 0 ) {
        for(index_t i = 0; i < poly.size(); ++i) {
            transpose(poly[i], derivations[i]);
            generic_boxes[i] = Tensor_box<Iprecise>(derivations[i]);
            generic_boxes[i].data.resize(num_vars, {0});
            generic_system[i] = Tensor_poly<I>(derivations[i]);
            generic_system[i].data.resize(generic_system[i].idx.back().size(), 0);
        }
        systems.resize(1);
        systems[0] = generic_system;
        box_sets.resize(1);
        box_sets[0] = make_tensor_box(b);
        box_sets_certified[0].resize(1);
        centers.resize(1);
        centers[0] = std::vector<Iprecise>(num_vars, 0);
    } else {
        // to make order of evaluation match polynomial evaluation order
        std::reverse(b.begin(), b.end());
        std::reverse(grid_sizes.begin(), grid_sizes.end());
        for(index_t i=0; i<na; ++i) { axes[i] = num_vars-1-axes[i]; }
        box_sets.resize(1);
        box_sets[0] = make_tensor_box(b);
        systems.resize(1);
        systems[0].resize(poly.size());
        for(index_t i = 0; i < poly.size(); ++i) {
            move(poly[i], systems[0][i]);
        }
        reversed = true;
    }
    std::vector<index_t> directions = grid_sizes;
    for(index_t i = 0; i < directions.size(); ++i) {
        if(grid_sizes[i] > 0) {
            directions[i] = 2;
        }
    }
    split(box_sets[0], grid_sizes);

    // TODO: split box with respect to grid_sizes
    for(index_t i = 0; i < refine; ++i) {
        if(stop) {
            box_sets_certified.resize(i+1);
            refine = i+1;
            //break;
        }
        if(verbose >= 1) {
            std::cout << "depth: " << i << '\n';
        }
        bool lazy = (i >= max_depth-1) and (max_depth < refine);
        if(i >= max_depth) {
            if(scheme.compare("Horner") == 0) {
                scheme = "Linear";
            }
        }
        std::vector<f> condition_numbers;

        if(center > i) {
            // recenter the systems at each box
            index_t Nb = box_sets.size();
            std::vector<index_t> ind(Nb+1);
            ind[0] = Nb;
            for(index_t j = 0; j < Nb; ++j) {
                ind[j+1] = ind[j] + box_sets[j].get_number_elements();
            }
            box_sets.resize(ind.back());
            centers.resize(ind.back(), std::vector<Iprecise>(num_vars,0));
            if(verbose >= 1) {
                std::cout << "Centering " << (ind.back() - Nb)*poly.size()
                          << " polynomial" << ((ind.back()-Nb)*poly.size()>1?"s.\n":".\n");
            }
            for(index_t j = 0; j < Nb; ++j) {
                std::vector<I> lb(num_vars, 0);
                std::vector<I> cb(num_vars, 0);
                std::vector<I> db(num_vars, 0);
                std::vector<index_t> it_box = box_sets[j].begin();  
                std::vector<small_index_t> idx(num_vars);  
                for(index_t k = 0; k < box_sets[j].get_number_elements(); ++k) {
                    get_box(box_sets[j], it_box, lb);
                    box_sets[j].get_idx(it_box, idx);
                    for(index_t m = 0; m < lb.size(); ++m) {
                        //mid(lb[m], cb[m]);
                        //delta(lb[m], db[m]);
                        cb[m] = I(get_near_zero(lb[m]));
                        shift_to_zero(lb[m], db[m]);
                        add(centers[j][m], Iprecise(cb[m]), centers[ind[j] + k][m]);
                    }
                    box_sets[ind[j] + k] = make_tensor_box(db, idx);
                    box_sets[j].advance(it_box);
                }
            }
            box_sets.erase(box_sets.begin(), box_sets.begin() + Nb);
            centers.erase(centers.begin(), centers.begin() + Nb);
            systems.resize(ind.back()-Nb, generic_system);

            #pragma omp parallel for shared(systems, centers, generic_boxes, ind, poly)
            for(index_t k = 0; k < centers.size(); ++k) {
                std::vector<Tensor_box<Iprecise>> local_generic_boxes = generic_boxes;
                std::vector<Iprecise> new_coeffs;
                for(index_t p = 0; p < poly.size(); ++p) {
                    for(index_t m = 0; m < num_vars; ++m) {
                        local_generic_boxes[p].data[m][0] = centers[k][num_vars-1-m];
                    }
                    evaluate<Shift>(poly[p], local_generic_boxes[p], new_coeffs);
                    for(index_t m = 0; m < new_coeffs.size(); ++m) {
                        systems[k][p].data[m] = I(new_coeffs[m]);
                    }
                }
            }
            //systems.erase(systems.begin(), systems.begin() + Nb);

            if(verbose >= 2) {
                for(index_t j = 0; j < box_sets.size(); ++j) {
                    std::cout << "Centered at ";
                    for(index_t k = 0; k < num_vars; ++k) {
                        std::cout << centers[j][k];
                    }
                    std::cout << ":\n";
                    for(index_t k = 0; k < systems[j].size(); ++k) {
                        std::cout << "Polynomial " << k << ":\n";
                        if(systems[j][k].get_number_elements() < 100) {
                            std::cout << systems[j][k] << '\n';
                        } else {
                            print_polynomial(std::cout, systems[j][k], 100);
                            std::cout << '\n';
                        }
                    }
                }
            }
        }
        

        box_sets_certified[i].resize(box_sets.size());
        centers_normals[i].resize(box_sets.size());
        bool last = (i == refine - 1) and (normal or mesh);
        bool recenter = (center > 0) and (i >= center-1) and has_signs_zero and (i < refine - 1);
        //bool recenter = false;
        bool clean_eval = last or recenter;
        if(i == refine - 1) {
            centers_normals[refine].resize(box_sets.size());
        }
        if(verbose >= 1) {
            index_t Nb = 0;
            for(index_t j = 0; j < box_sets.size(); ++j) {Nb += box_sets[j].get_number_elements();}
            std::cout << "Evaluating " << (Nb<<num_vars) << " box" << (Nb>0?"es.\n":".\n");
        }
        for(index_t j = 0; j < box_sets.size(); ++j) {
            if(hardstop) {
                box_sets_certified.resize(i+1);
                box_sets_certified[i].resize(j);
                box_sets.resize(j);
                refine = i+1;
                break;
            }
            if(verbose >= 2) {
                std::cout << "Cell " << j << " contains " << (box_sets[j].get_number_elements()<<num_vars) << " boxes.\n";
            }
            std::vector<std::vector<I>> evals(systems[j].size());
            
            split(box_sets[j], directions);

            if(scheme.compare("Linear") == 0) {
                if(verbose >= 2) {
                    std::cout << "Linear scheme\n";
                }
                voxelize_loop<Linear>(systems[j], signs, box_sets[j], box_sets_certified[i][j], evals, lazy, clean_eval, verbose, inflation);
                if(normal or mesh) {
                    index_t N = box_sets_certified[i][j].get_number_elements();
                    if(N > 0) {
                        get_centers_normals<Linear>(box_sets_certified[i][j], evals, 0, N, signs, axes, centers_normals[i][j]);
                    }
                    if(i == refine-1) {
                        index_t M = box_sets[j].get_number_elements();
                        if(M > 0) {
                            get_centers_normals<Linear>(box_sets[j], evals, N, N+M, signs, axes, centers_normals[refine][j]);
                        }
                    }
                }
                if(recenter) {
                    get_condition_numbers<Linear>(evals[first_equ].begin(), box_sets[j], condition_numbers);
                }
            } else if(scheme.compare("Quadratic") == 0) {
                if(verbose >= 2) {
                    std::cout << "Quadratic scheme\n";
                }
                voxelize_loop<Quadratic>(systems[j], signs, box_sets[j], box_sets_certified[i][j], evals, lazy, clean_eval, verbose, inflation);
                if(normal or mesh) {
                    index_t N = box_sets_certified[i][j].get_number_elements();
                    if(N > 0) {
                        get_centers_normals<Quadratic>(box_sets_certified[i][j], evals, 0, N, signs, axes, centers_normals[i][j]);
                    }
                    if(i == refine-1) {
                        index_t M = box_sets[j].get_number_elements();
                        if(M > 0) {
                            get_centers_normals<Quadratic>(box_sets[j], evals, N, N+M, signs, axes, centers_normals[refine][j]);
                        }
                    }
                }
                if(recenter) {
                    get_condition_numbers<Quadratic>(evals[first_equ].begin(), box_sets[j], condition_numbers);
                }
            } else {
                if(verbose >= 2) {
                    std::cout << "Horner scheme\n";
                }
                voxelize_loop<Horner>(systems[j], signs, box_sets[j], box_sets_certified[i][j], evals, lazy, clean_eval, verbose, inflation);
                if(i == refine-1) {
                    index_t M = box_sets[j].get_number_elements();
                    centers_normals[refine][j].resize(M, {0,0,0,0,0,0});
                }
                if(recenter) {
                    get_condition_numbers<Horner>(evals[first_equ].begin(), box_sets[j], condition_numbers);
                }
            }
            if(center > 0) {
                // the center is not reversed to match the transposed polynomial
                std::vector<I> c(num_vars, 0);
                for(index_t k = 0; k < num_vars; ++k) {
                    c[k] = I(centers[j][k]);
                }
                shift(box_sets_certified[i][j], c);
                if(recenter) {
                    // recenter the systems at each box
                    bool unstable = false;
                    index_t unstable_ind = 0;
                    index_t unstable_num = 0;
                    index_t unstable_den = 1;
                    for(index_t k = 0; k < box_sets[j].get_number_elements(); ++k) {
                        if(condition_numbers[2*k+1] == 0) {
                            unstable = true;
                            if((unstable_den > 0) or (unstable_den == 0 and unstable_num < condition_numbers[2*k])) {
                                unstable_num = condition_numbers[2*k+1];
                                unstable_ind = k;
                            }
                        } else if (unstable_num * condition_numbers[2*k+1] < condition_numbers[2*k] * unstable_den) {
                                unstable_num = condition_numbers[2*k];
                                unstable_den = condition_numbers[2*k+1];
                                unstable_ind = k;
                        }
                    }
                    if(unstable) {
                        std::vector<I> lb(num_vars, 0);
                        std::vector<I> cb(num_vars, 0);
                        std::vector<I> db(num_vars, 0);
                        std::vector<index_t> it_box = box_sets[j].begin();  
                        for(index_t k = 0; k < unstable_ind; ++k) {
                            box_sets[j].advance(it_box);
                        }
                        get_box(box_sets[j], it_box, lb);
                        for(index_t m = 0; m < lb.size(); ++m) {
                            mid(lb[m], cb[m]);
                            add(centers[j][m], Iprecise(cb[m]), centers[j][m]);
                            neg(cb[m], cb[m]);
                        }
                        shift(box_sets[j], cb);
                        if(verbose >= 1) {
                            std::cout << "Recentering polynomials in cell " << j << "\n";
                        }
                        //#pragma omp parallel for shared(systems, centers, generic_boxes, ind, poly)
                        std::vector<Tensor_box<Iprecise>> local_generic_boxes = generic_boxes;
                        std::vector<Iprecise> new_coeffs;
                        for(index_t p = 0; p < poly.size(); ++p) {
                            for(index_t m = 0; m < num_vars; ++m) {
                                local_generic_boxes[p].data[m][0] = centers[j][num_vars-1-m];
                            }
                            evaluate<Shift>(poly[p], local_generic_boxes[p], new_coeffs);
                            for(index_t m = 0; m < new_coeffs.size(); ++m) {
                                systems[j][p].data[m] = I(new_coeffs[m]);
                            }
                        }
                        //systems.erase(systems.begin(), systems.begin() + Nb);

                        if(verbose >= 2) {
                            std::cout << "Centered at ";
                            for(index_t k = 0; k < num_vars; ++k) {
                                std::cout << centers[j][k];
                            }
                            std::cout << ":\n";
                            for(index_t k = 0; k < systems[j].size(); ++k) {
                                std::cout << "Polynomial " << k << ":\n";
                                if(systems[j][k].get_number_elements() < 100) {
                                    std::cout << systems[j][k] << '\n';
                                } else {
                                    print_polynomial(std::cout, systems[j][k], 100);
                                    std::cout << '\n';
                                }
                            }
                        }
                    }
                }
            }
            if(verbose >= 2 and (normal or mesh)) {
                if(lazy) {
                    if(centers_normals[i][j].size() > 0) {
                        std::cout << "First normal of certified boxes of cell " << j << ":\n";
                        std::cout << centers_normals[i][j][0][4] << ' ';
                        std::cout << centers_normals[i][j][0][5] << ' ';
                        std::cout << centers_normals[i][j][0][6] << '\n';
                    }
                }
                if(last) {
                    if(centers_normals[refine][j].size() > 0) {
                        std::cout << "First normal of non certified boxes cell " << j << ":\n";
                        std::cout << centers_normals[refine][j][0][4] << ' ';
                        std::cout << centers_normals[refine][j][0][5] << ' ';
                        std::cout << centers_normals[refine][j][0][6] << '\n';
                    }
                }
            }
        }
    }
    if(center > 0) {
        if(verbose >= 1) {
            std::cout << "Shifting boxes. \n";
        }
        std::vector<I> c(num_vars, 0);
        for(index_t j = 0; j < box_sets.size(); ++j) {
            for(index_t k = 0; k < num_vars; ++k) {
                c[k] = I(centers[j][k]);
            }
            shift(box_sets[j], c);
        }
    }
    t1 = std::chrono::high_resolution_clock::now();
    if(verbose >= 1) {
        std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << ".\n";
    }

    index_t Nb = 0;
    for(index_t i = 0; i < box_sets_certified.size(); ++i) {
        for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {Nb += box_sets_certified[i][j].get_number_elements();}
    }
    for(index_t j = 0; j < box_sets.size(); ++j) {
        Nb += box_sets[j].get_number_elements();
    }

    if(normal or mesh) {
        if(verbose >= 1) {
            std::cout << "Computation of the normals. \n";
        }
        t0 = std::chrono::high_resolution_clock::now();

        std::vector<std::array<ply_float,6>> points;
        points.reserve(Nb);
        index_t skipped = 0;
        std::vector<std::vector<index_t>> box_sizes(0);
        index_t Nlast = 0;
        for(index_t i = 0; i < refine; ++i) {
            box_sizes.push_back(std::vector<index_t>(1,Nlast));
            for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {
                index_t s = append_points_normals(box_sets_certified[i][j], centers_normals[i][j], axes, points, trim);
                if(verbose >= 2 and s > 0) {
                    std::cout << s << " ill-conditioned points in cell " << j << " of level " << i << ".\n";
                }
                skipped += s;
                index_t N = box_sizes[i].back() + box_sets_certified[i][j].get_number_elements();
                box_sizes[i].push_back(N);
            }
            Nlast = box_sizes[i].back();
        }
        box_sizes.push_back(std::vector<index_t>(1,Nlast));
        for(index_t k = 0; k < box_sets.size(); ++k) {
            index_t s = append_points_normals(box_sets[k], centers_normals[refine][k], axes, points, trim);
            if(verbose >= 2 and s > 0) {
                std::cout << s << " ill-conditioned points in cell " << k << ".\n";
            }
            skipped += s;
            index_t N = box_sizes[refine].back() + box_sets[k].get_number_elements();
            box_sizes[refine].push_back(N);
        }
        if(verbose >= 1 and skipped > 0) {
            std::cout << skipped << " ill-conditioned points removed.\n";
        }
        vertices->resize(3*points.size());
        for(index_t j = 0; j < points.size(); ++j) {
            (*vertices)[3*j]   = points[j][0];
            (*vertices)[3*j+1] = points[j][1];
            (*vertices)[3*j+2] = points[j][2];
        }
        normals->resize(3*points.size());
        for(index_t j = 0; j < points.size(); ++j) {
            (*normals)[3*j]   = points[j][3];
            (*normals)[3*j+1] = points[j][4];
            (*normals)[3*j+2] = points[j][5];
        }
        t1 = std::chrono::high_resolution_clock::now();
        if(verbose >= 1) {
            std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << ".\n";
        }
        if(mesh) {
            if(verbose >= 1) {
                std::cout << "Computation of the mesh. \n";
            }
            t0 = std::chrono::high_resolution_clock::now();

            // number of neighbours of a hyercube at distance 2 in norm 1
            index_t N2 = 2*num_vars + 2*num_vars*(num_vars-1);
            // a block is sorted by size, tie in lexicographical order :
            // For dim 3 :
            //  case -1 and 1 : [-e1, -e2, -e3, e1, e2, e3,
            //  case -1,-1 and -1, 1: [-e1-e2, -e1-e3, -e2-e3, -e1+e2, -e1+e3, -e2+e3]
            //  case 1,-1 and 1, 1: [e1-e2, e1-e3, e2-e3, e1+e2, e1+e3, e2+e3]
            std::vector<int32_t> neighbours(N2*box_sizes.back().back(), -1);
            // mesh certified boxes
            for(index_t i = 0; i < refine; ++i) {
                for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {
                    for(index_t m = 0; m <= i; ++m) {
                        for(index_t n = 0; n < box_sets_certified[m].size(); ++n) {
                            append_neighbours(box_sets_certified[i][j], box_sizes[i][j], i,
                                              box_sets_certified[m][n], box_sizes[m][n], m,
                                              neighbours);
                        }
                    }
                }
            }
            // mesh remaining boxes
            for(index_t j = 0; j < box_sets.size(); ++j) {
                for(index_t n = 0; n < box_sets.size(); ++n) {
                    append_neighbours(box_sets[j], box_sizes[refine][j], refine - 1,
                                      box_sets[n], box_sizes[refine][n], refine - 1,
                                      neighbours);
                }
                for(index_t n = 0; n < box_sets_certified[refine-1].size(); ++n) {
                    append_neighbours(box_sets_certified[refine-1][n], box_sizes[refine-1][n], refine-1,
                                      box_sets[j], box_sizes[refine][j], refine - 1,
                                      neighbours);
                }
                for(index_t m = 0; m < refine; ++m) {
                    for(index_t n = 0; n < box_sets_certified[m].size(); ++n) {
                        append_neighbours(box_sets[j], box_sizes[refine][j], refine - 1,
                                          box_sets_certified[m][n], box_sizes[m][n], m,
                                          neighbours);
                    }
                }
            }
            std::vector<struct ply_triangle> triangles(0);
            append_triangles(points, neighbours, num_vars, triangles);
            t1 = std::chrono::high_resolution_clock::now();
            if(verbose >= 1) {
                std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count() << " ms" << ".\n";
            }
            //std::ofstream file(outfile, std::ios::out | std::ios::binary);
            faces->resize(3*triangles.size());
            for(index_t j = 0; j < triangles.size(); ++j) {
                (*faces)[3*j]   = triangles[j].v0;
                (*faces)[3*j+1] = triangles[j].v1;
                (*faces)[3*j+2] = triangles[j].v2;
            }
        }
    }// else {
        result->clear();
        result->reserve(num_vars*Nb);
        for(index_t i = 0; i < box_sets_certified.size(); ++i) {
            for(index_t j = 0; j < box_sets_certified[i].size(); ++j) {
                append_boxes(box_sets_certified[i][j], reversed, *result);
            }
        }
        for(index_t k = 0; k < box_sets.size(); ++k) {
            append_boxes(box_sets[k], reversed, *result);
        }
    //}
    std::fesetround(old_rounding);
}

extern "C" {

    std::vector<float>* new_vector_float() {
        std::vector<float>* result = new std::vector<float>();
        return result;
    }

    void delete_vector_float(std::vector<float>* v) {
        delete v;
    }

    std::vector<int32_t>* new_vector_int32() {
        std::vector<int32_t>* result = new std::vector<int32_t>();
        return result;
    }

    void delete_vector_int32(std::vector<int32_t>* v) {
        delete v;
    }


    void solve_32(char** input, int num_polys, bool normal, bool mesh, double* lower, int lsize, double* upper, int usize, int* grid_sizes, int gsize, int* axes, int asize,
                 int center, int max_depth, int refine, char* scheme, int verbose, bool trim, int* signs, int ssize, std::vector<Interval32>* result, int64_t* Nboxcoords,
                 std::vector<float>* vertices, std::vector<float>* normals, int64_t* Nvertices, std::vector<int32_t>* faces, int64_t* Nfaces)
                
    {
        index_t l_refine = static_cast<index_t>(refine);
        index_t l_max_depth = static_cast<index_t>(max_depth);
        std::string l_scheme(scheme);
        std::vector<double> lo(lower, lower+lsize);
        std::vector<double> up(upper, upper+usize);
        index_t l_verbose = static_cast<index_t>(verbose);
        index_t l_center = static_cast<index_t>(center);
        std::vector<index_t> l_grid_sizes(grid_sizes, grid_sizes+gsize);
        std::vector<index_t> ax(axes, axes+asize);
        std::vector<char> sig(signs, signs+ssize);
        double inflation = 1.1;

        std::vector<std::string> inpoly(num_polys);
        for(int i = 0; i < num_polys; ++i) {
            inpoly[i] = std::string(input[i]);
        }

        auto python_sighandler = signal(SIGINT, stop_loop);
        voxelize<Interval32> (normal, mesh, inflation, l_refine, l_max_depth, l_scheme, lo, up, l_center, l_grid_sizes, ax, l_verbose, trim, inpoly, sig, result, vertices, normals, faces);
        signal(SIGINT, python_sighandler);
        stop = false;
        *Nboxcoords = result->size()*2;
        *Nvertices = vertices->size();
        *Nfaces = faces->size();
    }
    
    void vector_32_to_array(std::vector<Interval32>* input, float* result)
    {
        for(index_t i = 0; i < input->size(); ++i) {
            result[2*i]   = -(*input)[i].neg_lower;
            result[2*i+1] = (*input)[i].upper;
        }
    }

    void vector_float_to_array(std::vector<float>* input, float* result)
    {
        std::copy(input->begin(), input->end(), result);
    }

    void vector_int32_to_array(std::vector<int32_t>* input, int32_t* result)
    {
        std::copy(input->begin(), input->end(), result);
    }

    std::vector<Interval32>* new_vector_32() {
        std::vector<Interval32>* result = new std::vector<Interval32>();
        return result;
    }

    void delete_vector_32(std::vector<Interval32>* v) {
        delete v;
    }

    void solve_64(char** input, int num_polys, bool normal, bool mesh, double* lower, int lsize, double* upper, int usize, int* grid_sizes, int gsize, int* axes, int asize,
                 int center, int max_depth, int refine, char* scheme, int verbose, bool trim, int* signs, int ssize, std::vector<Interval64>* result,int64_t* Nboxcoords,
                 std::vector<float>* vertices, std::vector<float>* normals, int64_t* Nvertices, std::vector<int32_t>* faces, int64_t* Nfaces)
    {
        index_t l_refine = static_cast<index_t>(refine);
        index_t l_max_depth = static_cast<index_t>(max_depth);
        std::string l_scheme(scheme);
        std::vector<double> lo(lower, lower+lsize);
        std::vector<double> up(upper, upper+usize);
        index_t l_verbose = static_cast<index_t>(verbose);
        index_t l_center = static_cast<index_t>(center);
        std::vector<index_t> l_grid_sizes(grid_sizes, grid_sizes+gsize);
        std::vector<index_t> ax(axes, axes+asize);
        std::vector<char> sig(signs, signs+ssize);
        double inflation = 1.1;

        std::vector<std::string> inpoly(num_polys);
        for(int i = 0; i < num_polys; ++i) {
            inpoly[i] = std::string(input[i]);
        }

        auto python_sighandler = signal(SIGINT, stop_loop);
        voxelize<Interval64> (normal, mesh, inflation, l_refine, l_max_depth, l_scheme, lo, up, l_center, l_grid_sizes, ax, l_verbose, trim, inpoly, sig, result, vertices, normals, faces);
        signal(SIGINT, python_sighandler);
        stop = false;
        *Nboxcoords = result->size()*2;
        *Nvertices = vertices->size();
        *Nfaces = faces->size();
    }
    
    void vector_64_to_array(std::vector<Interval64>* input, double* result)
    {
        for(index_t i = 0; i < input->size(); ++i) {
            result[2*i]   = -(*input)[i].neg_lower;
            result[2*i+1] = (*input)[i].upper;
        }
    }

    std::vector<Interval64>* new_vector_64() {
        std::vector<Interval64>* result = new std::vector<Interval64>();
        return result;
    }

    void delete_vector_64(std::vector<Interval64>* v) {
        delete v;
    }

    void solve_80(char** input, int num_polys, bool normal, bool mesh, double* lower, int lsize, double* upper, int usize, int* grid_sizes, int gsize, int* axes, int asize,
                 int center, int max_depth, int refine, char* scheme, int verbose, bool trim, int* signs, int ssize, std::vector<Interval80>* result, int64_t* Nboxcoords,
                 std::vector<float>* vertices, std::vector<float>* normals, int64_t* Nvertices, std::vector<int32_t>* faces, int64_t* Nfaces)
    {
        index_t l_refine = static_cast<index_t>(refine);
        index_t l_max_depth = static_cast<index_t>(max_depth);
        std::string l_scheme(scheme);
        std::vector<double> lo(lower, lower+lsize);
        std::vector<double> up(upper, upper+usize);
        index_t l_verbose = static_cast<index_t>(verbose);
        index_t l_center = static_cast<index_t>(center);
        std::vector<index_t> l_grid_sizes(grid_sizes, grid_sizes+gsize);
        std::vector<index_t> ax(axes, axes+asize);
        std::vector<char> sig(signs, signs+ssize);
        double inflation = 1.1;

        std::vector<std::string> inpoly(num_polys);
        for(int i = 0; i < num_polys; ++i) {
            inpoly[i] = std::string(input[i]);
        }

        auto python_sighandler = signal(SIGINT, stop_loop);
        voxelize<Interval80> (normal, mesh, inflation, l_refine, l_max_depth, l_scheme, lo, up, l_center, l_grid_sizes, ax, l_verbose, trim, inpoly, sig, result, vertices, normals, faces);
        signal(SIGINT, python_sighandler);
        stop = false;
        *Nboxcoords = result->size()*2;
        *Nvertices = vertices->size();
        *Nfaces = faces->size();
    }
    
    void vector_80_to_array(std::vector<Interval80>* input, long double* result)
    {
        for(index_t i = 0; i < input->size(); ++i) {
            result[2*i]   = -(*input)[i].neg_lower;
            result[2*i+1] = (*input)[i].upper;
        }
    }

    std::vector<Interval80>* new_vector_80() {
        std::vector<Interval80>* result = new std::vector<Interval80>();
        return result;
    }

    void delete_vector_80(std::vector<Interval80>* v) {
        delete v;
    }
}
