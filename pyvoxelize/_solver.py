#*****************************************************************************
#       Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

import collections.abc
collections.Iterable = collections.abc.Iterable # Workaround vispy bug
import platform
import glob
import os
import sysconfig
import ctypes as ct
import numpy as np
import warnings
import time
import multiprocessing as mp
try:
    import scipy.spatial
    import scipy.sparse
    import vispy.scene
    import vispy.app
    import imageio
except ImportError:
    pass


try:
    path = os.path.dirname(__file__)
    libname = glob.glob(os.path.join(path, "_libvoxelize*"))[0]
except:
    path = os.path.join(sysconfig.get_paths()['purelib'], 'pyvoxelize')
    libname = glob.glob(os.path.join(path, "_libvoxelize*"))[0]

libvoxelize = ct.cdll.LoadLibrary(libname)

libvoxelize.new_vector_float.restype = ct.c_void_p
libvoxelize.new_vector_float.argtypes = []
libvoxelize.delete_vector_float.restype = None
libvoxelize.delete_vector_float.argtypes = [ct.c_void_p]
libvoxelize.vector_float_to_array.restype = None
libvoxelize.vector_float_to_array.argtypes = [ct.c_void_p, np.ctypeslib.ndpointer(ct.c_float, flags="C_CONTIGUOUS")]

libvoxelize.new_vector_int32.restype = ct.c_void_p
libvoxelize.new_vector_int32.argtypes = []
libvoxelize.delete_vector_int32.restype = None
libvoxelize.delete_vector_int32.argtypes = [ct.c_void_p]
libvoxelize.vector_int32_to_array.restype = None
libvoxelize.vector_int32_to_array.argtypes = [ct.c_void_p, np.ctypeslib.ndpointer(ct.c_int32, flags="C_CONTIGUOUS")]

solve_signature = [ct.POINTER(ct.c_char_p), ct.c_int,
                   ct.c_bool, ct.c_bool,
                   ct.POINTER(ct.c_double), ct.c_int,
                   ct.POINTER(ct.c_double), ct.c_int,
                   ct.POINTER(ct.c_int), ct.c_int,
                   ct.POINTER(ct.c_int), ct.c_int,
                   ct.c_int, ct.c_int, ct.c_int, ct.c_char_p,
                   ct.c_int, ct.c_bool, ct.POINTER(ct.c_int), ct.c_int,
                   ct.c_void_p, ct.POINTER(ct.c_int),
                   ct.c_void_p, ct.c_void_p, ct.POINTER(ct.c_int),
                   ct.c_void_p, ct.POINTER(ct.c_int)]

libvoxelize.solve_32.restype  = None
libvoxelize.solve_32.argtypes = solve_signature
libvoxelize.vector_32_to_array.restype = None
libvoxelize.vector_32_to_array.argtypes = [ct.c_void_p, np.ctypeslib.ndpointer(ct.c_float, flags="C_CONTIGUOUS")]
libvoxelize.new_vector_32.restype = ct.c_void_p
libvoxelize.new_vector_32.argtypes = []
libvoxelize.delete_vector_32.restype = None
libvoxelize.delete_vector_32.argtypes = [ct.c_void_p]

libvoxelize.solve_64.restype  = ct.c_int64
libvoxelize.solve_64.argtypes = solve_signature
libvoxelize.vector_64_to_array.restype = None
libvoxelize.vector_64_to_array.argtypes = [ct.c_void_p, np.ctypeslib.ndpointer(ct.c_double, flags="C_CONTIGUOUS")]
libvoxelize.new_vector_64.restype = ct.c_void_p
libvoxelize.new_vector_64.argtypes = []
libvoxelize.delete_vector_64.restype = None
libvoxelize.delete_vector_64.argtypes = [ct.c_void_p]

libvoxelize.solve_80.restype  = ct.c_int64
libvoxelize.solve_80.argtypes = [ct.POINTER(ct.c_char_p), ct.c_int,
                                 ct.c_bool, ct.c_bool,
                                 ct.POINTER(ct.c_double), ct.c_int,
                                 ct.POINTER(ct.c_double), ct.c_int,
                                 ct.POINTER(ct.c_int), ct.c_int,
                                 ct.POINTER(ct.c_int), ct.c_int,
                                 ct.c_int, ct.c_int, ct.c_int, ct.c_char_p,
                                 ct.c_int, ct.c_bool, ct.POINTER(ct.c_int), ct.c_int,
                                 ct.c_void_p, ct.POINTER(ct.c_int),
                                 ct.c_void_p, ct.c_void_p, ct.POINTER(ct.c_int),
                                 ct.c_void_p, ct.POINTER(ct.c_int)]
libvoxelize.vector_80_to_array.restype = None
libvoxelize.vector_80_to_array.argtypes = [ct.c_void_p, np.ctypeslib.ndpointer(ct.c_longdouble, flags="C_CONTIGUOUS")]
libvoxelize.new_vector_80.restype = ct.c_void_p
libvoxelize.new_vector_80.argtypes = []
libvoxelize.delete_vector_80.restype = None
libvoxelize.delete_vector_80.argtypes = [ct.c_void_p]

def numtobytes(e):
    if isinstance(e, (str, bytes)):
        res = e.encode()
    elif isinstance(e, int) or np.issubdtype(type(e), np.integer):
        res = str(e).encode()
    elif isinstance(e, float):
        res = str(e).encode()
    elif isinstance(e, np.inexact) and np.finfo(e).precision <= 15:
        res = str(float(e)).encode()
    elif hasattr(e, '__getitem__') and len(e) == 2 and not hasattr(e[0], '__getitem__'):
        res = b'[' + numtobytes(e[0]) + b',' + numtobytes(e[1]) + b']'
    else:
        raise TypeError('element of type {0} cannot be represented'.format(type(e)))
    return res

def array_to_poly(p):
    ind = p.nonzero()
    result = list(zip(p[ind], zip(*ind)))
    return result

def expand_dim(p, dim):
    if dim is not None and len(p) > 0:
        delta = dim - len(p[0][1])
        if(delta > 0):
            q = [(c,tuple(e) + (0,)*delta) for c,e in p]
            print(q)
            return q
    return p

def format_polys(polys, deg_bound=20, dim=None):
    if isinstance(polys, np.ndarray) or callable(polys):
        list_polys = [polys]
    elif (hasattr(polys, '__iter__') and len(polys) > 0 and hasattr(polys[0], '__iter__')
          and len(polys[0]) == 2 and hasattr(polys[0][1], '__iter__')
          and len(polys[0][1]) > 0 and all (type(e) is int for e in polys[0][1])):
        list_polys = [polys]
    else:
        list_polys = polys
    if any(callable(p) for p in list_polys):
        warnings.warn("approximate coefficients built from evaluation")
    sane_polys = [ array_to_poly(p) if isinstance(p, np.ndarray)
                   else array_to_poly(get_approximate_coefficients(p, deg_bound)) if callable(p)
                   else p 
                   for p in list_polys]
    sane_polys = [expand_dim(p, dim) for p in sane_polys if len(p)>0]
    return sane_polys


def get_poly(filename):
    with open(filename, 'r') as input_poly:
        p = []
        for line in input_poly:
            l = line.strip()
            if l and l[0] != '#':
                m = l.split()
                p.append((m[0], tuple(int(i) for i in m[1:])))
        return p

def get_approximate_coefficients(function, deg_bound=20, epsilon=1e-10):
    """ Compute the polynomial coefficients associated with a function.

    Parameters
    ----------
    function: a function representing a polynomial or a tuple of
    polynomials.

    deg_bound: a bound on the degree of the polynomials.

    Return
    ------
    An array or a tuple of arrays filled with the coefficients of the
    polynomials.
    """
    n = function.__code__.co_argcount
    X = (np.exp(-2j*np.pi*a/deg_bound) for a in np.ogrid[[slice(0, deg_bound)]*n])
    res = function(*X)
    if isinstance(res, np.ndarray):
        poly = np.fft.ifftn(res).real
        poly[np.isclose(poly,0,atol=epsilon)] = 0
        coords = poly.nonzero()
        poly = poly[tuple(slice(0, max(c)+1) for c in coords)]
        return poly
    else:
        polys = tuple(np.fft.ifftn(val).real for val in res)
        upper = np.zeros(n, dtype=int)
        for p in polys:
            p[np.isclose(p,0,atol=epsilon)] = 0
            hull = [max(c)+1 for c in p.nonzero()]
            upper = np.maximum(upper, hull)
        result = []
        for p in polys:
            result.append( p[tuple(slice(0, u) for u in upper)] )
        return result

def get_coefficients(*args, **kwargs):
    warnings.warn("approximate coefficients built from evaluation")
    return get_approximate_coefficients(*args, **kwargs)


def voxelize(eqs, ineqs, mesh=False, normal=False, lower=-2, upper=2, min_depth=7, max_depth=7, axes = [0,1,2],
        center=0, grid_size=1, precision=64, order=2, verbose=0, deg_bound=20, dim=None):
    """ Compute enclosing boxes of the solutions of a system of polynomial
    equations.

    Given a list of polynomials polys = [p1, ..., pn] and a list, this
    function outputs an array of intervals encoding the enclosing boxes of
    the solutions of the system p1 = ... = pn = 0.

    Parameters
    ----------
    eqs: list of polynomials
        Polynomial equations. Each polynomial in the list can be represented
        either with a sparse or a dense representation. See below for a
        description of those representations.

    ineqs: list of polynomials
        Polynomial positive inequalities. Each polynomial in the list can be
        represented either with a sparse or a dense representation. See 
        below for a description of those representations.

    lower: list of numbers or number
        The coordinates of the lower corner of the domain box; if the number
        of values is less than the number of variables, the last value is
        used for the remaining coordinates
        (default: -2)

    upper: list of number or number
        The coordinates of the upper corner of the domain box; if the number
        of values is less than the number of variables, the last value is
        used for the remaining coordinates
        (default: 2)
        
    min_depth: integer
        Integer for a depth of the subdivision tree; until
        the value min_depth, all the boxes are subdivided
        (default: 7)

    max_depth: integer
        Integer for a depth in the subdivision tree; the boxes at a depth
        greater or equal to min_depth and less than max_depth are
        subdivided only if they are not guaranteed to contain a solution of
        the input system
        (default: 0)

    axes: list of integers
        the indices of the variables to visualize; if the list is empty,
        the first indices of the variables are used
        (default: [])

    grid_size: list of integers or integer
        The initial view box is uniformly divided in each direction
        according to the corresponding number in grid size; if the number
        of values is less than the number of variables, the last value is
        used for the remaining coordinates.
        (default: 1)

    center: integer
        Integer for a depth in the subdivision tree; until the
        value depth, on each box the polynomials are evaluated
        after being recentered.
        (default: 0)

    precision: integer
        The precision of the intermediate computations either 32, 64 or 80.
        (default: 64)

    order: integer
        Selects the order of the expansion for the evaluation: 0 is direct,
        1 is linear and 2 is quadratic
        (default: 2)

    verbose: integer
        0, 1 or 2 for the level of verbose
        (default: 1)

    Representation of polynomials
    -----------------------------
    * Sparse representation
        A polynomial is represented as a list of monomials, where each
        monomial is of the form (coeff, monomial), where coeff is a number
        and monomial is a tuple of positive integer exponents.

    * Dense representation
        A polynomial is represented as a numpy.ndarray, where the the
        coefficient of a monomial is the corresponding entry in the array.

    Examples
    --------

    Compute the boxes enclosing the intersection of a unit circle and an
    ellipsoid.
    >>> p = [(1,(2,0)), (1,(0,2)), (-1,(0,0))]
    >>> q = [(0.5,(2,0)), (1.5,(0,2)), (-1,(0,0))]
    >>> solve([p,q], [-2,-2], [2,2], 7, 12)
    array([[[-0.70751953, -0.70703125],
            [-0.70751953, -0.70703125]],
    <BLANKLINE>
           [[-0.70751953, -0.70703125],
            [ 0.70703125,  0.70751953]],
    <BLANKLINE>
           [[ 0.70703125,  0.70751953],
            [-0.70751953, -0.70703125]],
    <BLANKLINE>
           [[ 0.70703125,  0.70751953],
            [ 0.70703125,  0.70751953]]])

    Compute the boxes of width at least 4/2**7 enclosing the unit circle
    >>> solve([p], [-2,-2], [2,2], 7, 12)
    array([[[-1.00048828e+00, -1.00000000e+00],
            [-4.88281250e-04,  0.00000000e+00]],
    <BLANKLINE>
           [[-1.00000000e+00, -9.99511719e-01],
            [-3.12500000e-02, -3.07617188e-02]],
    <BLANKLINE>
           [[-1.00000000e+00, -9.99511719e-01],
            [-3.07617188e-02, -3.02734375e-02]],
    <BLANKLINE>
           ...,
    <BLANKLINE>
           [[ 9.99511719e-01,  1.00000000e+00],
            [ 3.02734375e-02,  3.07617188e-02]],
    <BLANKLINE>
           [[ 9.99511719e-01,  1.00000000e+00],
            [ 3.07617188e-02,  3.12500000e-02]],
    <BLANKLINE>
           [[ 1.00000000e+00,  1.00048828e+00],
            [-0.00000000e+00,  4.88281250e-04]]])
    """

    sane_eqs = format_polys(eqs, deg_bound, dim)
    sane_ineqs = format_polys(ineqs, deg_bound, dim)
    sane_polys = sane_eqs + sane_ineqs
    Neqs = len(sane_eqs)
    Nineqs = len(sane_ineqs)
    Npols = Neqs + Nineqs

    if Npols == 0:
        return (np.zeros((0, 3), dtype=np.float32),
                np.zeros((0, 3), dtype=np.float32),
                np.zeros((0, 3), dtype=np.int32))

    dim  = len(sane_polys[0][0][1])
    #if dim == 2:
    #    min_depth = max_depth
    #elif dim >= 4:
    #    max_depth = min_depth
    
    # reformat input
    lower = lower[:dim] if hasattr(lower, '__iter__') else [lower]
    upper = upper[:dim] if hasattr(upper, '__iter__') else [upper]
    axes = axes[:dim] if len(axes) > 0 else list(range(min(3,dim)))
    grid_size = grid_size[:dim] if hasattr(grid_size, '__iter__') else [grid_size]
    byte_polys = [ b'\n'.join(numtobytes(coef) + b' ' + b' '.join(numtobytes(e) for e in exp)
                              for coef, exp in p) + b'\0'
                  for p in sane_polys ]
    
    # convert input to c data structures
    c_polys = (ct.c_char_p * Npols)()
    c_lower = (ct.c_double * len(lower))()
    c_upper = (ct.c_double * len(upper))()
    c_grid  = (ct.c_int * len(grid_size))()
    c_axes  = (ct.c_int * len(axes))()
    c_signs  = (ct.c_int * (Neqs + Nineqs))()


    c_polys[:] = byte_polys
    c_lower[:] = [float(l) for l in lower]
    c_upper[:] = [float(u) for u in upper]
    c_grid[:]  = grid_size
    c_axes[:]  = axes
    c_signs[:]  = [0]*Neqs + [1]*Nineqs

    scheme = "Horner" if order==0 else "Linear" if order==1 else "Quadratic"
    c_scheme = ct.c_char_p(scheme.encode())

    # result sizes
    N = ct.c_int(0)
    Nv = ct.c_int(0)
    Nf = ct.c_int(0)
    
    # result vectors
    c_vertices = libvoxelize.new_vector_float()
    c_normals = libvoxelize.new_vector_float()
    c_faces = libvoxelize.new_vector_int32()

    if precision==32:
        c_result = libvoxelize.new_vector_32()
        libvoxelize.solve_32(c_polys, Npols, mesh, normal, c_lower, len(lower),
                              c_upper, len(upper), c_grid, len(grid_size),
                              c_axes, len(axes), center, min_depth, max_depth,
                              c_scheme, verbose, False, c_signs, Npols,
                              c_result, ct.byref(N), c_vertices, c_normals, ct.byref(Nv),
                              c_faces, ct.byref(Nf))
        result = np.zeros(N.value, dtype=np.float32)
        libvoxelize.vector_32_to_array(c_result, result)
        libvoxelize.delete_vector_32(c_result)
    elif precision==64:
        c_result = libvoxelize.new_vector_64()
        libvoxelize.solve_64(c_polys, Npols, mesh, normal, c_lower, len(lower),
                              c_upper, len(upper), c_grid, len(grid_size),
                              c_axes, len(axes), center, min_depth, max_depth,
                              c_scheme, verbose, False, c_signs, Npols,
                              c_result, ct.byref(N), c_vertices, c_normals, ct.byref(Nv),
                              c_faces, ct.byref(Nf))
        result = np.zeros(N.value, dtype=np.double)
        libvoxelize.vector_64_to_array(c_result, result)
        libvoxelize.delete_vector_64(c_result)
    elif precision==80:
        c_result = libvoxelize.new_vector_80()
        libvoxelize.solve_80(c_polys, Npols, mesh, normal, c_lower, len(lower),
                              c_upper, len(upper), c_grid, len(grid_size),
                              c_axes, len(axes), center, min_depth, max_depth,
                              c_scheme, verbose, False, c_signs, Npols,
                              c_result, ct.byref(N), c_vertices, c_normals, ct.byref(Nv),
                              c_faces, ct.byref(Nf))
        result = np.zeros(N.value, dtype=np.longdouble)
        libvoxelize.vector_80_to_array(c_result, result)
        libvoxelize.delete_vector_80(c_result)
    else:
        raise ValueError('Precision {0} not supported.'.format(precision))

    vertices = np.zeros(Nv.value, dtype=np.float32)
    libvoxelize.vector_float_to_array(c_vertices, vertices)
    libvoxelize.delete_vector_float(c_vertices)
    
    normals = np.zeros(Nv.value, dtype=np.float32)
    libvoxelize.vector_float_to_array(c_normals, normals)
    libvoxelize.delete_vector_float(c_normals)
    
    faces = np.zeros(Nf.value, dtype=np.int32)
    libvoxelize.vector_int32_to_array(c_faces, faces)
    libvoxelize.delete_vector_int32(c_faces)

    result.shape = (-1, dim, 2)
    vertices.shape = (-1, 3)
    normals.shape = (-1, 3)
    faces.shape = (-1, 3)

    return result, vertices, normals, faces

def solve(eqs, ineqs=[], lower=-2, upper=2, min_depth=7, max_depth=7, center=0,
        grid_size=1, precision=64, order=2, verbose=0):

    result, vertices, normals, faces = voxelize(
            eqs, ineqs, False, False, lower, upper, min_depth, max_depth, [], center,
            grid_size, precision, order, verbose)
    return result

def surface(*eqs, ineqs=[], lower=-2, upper=2, min_depth=7, max_depth=10, axes=[0,1,2], center=1,
        grid_size=1, precision=64, order=2, verbose=0, deg_bound=20):

    if len(eqs) == 1:
        eqs = eqs[0]
    result, vertices, normals, faces = voxelize(
            eqs, ineqs, True, True, lower, upper, min_depth, max_depth, axes, center,
            grid_size, precision, order, max(0,verbose-1), deg_bound)
    return vertices, normals, faces

def _surface_wrapper(dict_args):
    eqs = dict_args.pop('eqs')
    n = dict_args.pop('total')
    result = surface(eqs, **dict_args)
    if dict_args.get('verbose', 0) > 0:
        with counter.get_lock():
            counter.value += 1
            print("\r {0}/{1}".format(counter.value, n), end='')
    return result


def get_extremes_singulars(graph):
    indices, doubledegrees = np.unique(graph.nonzero(), return_counts=True)
    extremes  = indices[doubledegrees == 2]
    singulars = indices[doubledegrees >= 6]
    return extremes, singulars

#def snap_extremes(extremes, neighbours, distances, mapping):
#    edges = []
#    #st = time.time()
#    mextremes = mapping[extremes]
#    for v in extremes:
#        mv = mapping[v]
#        _, indn, inde  = np.intersect1d(neighbours[mv][1:], mextremes, return_indices=True)
#        for i, j in zip(indn, inde):
#            edges.append( (v, extremes[j], distances[mv][i+1]) )
#    #print(time.time()-st)
#    return edges


def snap_extremes(extremes, graph):
    edges = []
    #st = time.time()
    for v in extremes:
        _, indn, inde  = np.intersect1d(graph[v].indices, extremes, return_indices=True)
        for i, j in zip(indn, inde):
            d = graph[v].data[i]
            if d > 0:
                edges.append( (v, extremes[j], d) )
    #print(time.time()-st)
    return edges

def clean_tree(graph, extremes, singulars, snap_edges, threshold):
    #st = time.time()
    distances, predecessors = scipy.sparse.csgraph.shortest_path(graph, indices=extremes, return_predecessors=True, directed=False, unweighted=True)
    #print(time.time()-st)
    ind = dict((v,i) for i,v in enumerate(extremes))
    snaps = dict()
    flags = np.ones(len(extremes), dtype=bool)
    # Snap extremes of a big loop
    #st = time.time()
    for v,w,d in snap_edges:
        #print(v,w,d)
        if distances[ind[v], w] >= threshold:
            #print("selected")
            if not (v in snaps) or d < snaps[v][1]:
                snaps[v] = (w, d)
                flags[ind[v]]=False
                flags[ind[w]]=False
    rows = list(snaps.keys())
    #print(time.time()-st)
    if len(rows) > 0:
        cols, data = map(list, zip(*snaps.values()))
    else:
        cols, data = [], []
    rows, cols = rows + cols, cols + rows
    data = data*2
    # Erase small branches
    #st = time.time()
    if len(singulars)>0:
        for v in extremes[flags]:
            i = distances[ind[v], singulars].argmin()
            if distances[ind[v], singulars[i]] < threshold:
                v0 = singulars[i]
                while v0 != v:
                    v1 = predecessors[ind[v], v0]
                    rows += [v0, v1]
                    cols += [v1, v0]
                    data += [0,0]
                    v0 = v1
    #print(time.time()-st)
    #st = time.time()
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        graph[rows, cols] = data
    graph.eliminate_zeros()
    #print(time.time()-st)

def curve(*eqs, ineqs=[], lower=-2, upper=2, min_depth=7, max_depth=7, axes=[], center=0,
        grid_size=1, precision=64, order=2, verbose=0, deg_bound=20):

    #st = time.time()
    if len(eqs) == 1:
        eqs = eqs[0]
    result, vertices, normals, faces = voxelize(
            eqs, ineqs, False, True, lower, upper, min_depth, max_depth, axes, center,
            grid_size, precision, order, max(0,verbose-1), deg_bound)
    #print(0, time.time()-st)

    # get edges
    if result.size == 0:
        return ()
    midpoints = result.mean(-1)/np.diff(result[0]).flat
    kdtree = scipy.spatial.cKDTree(midpoints, leafsize=50)
    edges = kdtree.query_ball_tree(kdtree, 1.5, np.inf)
    points, indices = np.unique(vertices, return_inverse=True, axis=0)
    #points, indices = np.unique(result.mean(-1), return_inverse=True, axis=0)
    n = len(points)
    starts, ends, weights = [], [], []
    for i in range(len(edges)):
        for j in edges[i]:
            if indices[i] != indices[j]:
                starts.append(indices[i])
                ends.append(indices[j])
                #weights.append(np.linalg.norm(points[indices[j]]-points[indices[i]]))
    weights = np.linalg.norm(points[starts] - points[ends], axis = -1)

    # Old approach for point cloud
    # points = np.unique(vertices, axis=0)
    # # local distances graph
    # #st = time.time()
    # k = 6
    # n = len(points)
    # kdtree = scipy.spatial.cKDTree(points, leafsize=50)
    # distances, neighbours = kdtree.query(points, k)
    # valid = (neighbours != n)&(distances>0)
    # weights = distances[valid]
    # starts = np.arange(n).reshape(-1,1).repeat(k,axis=-1)[valid]
    # ends = neighbours[valid]

    graph = scipy.sparse.csr_matrix((weights, (starts, ends)), shape=(n,n))

    # connected paths
    #st = time.time()
    nc, components = scipy.sparse.csgraph.connected_components(graph, directed=False)
    #print(2, time.time()-st)
    paths = []
    if verbose > 0:
        print("Number of connected components: {0}".format(nc))
    for i in range(nc):
        if verbose > 0:
            print("Component {0}".format(i))

        ci = np.flatnonzero(components==i)
        g = graph[ci][:,ci]
        mstree = scipy.sparse.csgraph.minimum_spanning_tree(g)
        mstree = mstree.maximum(mstree.transpose())
        extremes, singulars = get_extremes_singulars(mstree)
        if verbose > 0:
            #e, s = get_extremes_singulars(g)
            #print("{0} points: {1} extreme and {2} singular".format(ci.size, len(e), len(s)))
            print("{0} points: {1} extreme and {2} singular".format(ci.size, len(extremes), len(singulars)))
        if len(extremes) > 10000 or len(singulars) > 10000:
            #TODO: use point plots in this case
            raise ValueError("Ill-conditioned input, try to increase the option center")
        #st = time.time()
        snap_edges = snap_extremes(extremes, g)
        #print(3, time.time()-st)
        #st = time.time()
        clean_tree(mstree, extremes, singulars, snap_edges, 10)
        #clean_tree(mstree, extremes, singulars, [], 10)
        #print(4, time.time()-st)
        #st = time.time()
        #snap_tree(mstree, neighbours, distances, ci)
        #print(5, time.time()-st)
        #mstree = graph[ci][:,ci]
        extremes, singulars = get_extremes_singulars(mstree)
        if verbose > 0:
            print("after cleaning: {1} extreme and {2} singular".format(ci.size, len(extremes), len(singulars)))
        if len(extremes) > 1000 or len(singulars) > 1000:
            raise ValueError("Ill-conditioned input, try to increase the option center")
        #print(extremes)
        #print(singulars)
        if len(singulars) + len(extremes) == 0:
            if mstree.indices.size > 0:
                start = mstree.indices[0]
                path = scipy.sparse.csgraph.depth_first_order(mstree, start,
                        return_predecessors=False)
                path = np.append(path, start)
                paths.append(ci[path])
        elif len(singulars) == 0:
            start = extremes[0]
            path = scipy.sparse.csgraph.depth_first_order(mstree, start,
                    return_predecessors=False)
            paths.append(ci[path])
        else:
            start = singulars[0]
            subpaths, parents = scipy.sparse.csgraph.depth_first_order(mstree, start)
            # Method 1
            path = [start]
            #print(start)
            for v in subpaths[1:]:
                if path[-1] == parents[v]:
                    path.append(v)
                    if np.isin(v, singulars):
                        #print(v)
                        paths.append(ci[path])
                        path = [v]
                else:
                    snap = np.intersect1d(mstree[path[-1]].indices, singulars)
                    snap = np.delete(snap, snap == parents[path[-1]])
                    if len(snap) > 0:
                        #print('snap', snap[0])
                        path.append(snap[0])
                    paths.append(ci[path])
                    #print(parents[v], v)
                    path = [parents[v], v]
            snap = np.intersect1d(mstree[path[-1]].indices, singulars)
            snap = np.delete(snap, snap == parents[path[-1]])
            if len(snap) > 0:
                #print('snap', snap[0])
                path.append(snap[0])
            paths.append(ci[path])

            # Method 2: Change the cutting points
            # endpoints = np.concatenate([singulars, extremes] + list(mstree[v].indices for v in singulars))
            # cuts = np.flatnonzero(np.isin(subpaths, endpoints))
            # cuts = cuts[1:]
            # low = 0
            # for high in cuts:
            #     if high > low + 1:
            #         path = subpaths[low:high+1]
            #         snap = np.intersect1d(mstree[path[-1]].indices, singulars)
            #         if snap.size > 0:
            #             path = np.append(path, snap[0])
            #         snap = np.intersect1d(mstree[path[0]].indices, singulars)
            #         if snap.size > 0:
            #             path = np.insert(path, 0, snap[0])
            #         if len(path) > 2:
            #             paths.append(ci[path])
            #         low = high+1
            #     else:
            #         low = high

            #prefix = []
            #for indsing in cuts:
            #    path = np.insert(subpaths[low:indsing], 0, prefix)
            #    if subpath[indsing-1] == parents[subpath[indsing]]:
            #        path = subpaths[low:indsing+1]
            #        low = indsing
            #    else:

            #        prefix = [subpath[indsing]]


    return tuple(points[path] for path in paths)

solve.__doc__   = voxelize.__doc__
surface.__doc__ = voxelize.__doc__
curve.__doc__   = voxelize.__doc__

class Plot3D():
    def __init__(self, lower=[-2,-2,-2], upper=[2,2,2], min_depth=7,
            max_depth=10, curve_colors=['red','blue', 'yellow', 'green'],
            surface_color=(0.5, 0.5, 1, 1), tube_radius = 0.05,
            accumulate=False, axes=[0,1,2], center=1, grid_size=1, precision=64,
            order=2, verbose=0, dt = 0.05, show_mesh = True, show_wireframe
            = False, deg_bound = 20, show=True, basename = 'animation'):

        lower = lower if hasattr(lower, '__iter__') else [lower]
        for i in range(3-len(lower)):
            lower.append(lower[-1])
        upper = upper if hasattr(upper, '__iter__') else [upper]
        for i in range(3-len(upper)):
            upper.append(upper[-1])
        self.lower = lower
        self.upper = upper
        self.min_depth = min_depth
        self.max_depth = max_depth
        self.curve_colors = curve_colors
        self.surface_color = surface_color
        self.tube_radius = tube_radius
        self.accumulate = accumulate
        self.axes = axes
        self.center = center
        self.grid_size = grid_size
        self.precision = precision
        self.order = order
        self.verbose = verbose
        self.show_mesh = show_mesh
        self.show_wireframe = show_wireframe
        self.deg_bound = deg_bound

        self.visuals = []
        self.always_show = []
        self.current = None

        # Initialize the canva
        self.canvas = vispy.scene.SceneCanvas(
                keys=dict(
                    space = self.toggle_animation,
                    p     = self.toggle_animation,
                    right = self.next,
                    left  = self.prev,
                    l     = self.turn_light,
                    w     = self.toggle_wireframe,
                    m     = self.toggle_mesh,
                    a     = self.toggle_accumulate,
                    f     = self.toggle_fix,
                    s     = self.split,
                    r     = self.toggle_record,
                    g     = self.group,
                    q     = 'close'),
                bgcolor='#efefef')# show=True)
        self.view = self.canvas.central_widget.add_view()
        self.view.camera = 'turntable'  # or try 'arcball'
        self.view.camera.set_range(*zip(lower, upper))

        # Handles the GUI loop
        self.ipython_running = True
        try:
            #import IPython
            if get_ipython().__class__.__name__ == 'TerminalInteractiveShell':
                get_ipython().enable_gui(gui='qt')
            self.ipython_running = True
        except:
            self.ipython_running = False
            pass
            #atexit.register(self.keep_open)

        # Timer attributes
        self.basename = basename
        self.counter = 0
        self.recording = False
        self.animated = False
        self.dt = dt
        self.timer = vispy.app.Timer(interval=dt, connect=self.recurrent_actions, iterations=-1, start=False)

        if show:
            self.canvas.show()

    def show(self):
        self.canvas.show()

    def toggle_record(self):
        if not self.recording:
            try:
                vispy.gloo.read_pixels()
                if self.verbose > 0:
                    print('Recording to file {0}'.format(self.basename + '_' + str(self.counter) + '.mp4'))
                self.writer = imageio.get_writer(self.basename + '_' + str(self.counter) + '.mp4',
                                                 fps = 1/self.dt if self.dt != 0 else None)
                self.counter += 1
                self.recording = True
            except:
                print("Warning: recording not supported with this backend")
        else:
            self.recording = False
            self.writer.close()
            if self.verbose > 0:
                print('Done')
        self.update_timer()

    def toggle_animation(self):
        self.animated = not self.animated
        self.update_timer()
        #if self.timer.running:
        #    self.timer.stop()
        #else:
        #    self.timer.start()

    def update_timer(self):
        if (self.recording or self.animated) and not self.timer.running:
            self.timer.start()
        if not self.recording and not self.animated and self.timer.running:
            self.timer.stop()

    def next(self):
        if self.timer.running:
            self.timer.stop()
        pos = 0
        if self.always_show[self.current]:
            for pos in range(len(self.visuals)):
                if not self.always_show[(self.current + pos + 1) % len(self.visuals)]:
                    break
            else:
                return
        self.update_view(self.current+pos+1)
    
    def prev(self):
        if self.timer.running:
            self.timer.stop()
        pos = 0
        if self.always_show[self.current]:
            for pos in range(len(self.visuals)):
                if not self.always_show[(self.current - pos - 1) % len(self.visuals)]:
                    break
            else:
                return
        self.update_view(self.current-pos-1)
    
    def turn_light(self):
        for v in self.visuals:
            try:
                v.shading_filter.light_dir = [-x for x in v.shading_filter.light_dir]
            except:
                try:
                    v[0].shading_filter.light_dir = [-x for x in v[0].shading_filter.light_dir]
                except:
                    for sv in v.children:
                        sv.shading_filter.light_dir = [-x for x in sv.shading_filter.light_dir]


    def toggle_wireframe(self):
        self.show_wireframe = not self.show_wireframe
        self.update_view()

    def toggle_mesh(self):
        self.show_mesh = not self.show_mesh
        self.update_view()
    
    def toggle_accumulate(self):
        self.accumulate = not self.accumulate
        self.update_view()
    
    def toggle_fix(self):
        self.always_show[self.current] = not self.always_show[self.current]
        self.update_view()

    def is_visual_visible(self, i):
        try:
            return self.visuals[i].visible
        except:
            return self.visuals[i][0].visible or self.visuals[i][1].visible

    def show_visual(self, i):
        try:
            self.visuals[i].visible = True
            self.visuals[i].parent = self.view.scene
        except:
            self.visuals[i][0].visible = self.show_mesh
            self.visuals[i][1].visible = self.show_wireframe
            self.visuals[i][0].parent = self.view.scene
            self.visuals[i][1].parent = self.view.scene

    def hide_visual(self, i):
        try:
            self.visuals[i].visible = False
            self.visuals[i].parent = None
        except:
            self.visuals[i][0].visible = False
            self.visuals[i][1].visible = False
            self.visuals[i][0].parent = None
            self.visuals[i][1].parent = None

    def update_view(self, position=None):
        if position is not None:
            self.current = position % len(self.visuals)
        for i in range(len(self.visuals)):
            if (self.accumulate and i < self.current) or self.always_show[i] or i == self.current:
                self.show_visual(i)
            else:
                self.hide_visual(i)

    def recurrent_actions(self, event):
        if self.recording:
            try:
                im = vispy.gloo.read_pixels()
            except:
                pass
                #im = self.canvas.render()
            self.writer.append_data(im)
        if self.animated:
            self.forward()


    def forward(self):
        for pos in range(len(self.visuals)):
            if not self.always_show[(self.current + pos + 1) % len(self.visuals)]:
                break
        else:
            return
        self.update_view(self.current+pos+1)

    def surface_from_data(self, vertices, normals, faces, color=None, always_show=False):
        color = self.surface_color if color is None else color

        # mesh visual
        mesh = vispy.scene.Mesh(vertices.astype(np.float32), faces.astype(np.int32), shading='smooth', color=color)
        mesh.mesh_data._vertex_normals = -normals.astype(np.float32)
        mesh.shading_filter.shininess = 0
        mesh.visible = self.show_mesh
        # wireframe visual
        nf = len(faces)
        mask = np.tile([0, 1, 1, 2, 2, 0], nf) + np.repeat(np.arange(0, nf * 3, 3), 6)
        edges = faces.reshape(-1)[mask]
        wireframe = vispy.scene.Line(pos=vertices[edges], connect='segments', color='black', width=5)
        wireframe.visible = self.show_wireframe
        self.view.add(mesh)
        self.view.add(wireframe)
        self.visuals.append([mesh, wireframe])
        self.always_show.append(always_show)
        self.update_view(len(self.visuals)-1)
        if not self.ipython_running:
            vispy.app.process_events()
        return mesh, wireframe

    def surface_batch(self, list_args, verbose=None):
        global counter
        keys_default_surface = ['lower', 'upper', 'min_depth', 'max_depth', 'axes', 'center',
                    'grid_size', 'precision', 'order', 'verbose', 'deg_bound']
        list_surface_args = []
        for d in list_args:
            d['total'] = len(list_args)
            if verbose is not None:    d['verbose'] = verbose
            if 'color' not in d:       d['color'] = self.surface_color
            if 'always_show' not in d: d['always_show'] = False
            for key in keys_default_surface:
                if key not in d:
                    d[key] = getattr(self, key)
            list_surface_args.append(dict((k, d[k]) for k in keys_default_surface + ['eqs', 'ineqs', 'total']))

        counter = mp.Value('i',0)
        verbose = self.verbose if verbose is None else verbose
        if verbose > 0:
            print('Computing {0} meshes'.format(len(list_args)))
        pool = mp.Pool()
        results = pool.map(_surface_wrapper, list_surface_args)
        pool.close()
        pool.join()
        if verbose > 0:
            print('')
        for data, arg in zip(results, list_args):
            self.surface_from_data(data[0], data[1], data[2], color=arg['color'], always_show=arg['always_show'])
        return results



    def surface(self, *eqs, ineqs=[], lower=None, upper=None,
                color=None, min_depth=None, max_depth=None,
                axes=None, center=None,
                grid_size=None, precision=None,
                order=None, verbose=None, deg_bound=None, always_show = False):
        lower = self.lower if lower is None else lower
        upper = self.upper if upper is None else upper
        min_depth = self.min_depth if min_depth is None else min_depth
        max_depth = self.max_depth if max_depth is None else max_depth
        axes = self.axes if axes is None else axes
        center = self.center if center is None else center
        grid_size = self.grid_size if grid_size is None else grid_size
        precision = self.precision if precision is None else precision
        order = self.order if order is None else order
        verbose = self.verbose if verbose is None else verbose
        deg_bound = self.deg_bound if deg_bound is None else deg_bound

        if len(eqs) == 1:
            eqs = eqs[0]
        result, vertices, normals, faces = voxelize(
                eqs, ineqs, True, True, lower, upper, min_depth, max_depth, axes, center,
                grid_size, precision, order, verbose, deg_bound, dim=3)
        return self.surface_from_data(vertices, normals, faces, color,
                always_show)

        # # mesh visual
        # mesh = vispy.scene.Mesh(vertices, faces, shading='smooth', color=color)
        # mesh.mesh_data._vertex_normals = -normals
        # mesh.shininess = 0
        # mesh.visible = self.show_mesh
        # # wireframe visual
        # nf = len(faces)
        # mask = np.tile([0, 1, 1, 2, 2, 0], nf) + np.repeat(np.arange(0, nf * 3, 3), 6)
        # edges = faces.reshape(-1)[mask]
        # wireframe = vispy.scene.Line(pos=vertices[edges], connect='segments', color='black', width=5)
        # wireframe.visible = self.show_wireframe
        # self.view.add(mesh)
        # self.view.add(wireframe)
        # self.visuals.append([mesh, wireframe])
        # self.always_show.append(always_show)
        # self.update_view(len(self.visuals)-1)
        # vispy.app.process_events()
        # return mesh, wireframe

    def curve_from_data(self, paths, tube_radius=None, colors=None, always_show = False):
        tube_radius = self.tube_radius if tube_radius is None else tube_radius
        colors = self.curve_colors if colors is None else colors
        if isinstance(colors, str) or \
           (len(colors)>0 and isinstance(colors[0], (int, float))):
            colors = [colors]
        tubes = []

        lc = len(colors)
        c = 0
        for path in paths:
            isloop = np.all(path[0] == path[-1])
            if (path.shape[0] % 10) == 1:
                lightpath = path[::10]
            else:
                lightpath = np.vstack((path[::10], path[-1]))
            tube = vispy.scene.Tube(lightpath, radius = tube_radius, tube_points=16, color=colors[c], closed=isloop)
            tube.shading_filter.shininess = 0
            tubes.append(tube)
            c = (c+1) % lc
        union = vispy.scene.Node(parent=self.view.scene)
        for t in tubes:
            t.parent = union
        #union = vispy.scene.Compound(tubes)
        #self.view.add(union)
        self.visuals.append(union)
        self.always_show.append(always_show)
        self.update_view(len(self.visuals)-1)
        if not self.ipython_running:
            vispy.app.process_events()
        return paths


    def curve(self, *eqs, ineqs=[], lower=None, upper=None,
                colors=None, min_depth=None, max_depth=None,
                tube_radius=None, axes=None, center=None,
                grid_size=None, precision=None, order=None,
                verbose=None, deg_bound = None, always_show = False):
        lower = self.lower if lower is None else lower
        upper = self.upper if upper is None else upper
        #tube_radius = self.tube_radius if tube_radius is None else tube_radius
        #colors = self.curve_colors if colors is None else colors
        #if isinstance(colors, str) or \
        #   (len(colors)>0 and isinstance(colors[0], (int, float))):
        #    colors = [colors]
        min_depth = self.min_depth if min_depth is None else min_depth
        max_depth = self.max_depth if max_depth is None else max_depth
        axes = self.axes if axes is None else axes
        center = self.center if center is None else center
        grid_size = self.grid_size if grid_size is None else grid_size
        precision = self.precision if precision is None else precision
        order = self.order if order is None else order
        verbose = self.verbose if verbose is None else verbose
        deg_bound = self.deg_bound if deg_bound is None else deg_bound

        paths = curve(*eqs, ineqs=ineqs, lower=lower, upper=upper,
                min_depth=min_depth, max_depth=max_depth, axes=axes,
                center=center, grid_size=grid_size, precision=precision, order=order,
                verbose=verbose, deg_bound=deg_bound)
        return self.curve_from_data(paths, tube_radius, colors, always_show)

        #tubes = []
        #lc = len(colors)
        #c = 0
        #for path in paths:
        #    isloop = np.all(path[0] == path[-1])
        #    if (path.shape[0] % 10) == 1:
        #        lightpath = path[::10]
        #    else:
        #        lightpath = np.vstack((path[::10], path[-1]))
        #    tube = vispy.scene.Tube(lightpath, radius = self.tube_radius, tube_points=16, color=colors[c], closed=isloop)
        #    tube.shininess = 0
        #    tubes.append(tube)
        #    c = (c+1) % lc
        #union = vispy.scene.Compound(tubes)
        #self.view.add(union)
        #self.visuals.append(union)
        #self.always_show.append(always_show)
        #self.update_view(len(self.visuals)-1)
        #vispy.app.process_events()
        #return union

    def split(self):
        if isinstance(self.visuals[self.current], vispy.scene.Node):
            sv = self.visuals[self.current].children
            show = self.always_show[self.current]
            del self.visuals[self.current]
            del self.always_show[self.current]
            for v in sv[::-1]:
                self.visuals.insert(self.current, v)
                self.always_show.insert(self.current, show)
            self.update_view()

    def group(self):
        L = [i for i in range(len(self.visuals)) if self.is_visual_visible[i]]
        #C = vispy.scene.Compound([self.visuals[i] for i in L])
        C = vispy.scene.Node(parent=self.view.scene)
        V = []
        for i in reversed(L):
            self.visuals[i].parent = C
            del self.visuals[i]
            del self.always_show[i]
        C.visible = False
        #self.view.add(C)
        self.visuals.insert(i, C)
        self.always_show.insert(i, False)
        self.update_view(i)

    def animate(self):
        self.animated = True
        self.update_timer()

    def stop_animate(self):
        self.animated = False
        self.update_timer()

    def keep_open(self):
        self.canvas.app.run()

