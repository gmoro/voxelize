import ast
import contextlib
import ctypes
import ctypes.util

try:
    libm = ctypes.cdll.LoadLibrary(ctypes.util.find_library('m'))
    mode_upward = 0x800
    fegetround = libm.fegetround
    fesetround = libm.fesetround
except:
    mode_upward = 0x0200
    def fegetround():
        return ctypes.cdll.msvcrt._controlfp(0,0)
    def fesetround(mode):
        return ctypes.cdll.msvcrt._controlfp(mode,0x300)


@contextlib.contextmanager
def round_upward():
    mode = fegetround()
    try:
        fesetround(mode_upward)
        yield
    finally:
        fesetround(mode)

class Interval:
    def __init__(self, upper, neg_lower = None):
        with round_upward():
            self.upper = float(upper)
            self.neg_lower = float(-upper) if neg_lower is None else float(neg_lower)

    def __iter__(self):
        yield -self.neg_lower
        yield self.upper

    def __repr__(self):
        return str([-self.neg_lower, self.upper])

    def sign(self):
        result = -1 if self.upper < 0 else \
                  1 if self.neg_lower < 0 else \
                  0
        return result

    def __add__(self, other):
        if not isinstance(other, Interval):
            try:
                other = Interval(other)
            except:
                return NotImplemented
        with round_upward():
            upper = self.upper + other.upper
            neg_lower = self.neg_lower + other.neg_lower
        return Interval(upper, neg_lower)

    def __radd__(self, other):
        return self + other
    
    def __sub__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        with round_upward():
            upper = self.upper + other.neg_lower
            neg_lower = self.neg_lower + other.upper
        return Interval(upper, neg_lower)

    def __rsub__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        return other - self

    def __mul__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        print(self, other)
        with round_upward():
            upper = max(self.upper * other.upper,
                        self.upper * (-other.neg_lower),
                        self.neg_lower * (-other.upper),
                        self.neg_lower * other.neg_lower)
            neg_lower = max(self.upper * (-other.upper),
                            self.upper * other.neg_lower,
                            self.neg_lower * other.upper,
                            self.neg_lower * (-other.neg_lower))
        return Interval(upper, neg_lower)

    def __rmul__(self, other):
        return self * other

    def __truediv__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        s_self  = self.sign()
        s_other = other.sign()
        if s_other == 0:
            return Interval(float("infinity"), float("infinity"))
        s = (s_self, s_other)
        with round_upward():
            upper = self.upper / (-other.neg_lower)  if s == (1,1)  else \
                    self.neg_lower / other.neg_lower if s == (1,-1) else \
                    self.upper / other.upper         if s == (-1,1) else \
                    self.neg_lower / (-other.upper)
            neg_lower = self.neg_lower / other.upper        if s == (1,1)  else \
                        self.upper / (-other.upper)         if s == (1,-1) else \
                        self.neg_lower / (-other.neg_lower) if s == (-1,1) else \
                        self.upper / other.neg_lower
        return Interval(upper, neg_lower)

    def __rtruediv__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        return other / self


class SparsePolynomial:
    def __init__(self, nvars, coefficient=None, monomial=None):
        self.T = dict()
        self.N = nvars
        if monomial:
            self.set_term(coefficient, monomial)
        elif coefficient:
            self.set_constant(coefficient)

    def set_term(self, c, m):
        self.T[tuple(m)] = c

    def set_constant(self, c):
        self.T[(0,)*self.N] = c

    def __iter__(self):
        return ((list(c) if isinstance(c, Interval) else c, m) for m, c in sorted(self.T.items()) if c != 0) 

    def __copy__(self):
        result = SparsePolynomial(self.N)
        result.T.update(self.T)
        return result

    def __repr__(self):
        return str(self.T)

    def __add__(self, other):
        if not isinstance(other, SparsePolynomial):
            other = SparsePolynomial(self.N, other)
        result = self.__copy__()
        for m, c in other.T.items():
            if m in result.T:
                result.T[m] += c
            else:
                result.T[m] = c
        return result

    def __radd__(self, other):
        return self + other

    def __sub__(self,other):
        if not isinstance(other, SparsePolynomial):
            other = SparsePolynomial(self.N, other)
        result = self.__copy__()
        for m, c in other.T.items():
            if m in result.T:
                result.T[m] -= c
            else:
                result.T[m] = - c
        return result

    def __rsub__(self, other):
        if not isinstance(other, SparsePolynomial):
            other = SparsePolynomial(self.N, other)
        return other - self

    def __mul__(self, other):
        if not isinstance(other, SparsePolynomial):
            other = SparsePolynomial(self.N, other)
        result = SparsePolynomial(self.N)
        for m0, c0 in self.T.items():
            for m1, c1 in other.T.items():
                mres = tuple(i0 + i1 for i0, i1 in zip(m0,m1))
                result.T[mres] = result.T.get(mres,0) + c0*c1
        return result

    def __rmul__(self, other):
        return self * other

    def __neg__(self):
        result = self.__copy__()
        for m, c in result.T.items():
            result.T[m] = -c
        return result

    def __truediv__(self, other):
        result = self.__copy__()
        for m, c in result.T.items():
            result.T[m] = c/other
        return result

    def __pow__(self, other):
        if len(self.T) > 1:
            raise NotImplementedError
        result = SparsePolynomial(self.N)
        if len(self.T) == 1:
            m, c = next(iter(self.T.items()))
            if c != 1:
                raise NotImplementedError
            newm = tuple(i*other for i in m)
            result.T[newm] = 1
        return result

def atom(n,i):
    L = [0]*n
    L[i] = 1
    return SparsePolynomial(n, 1, L)

def to_poly(expression, varnames):
    var_names = [v.strip() for v in varnames.split(',')]
    poly_string = expression.strip().replace("^", "**").replace("\\", "")
    n = len(var_names)
    st = ast.parse(poly_string, mode="eval")
    for node in ast.walk(st):
        if isinstance(node, ast.BinOp) and isinstance(node.op, ast.Div):
            node.right = ast.parse(f"_interval({node.right.value})", mode="eval").body
    var_dict = dict((var_names[i], atom(n, i)) for i in range(n))
    local_vars = {**var_dict, '_interval': Interval}
    poly = eval(compile(st, "<ast>", mode="eval"), local_vars)
    if not isinstance(poly, SparsePolynomial):
        poly = SparsePolynomial(n, poly)
    result = list(poly)
    return result
         
# Test:
#  1/3 + x
#  1/3
