all: voxelize

OPENMP_FLAGS := -Xpreprocessor -fopenmp -lomp -I/usr/local/opt/libomp/include -L/usr/local/opt/libomp/lib

voxelize_debug:
	g++ -std=c++11 src/main/voxelize.cpp -g -o voxelize_debug

voxelize:
	g++ -std=c++11 src/main/voxelize.cpp -O3 $(OPENMP_FLAGS) -o voxelize

voxelize_float128:
	g++ -std=c++11 -DFLOAT128 src/main/voxelize.cpp -O3 $(OPENMP_FLAGS) -lquadmath -o voxelize_float128

voxelize_compact:
	g++ -std=c++11 -DCOMPACT src/main/voxelize.cpp -O3 $(OPENMP_FLAGS) -o voxelize_compact

voxelize_static:
	g++ -std=c++11 src/main/voxelize.cpp -O3 -o voxelize_static $(OPENMP_FLAGS) -Wl,-Bstatic
